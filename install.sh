#!/bin/sh
#
# Install all the EBN tools in one go
#
# Usage:
#    install [--prefix=/dir]
 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA


# change TOP to whatever you like for your top-level installation directory
if ( test `hostname -f | egrep -c englishbreakfast` -gt 0 ) then
  EBN=1
  TOP=/mnt/ebn
else
  EBN=0
  TOP=/usr/local/EBN
fi

# check command-line argument
case "$1" in
--prefix=*)
  TOP=`echo "$1" | sed -e 's+--prefix=++'`
  ;;
*)
  ;;
esac

( cd tools ; sh install.sh --prefix=$TOP )
( cd apidox ; sh install.sh --prefix=$TOP )

