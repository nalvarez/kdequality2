<?php
# Copyright (C) 2007 by Allen Winter <winter@kde.org>
#
# This file defines functions for handling the breadcrumbs on the EBN.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in a file called COPYING; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

if (!isset($breadcrumbs)) {
	$breadcrumbs = array();
}

function crumb($title,$url="") {
	global $breadcrumbs;
	if (empty($url)) {
		$breadcrumbs[] = array(  'name' => $title );
	} else {
		$breadcrumbs[] = array( 'url' => $url, 'name' => $title );
	}
}

function print_breadcrumbs() {
	global $breadcrumbs;

	if (!isset($breadcrumbs)) {
		/* Generic breadcrumbs. */
		$breadcrumbs = array();
		crumb('Home','/index.php');
	}

	if (isset($breadcrumbs) and is_array($breadcrumbs)) {
		$firstBreadcrumb = TRUE;
		print "<!-- Breadcrumbs -->\n";
		print '<p style="font-size: x-small;font-style: sans-serif;">';
		foreach($breadcrumbs as $b) {
			if (!$firstBreadcrumb) {
				print '&nbsp;&gt;&nbsp;';
			} else {
				$firstBreadcrumb = FALSE;
			}
			print "\n";
			if (isset($b["url"])) {	
				print '  <a href="'.$b["url"].'">'.$b["name"].'</a>';
			} else {
				print "  ".$b["name"];
			}
		}
		print "\n</p>\n\n";
	}
}

if (!isset($breadcrumbs)) {
	crumb('Home','/');
}

?>
