<?php
# Copyright (C) 2007 by Adriaan de Groot <groot@kde.org>
#
# This include file does most of the database processing needed
# to fetch results from one of the result databases and
# get it ready for display. After including this file,
# you will want to include resultstable.inc to do the
# actual display.
#
# This file wants a bunch of variables to customize its
# behavior. These must be set globally before including:
#
# $i_am = "apidox"
#     The tool namewe're working with.
# $table_name = "results_apidox";
#     The name of the database table to get results
#     from. Right now this is needed, but when the
#     results tables are all merged it will vanish.
# $mypath = "apidocs";
#     Path to the results page relative to the site root,
#     with no trailing slash (ie. the name of the subdirectory
#     this is from).
# $myname = "APIDOX";
#     User-readable name describing what the tool does;
#     this will also be used in the sentence "$myname Results".
# $toolname = "dox";
#     Name of the tool in the tools database.
# $reportsdir = "";
#     Common prefix to the reports generated by the
#     tool, if it is not included in the report field
#     in the database.
# 


# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in a file called COPYING; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.



include 'parseargs.inc';
include_once 'crumbs.inc';

if ($haveComponent) {
	$componentId = get_single_value("SELECT id FROM components WHERE name='".$queryVars['component']."'");

	$verboseComponent = get_single_value("SELECT verbose_name FROM components WHERE name='".$queryVars['component']."';");

	/* Figure out what tool ID corresponds to this component. */
	$toolId = get_single_value("SELECT id FROM tools WHERE name='$toolname' AND component=$componentId");

	/* Get the generation of this tool ID. */
	$generation = get_single_value("SELECT generation FROM tools WHERE id=".$toolId);

	/* Get the timestamp of the most recent run of this tool. */
	$when = get_single_value("SELECT timestamp FROM generations WHERE tool=".$toolId." AND generation=" . $generation);

	$svn_revision = get_single_value("SELECT revision FROM generations WHERE tool=".$toolId." AND generation=" . $generation);
}


$results = array();
if (!$haveApplication && !$haveModule && !$haveComponent) {
	crumb($myname);
	$title = "Overall results";
	$result = $db->query("
		SELECT components.verbose_name, 
			components.name, 
			SUM($table_name.issues) 
		FROM components, $table_name 
		WHERE components.id=$table_name.component AND 
			$table_name.generation = ( 
				SELECT generation FROM tools 
				WHERE name='$toolname' AND 
					component=components.id ) 
		GROUP BY verbose_name, name 
		ORDER BY verbose_name;");
	if (!DB::isError($result)) {
		while ($row = $result->fetchRow()):
			$results[] = array("subject" => $row[0],
			 "issues" => $row[2],
			 "link" => "index.php?component=".$row[1]);
		endwhile;
		$result->free();
	}

} else  if (!$haveApplication && !$haveModule && $haveComponent) {
	crumb($myname,"/$mypath/index.php");
	crumb($verboseComponent);
	$title = "Results for ".$verboseComponent;
	$result = $db->query("SELECT module, SUM(issues) FROM $table_name WHERE component=" . $componentId . "AND generation=" . $generation . " GROUP BY module ORDER BY module;");
	if (DB::isError($result)) {
		print '<!-- ' . $result->getMessage() . " -->\n";
	} else {
		while ($row = $result->fetchRow()) {
			$results[] = array("subject" => $row[0],
				 "issues" => $row[1],
				 "link" => "index.php?component=".$queryVars["component"]."&module=".$row[0]);
		}
		$result->free();
	}
} else  if (!$haveApplication && $haveModule && !$haveComponent) {
	print "<p>Must specify a component when giving a module.</p>";

} else  if (!$haveApplication && $haveModule && $haveComponent) {
	crumb($myname,"/$mypath/index.php");
	crumb($verboseComponent,"index.php?component=" . $queryVars['component']);
	crumb($queryVars['module']);
	$title = "Results for module ".$queryVars["module"]." in ".$verboseComponent;
	$result = $db->query("SELECT application, SUM(issues), report FROM $table_name WHERE component=(SELECT id FROM components WHERE name='".$queryVars["component"]."') AND module='". $queryVars["module"] ."' AND generation=" . $generation . " GROUP BY application, report ORDER BY application;");
	while ($row = $result->fetchRow()):
		$results[] = array("subject" => $row[0],
		 "issues" => $row[1],
		 "link" => $reportsdir.$row[2]);
	endwhile;
	$result->free();

} else if ($haveApplication && !$haveModule && !$haveComponent) {
	print "<p>You can find the report at there.</p>";

} else  if ($haveApplication && !$haveModule && $haveComponent) {
	print "<p>You can find the report at there.</p>";

} else  if ($haveApplication && $haveModule && !$haveComponent) {
	print "<p>Must specify a component when giving a module.</p>";

} else  if ($haveApplication && $haveModule && $haveComponent) {
	print "<p>You can find the report at there.</p>";
}

/* Compute maximum number of issues, and total number of issues, so that
* we can draw nice percentage graphs.
 */
$maxIssues = 0;
$totalIssues = 0;
foreach($results as $r) {
	if ($maxIssues < $r["issues"]) {
		$maxIssues = $r["issues"];
	}
	$totalIssues += $r["issues"];
}

