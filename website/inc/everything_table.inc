<?php
# Copyright (C) 2007 by Adriaan de Groot <groot@kde.org>
#
# This file defines tables which state what is allowed within the EBN.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in a file called COPYING; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.


$everything_table = array (
	"apidox" => array (
		"table_name" => "results_apidox",
		"mypath"     => "apidocs",
		"myname"     => "APIDOX",
		"toolname"   => "dox",
		"reportsdir" => "",
		"haverss"    => 1
		),
	"krazy" => array (
		"table_name" => "results_krazy",
		"mypath"     => "krazy",
		"myname"     => "Krazy Code Checker",
		"toolname"   => "krazy",
		"reportsdir" => "reports/",
		"haverss"    => 1
		),
	"sanitizer" => array(
		"table_name" => "results_sanitizer",
		"mypath" => "sanitizer",
		"myname" => "DocBook Sanitizer",
		"toolname" => "sanitizer",
		"reportsdir" => "reports/",
		"haverss" => 1
		),
	"usability" => array(
		"table_name" => "results_usability",
		"mypath" => "usability",
		"myname" => "Usability Watchdog",
		"toolname" => "usability",
		"reportsdir" => "reports/",
		"haverss" => 1
		),
	"unittests" => array(
		"table_name" => "results_unittests",
		"mypath" => "unittests",
		"myname" => "Unit Testing",
		"toolname" => "unittests",
		"reportsdir" => "reports/",
		"haverss" => 0
		)
) ;

