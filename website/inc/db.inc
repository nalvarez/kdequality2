<?php
# Copyright (C) 2007 by Adriaan de Groot <groot@kde.org>
# Copyright (C) 2007 by Frerich Raabe <raabe@kde.org>
#
# This file defines database access; in particular it opens
# a connection to the KDE database and sticks the PEAR DB
# object in the global variable $db. If database connection
# fails, $db is unset.
#
# Function generic_query executes a query and formats it as
# a <ul> list. Every row returned by the query is printed in a
# <li> item. The optional $format parameter determines how the
# rows are formatted. If $format is empty (the default) then
# a nested <ul> list is printed, with one <li> per column and
# the raw column data dumped. If $format is not empty, fields of
# the form %columname where columname appears in the database
# will be replaced by the raw column data.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in a file called COPYING; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

require_once('DB.php');
require_once('db-settings.inc');

error_reporting(E_ALL);

# Use the connection variables from elsewhere.
$db = DB::connect("pgsql://$db_user:$db_pass@$db_host/$db_name");
if (DB::iserror($db)) {
	print '<!-- ' . $db->getMessage() . '-->' ;
	unset($db);
}

function generic_query($query,$format="") {
	global $db;

	if (!isset($db)) {
		print "<!-- No database -->\n";
		return;
	}

	$q = $db->query($query);
	if (DB::iserror($q)) {
		print '<!-- ' . $q->getMessage() . ' -->' ;
		return;
	}

	if (!($q->numRows() > 0)) {
		print '<!-- No rows returned by query. -->' ;
		return;
	}

	# If no format is given, do a debug dump of the query
	if (empty($format)) {
		print "\n\n<ul>\n";
		while ($q->fetchInto( $row ) ) {
			print "  <li><ul>\n";
			for ($i=0; $i<count($row); $i++) {
				print '    <li>' . $row[$i] . "</li>\n";
			}
			print "  </ul></li>\n";
		}
		print "</ul>\n\n";
	} else {
		$replaces = array();
		foreach ($q->tableInfo() as $i) {
			$replaces[] = "%" . $i['name'] ;
		}

		while ($q->fetchInto( $row, DB_FETCHMODE_ASSOC ) ) {
			print str_replace( $replaces, $row, $format);
		}
	}

	$q->free();
}

function generic_query_as_table($query,$captions) {
	global $db;

	if (!isset($db)) {
		print "<!-- No database -->\n";
		return;
	}

	$q = $db->query($query);
	if (DB::iserror($q)) {
		print '<!-- ' . $q->getMessage() . ' -->' ;
		return;
	}

	if (!($q->numRows() > 0)) {
		print '<!-- No rows returned by query. -->' ;
		return;
	}

	print "\n\n";
	print '<table class="sortable" id="genericID" cellspacing="0" border="0">';
	print "\n";
	if (!empty($captions)) {
		print "  <tr>\n";
		for ($i=0; $i<count($captions); $i++) {
			print "    <th>".$captions[$i]."</th>\n";
		}
		print "  </tr>\n";
	}
	while ($q->fetchInto($row)) {
		print "  <tr>\n";
		for ($i=0; $i<count($row); $i++) {
			print "    <td>".$row[$i]."</td>\n";
		}
		print "  </tr>\n";
	}
	print "</table>\n";
	$q->free();
}
function generic_query_as_result($query) {
	global $db;
	if (!isset($db)) {
		print "<!-- No database -->\n";
		return;
	}

	$q = $db->query($query);
	if (DB::iserror($q)) {
		print '<!-- ' . $q->getMessage() . ' -->' ;
		return;
	}

	if (!($q->numRows() > 0)) {
		print '<!-- No rows returned by query. -->' ;
		return;
	}

	$q->fetchInto($row, DB_FETCHMODE_ASSOC);
	$q->free();
	return $row;
}

function generic_query_as_column($query) {
	global $db;
	if (!isset($db)) {
		print "<!-- No database -->\n";
		return;
	}

	$q = $db->getCol($query);
	if (DB::iserror($q)) {
		print '<!-- ' . $q->getMessage() . ' -->' ;
		return;
	}

	return $q;
}

/* Returns a list of distinct values which are in column $column
* of table $table. Useful for checking whether a user-given (for example,
* via query variables of an URL) indices which are going to be used in
* an SQL query make sense.
*
* Set $order to a column name to order by that column
*/
function get_defined_values($column, $table, $order = "" ) {
	global $db;
	$values = array();
	if (!isset($db)) {
		return $values;
	}

	if (empty($order)) {
		$order = $column;
	}

	$result = $db->query("SELECT DISTINCT ON (" . $order . ") " . $column . " FROM ". $table );
	if (DB::iserror($result)) {
		print '<!-- ' . $result->getMessage() . ' -->' ;
		return;
	}

	while ($row = $result->fetchRow()) {
		$values[] = $row[0];
	}
	$result->free();
	return $values;
}

/* Gets a single value out of the database.
*/
function get_single_value($sqlStatement) {
	global $db;
	if (!isset($db)) {
		print "<!-- get_single_value: Failed to execute ".$sqlStatement.": No database -->\n";
		return "";
	}

	$query = $db->query($sqlStatement);
	if (DB::iserror($query)) {
		print "<!-- get_single_value: Failed to execute ".$sqlStatement.": ".$query->getMessage()." -->\n";
		return "";
	}

	$row = $query->fetchRow();
	$query->free();
	return $row[0];
}
?>
