<?php
# Copyright (C) 2007 by Adriaan de Groot <groot@kde.org>
# Copyright (C) 2007 by Frerich Raabe <raabe@kde.org>
# Copyright (C) 2007 by Allen Winter <winter@kde.org>
#
# This file prints results after they have been read from the DB.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in a file called COPYING; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

print '<table style="clear: right;" class="sortable" id="reportTable" cellspacing="0" border="0" width="100%">'."\n";
print "  <tr>\n";
print '    <th class="underline leftline overline">Subject</th>'."\n";
print '    <th class="underline rightline overline">Issues</th>'."\n";
print "  </tr>\n";
foreach($results as $r) {
	if ($maxIssues>0) {
		$percentage = 100.0 * $r["issues"] / $maxIssues;
	} else  $percentage = 0;

	print "  <tr>\n";
	print '    <td style="padding-left: 0.2cm; border-bottom:1px dashed #C9AF83;">'."\n";
	print '      <a href="'.htmlspecialchars($r["link"]).'">'.htmlspecialchars($r["subject"])."</a>\n";
	if (array_key_exists('extra',$r)) print $r['extra'];
	print "    </td>\n";
	print '    <td width="140px" align="right" class="leftline"'."\n";
	print '        style="border-bottom:1px dashed #C9AF83; padding-right: 0.2cm; background-image:url(\'/img/ratingpng.php?width=140&amp;rating='.$percentage.'\'); background-repeat:repeat-y;">'."\n";
	print "      ".htmlspecialchars($r["issues"])."\n";
	print "    </td>\n";
	print "  </tr>\n";
}
print "</table>\n\n";


if (isset($rssfeed)) {
	print "<p>You can also get the statistics shown on this page as a\n";
	print '<a href="' .$rssfeed . '">RSS feed</a>'."\n";
}
