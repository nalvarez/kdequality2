<!DOCTYPE html
     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>
<?php 
if (!isset($title)) {
	$title="English Breakfast Network";
}
print $title;
?>
</title>
<link rel="stylesheet" type="text/css" title="Normal" href="<?php if (isset($site_root)) print $site_root; ?>style.css" />
<link rel="shortcut icon" href="http://www.englishbreakfastnetwork.org/favicon.ico" type="image/x-icon" />
<script src="/inc/sorttable.js"></script>
<?php
if (isset($rssfeed)) {
	print '<link rel="alternate" type="application/rss+xml" title="RSS for ' . $title .'" href="' . $rssfeed . '" />';
}
?>
</head>
<body>
<div id="title">
<div class="logo">&nbsp;</div>
<div class="header"><h1><a href="/">English Breakfast Network</a></h1>
<p>
<a href="/"><?php if (isset($subtitle)) print $subtitle; else print "Almost, but not quite, entirely unlike tea."; ?></a>
</p>
</div>
</div>
</div>

<div id="content">
<div class="inside">

<?php
include_once('crumbs.inc');
print_breadcrumbs();
?>
