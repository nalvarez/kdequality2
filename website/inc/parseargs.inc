<?php
# Copyright (C) 2007 by Frerich Raabe <raabe@kde.org>
#
# This file interprets the query variables passed in to the
# server and checks them for validity. The variables it interprets
# are 'component', 'module' and 'application'.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in a file called COPYING; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

parse_str($_SERVER['QUERY_STRING'], $queryVars);

# Make sure that the specified component/module/application values are valid!
$definedComponents = get_defined_values("name", "components", "verbose_name");
$haveComponent = isset($queryVars["component"]) &&
	array_search($queryVars["component"], $definedComponents) !== FALSE;

$definedModules = get_defined_values("module", $table_name);
$haveModule = isset($queryVars["module"]) &&
	array_search($queryVars["module"], $definedModules) !== FALSE;	

$definedApplications = get_defined_values("application", $table_name);
$haveApplication = isset($queryVars["application"]) &&
	array_search($queryVars["application"], $definedApplications) !== FALSE;

