<h1>API Documentation Statistics</h1>
<p>The KDE API Reference is generated daily
from up-to-date SVN sources. That's much more often than
the reference guides on developer.kde.org.
As a bonus, the error logs are included so you can
peruse them while sipping some good Ceylon.
Read the <a href="http://developer.kde.org/documentation/library/howto.php">apidox HOWTO</a> and the <a href="http://developer.kde.org/policies/documentationpolicy.php">library documentation policy</a> as well.
</p>
