<?php
include 'inc/header.inc';
include 'inc/db.inc';

print '<p>';

# Pick a random graph to display here. They are regenerated
# by the nightly update scripts and served statically.
$graphs = array(
	'commits-weekdaily.png' => 'Commits per day',
	'commit-who.png' => 'Top 20 committers'
);

$key = array_rand($graphs);
$alt='This week in KDE';
$ttl=$alt . ' - ' . $graphs[$key];

print '<a href="thisweek.php"><img src="img/' . $key . '" alt="' . $alt . '" title="' . $ttl . '" style="float: right; margin: 0px 0px 1ex 1em;" ></a>';
?>
The English Breakfast Network (EBN) is dedicated to
the contemplation of tea,
KDE <a href="apidocs/">API Documentation Validation</a>,
<a href="sanitizer/">User Documentation Validation</a>,
<a href="krazy/">Source Code Checking</a>,
omphaloskepsis, and star-gazing.

<p>
For more information please visit our <a href="about.php">'about this site'</a> page, and don't miss <a href="thisweek.php">'this week in pictures'</a>.
</p>

<p>
The EBN is reminiscent of the research project <a href="http://www.sqo-oss.org/">SQO-OSS</a> on software quality and is operated by <a href="http://blogs.fsfe.org/adridg/">Adriaan de Groot</a> and <a href="http://www.kdedevelopers.org/blog/1451">Allen Winter</a>.
</p>

<div class="figure" style="clear: right; text-align: center; min-height: 0;">
See issues specific to the tool:
[<a href="http://www.englishbreakfastnetwork.org/apidocs/">APIDOX</a>]
[<a href="http://www.englishbreakfastnetwork.org/sanitizer/">Docs Sanitizer</a>]
[<a href="http://www.englishbreakfastnetwork.org/krazy/">Krazy</a>]
</div>

<?php
function summary($version) {
		print '<table width="100%" class="boxed">'."\n";
		print "<tr>\n";
		print '  <th>Area</th><th class="numresult">Issues</th>'."\n";
		print "</tr>\n";
	generic_query("
		SELECT tools.description, tools.path, generations.count
		FROM tools, generations
		WHERE tools.generation = generations.generation AND
			tools.component = (
				SELECT id
				FROM components
				WHERE name = '" . $version . "')
		ORDER BY description",
		'  <tr><td><a href="%path?component=' . $version . '">%description</a></td><td class="numresult">%count</td></tr>'."\n");
	print "</table>\n";
}

$table_name = "modules_and_apps";
include 'inc/parseargs.inc';
if ($haveComponent) {
	$componentName = $queryVars["component"];
	$componentId = get_single_value("SELECT id FROM components WHERE name = '" . $componentName . "'");
	$verboseComponent = get_single_value("SELECT verbose_name FROM components WHERE id = " . $componentId);
}

if (($haveComponent && !$haveModule) or (!$haveComponent)) {
	foreach ($definedComponents as $component) {
		if ( !empty($componentName) && $component != $componentName )
			continue;
		if(preg_match( '/kde-3/', $component) ) # nobody cares about KDE3 anymore
                        continue;
                if (preg_match( '/kde-4\.[0-9]/', $component) ) # we don't show stable branches
			continue;
		if (!$haveComponent)
			$verboseComponent = get_single_value("SELECT verbose_name FROM components WHERE name = '" . $component . "'");
		$description = get_single_value("SELECT description FROM components WHERE name='" . $component . "'");
		$componentId = get_single_value("SELECT id FROM components WHERE name = '" . $component . "'");

		$tool_count = get_single_value("select count(*) from tools where component = " . $componentId );
		if (intval($tool_count) > 0) {
			print "<h1>".$verboseComponent."</h1>";
			#winterz commented description as of 22 Jan 2011
			#if (!empty($description))
			#	print '<p class="abstract">' . $description . '</p>';
			summary($component);
		}
	}
}

if ($haveComponent && $haveModule) {
	$moduleName = $queryVars["module"];
	$selection = "module='" . $moduleName . "'";

	if ($haveApplication) {
		$applicationName=$queryVars["application"];
		$selection = $selection . " AND " .
			"application='" . $applicationName . "'";
	}

	print '<h1>' . $verboseComponent . ' / ' . $moduleName;
	if ($haveApplication) print ' / ' . $applicationName;
	print '</h1>';
	print '<table width="100%" class="boxed">'."\n";
	print "<tr>\n";
	print '  <th>Area</th><th class="numresult">Issues</th>'."\n";
	print "</tr>\n";


	$tools = array(
		array("dox","results_apidox"),
		array("sanitizer","results_sanitizer"),
		array("krazy","results_krazy")
		);

	foreach ($tools as $tool) {
		$toolName = $tool[0];
		$resultsTable = $tool[1];

		$result = generic_query_as_result("SELECT id,description,generation FROM tools WHERE name='" . $toolName . "' AND component = " . $componentId);

		$toolId = $result["id"];
		$toolVerbose = $result["description"];
		$toolGeneration = $result["generation"];
		$toolCount = -1;

		if ($toolId > 0) {
			$toolCount = get_single_value("SELECT SUM(issues) FROM " . $resultsTable . " WHERE generation=" . $toolGeneration . " AND " . $selection );

		}

		if ( ($toolId > 0 ) and ($toolCount>=0) and (!empty($toolCount))) {
			print "<tr><td>$toolVerbose</td><td class='numresult'>$toolCount</td></tr>\n";
		}
	}


	print '</table>';
}

include 'inc/footer.inc';
?>
