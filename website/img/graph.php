<?php
# Copyright (C) 2007 by Adriaan de Groot <groot@kde.org>
#
# This file generates a graph of the last ten results from the
# database for a given tool ID.

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program in a file called COPYING; if not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

include "db.inc" ;

# Get a tool flavor if given, default to 1
$flavor = 1;
if (array_key_exists('f',$_GET)) {
	$flavor = $_GET['f'];
}
if (!is_numeric($flavor)) {
	$flavor = 1;
}

$component = 0;
$component = get_single_value("SELECT component FROM tools WHERE id=" . $flavor);

# Get a table name if given and valid
if ($component > 0 and array_key_exists('t',$_GET)) {
	$valid_tables = array("results_apidox"=>1,
		"results_sanitizer"=>1,
		"results_krazy"=>1);
	if (array_key_exists($_GET['t'],$valid_tables)) {
		$table = $_GET['t'];
	}
}

# Get module name if given
if ( isset($table) and array_key_exists('m',$_GET) ) {
	$module=$_GET['m'];
}
    
if ( isset($table) and isset($module) ) {
	$definedModules = get_defined_values("module", $table);
	$haveModule = array_search($module, $definedModules) !== FALSE;

	# If the table and module are given, just pull in those results
	if ($haveModule) {
		$q = $db->getAll( "
		SELECT generation, sum AS count, timestamp 
		FROM (
			SELECT generation, module, SUM(issues) 
			FROM $table 
			WHERE component=$component AND module='$module' 
			GROUP BY module, generation 
			ORDER BY generation 
			DESC LIMIT 10
		) AS foo 
		NATURAL JOIN generations",DB_FETCHMODE_ASSOC);
	}
}

if (! isset($q)) {
	# Default query gets recent results for the tool, not module
	$q = $db->getAll("
		SELECT generation,count,timestamp 
		FROM generations 
		WHERE tool=" . $flavor . "
		ORDER BY generation DESC LIMIT 10", DB_FETCHMODE_ASSOC);
}

if (!is_array($q)) {
	return;
}

$image = imagecreate(200,110);
$back = ImageColorAllocate($image, 255, 255, 255 );
$fill = ImageColorAllocate($image, 255, 225, 173 );
$black = ImageColorAllocate($image, 0,0,0 );
$font = "./benjamingothic.ttf";

# Find maximum error count
$c = count($q);
$max = $q[0]['count'];
$min = $max;
for ($i=$c-1; $i>=0; $i--) {
	if ($q[$i]['count'] > $max) $max = $q[$i]['count'];
	if ($q[$i]['count'] < $min) $min = $q[$i]['count'];
}
if ($min == $max) {
	$min = 0;
}

if (!isset($min) or empty($min)) {
	$min=0;
}

imagettftext($image, 6, 0, 3, 12, $black, $font, $max);
imagettftext($image, 6, 0, 3, 90, $black, $font, $min);

# Draw bars for each count, with label showing generation.
$donemax = FALSE;
for ($i=$c-1; $i>=0; $i--) {
	if ($max-$min > 0) {
		$top = 5+80*($max- $q[$i]['count'])/($max-$min);
	} else {
		# Act as if $max-min / $max-min = 1 when divide by zero
		$top = 85;
	}
	$left = 30 + (($c-1)-$i) * 170 / $c;
	$right = 30 + ($c-$i) * 170 / $c;
	ImageFilledRectangle($image, $left, 100, $right, $top,$fill);
	imageline($image, $left, $top, $right, $top, $black);
	if (!isset($lasttop)) {
		$lasttop=$top;
	}
	imageline($image, $left, $lasttop, $left, $top, $black);
	$lasttop = $top;
}

# Just rearrange the bits of the (asctime) timestamp string
function frob_tstamp($t) {
	$space = substr($t,0,3) . substr(strstr($t," "),0,8);
	return $space;
}

$tstamp = frob_tstamp($q[$c-1]['timestamp']);
imagettftext($image, 6, 0, 30, 100, $black, $font, $tstamp);

$tstamp = frob_tstamp($q[0]['timestamp']);
$box = imagettfbbox(6, 0, $font, $tstamp);
imagettftext($image, 6, 0, 198 - $box[4], 100, $black, $font, $tstamp);

header("Content-Type: image/x-png");
imagePNG($image);
imagedestroy($image);

?>
