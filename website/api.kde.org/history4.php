<?php
  $page_title = "API Reference Index";
  include 'header.inc';
?>

<p>
Reference guides for older KDE 4 releases
</p>

<table>
<tr>
<th style="text-align:center" width="25%">
KDE 4.0
</th>
<th style="text-align:center" width="25%">
KDE 4.1
</th>
<th style="text-align:center" width="25%">
KDE 4.2
</th>
<th style="text-align:center" width="25%">
KDE 4.3
</th>
</tr>
<tr>
<?php
  function apidox($v,$module) {
    print '<li><a href="' . $v . '-api/' . $module . '.tar.gz">';
    print '<img src="/img/14x14save.png" alt="[download]" title="Download ' . $module . ' APIDOX tarball."></a>';
    print '&nbsp;<a href="' . $v . '-api/' . $module . '-apidocs/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a></li>\n\n";
  }

  function apidox2($v,$module) {
    print '<li><a href="' . $v . '-api/' . $v . '-apidocs/' . $module . '/html" title="View online APIDOX for ' . $module . '">';
    print $module . "</a></li>\n\n";
  }

  function apidox3($v,$module) {
    print '<li><a href="' . $v . '-api/' . $module . '-apidocs/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a></li>\n\n";
  }

  function apidox4($v,$module) {
    print '<li><a href="' . $v . '-api/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a></li>\n\n";
  }

?>
<td valign="top">
<ul li style="list-style-type: none;">
<?php
  apidox('4.0','kdelibs');
  apidox('4.0','kdepimlibs');
  apidox('4.0','kdebase-runtime');
  apidox('4.0','kdebase-workspace');
  apidox('4.0','kdebase-apps');
  apidox('4.0','kdeedu');
  apidox('4.0','kdegames');
  apidox('4.0','kdegraphics');
  apidox('4.0','kdemultimedia');
  apidox('4.0','kdenetwork');
  apidox('4.0','kdesdk');
  apidox('4.0','kdeutils');
?>
</ul>
</td>
<td valign="top">
<ul li style="list-style-type: none;">
<?php
  apidox('4.1','kdelibs');
  apidox('4.1','kdepimlibs');
  apidox('4.1','kdebase-runtime');
  apidox('4.1','kdebase-workspace');
  apidox('4.1','kdebase-apps');
  apidox('4.1','kdeedu');
  apidox('4.1','kdegames');
  apidox('4.1','kdegraphics');
  apidox('4.1','kdemultimedia');
  apidox('4.1','kdenetwork');
  apidox('4.1','kdepim');
  apidox('4.1','kdeplasma-addons');
  apidox('4.1','kdesdk');
  apidox('4.1','kdeutils');
  apidox('4.1','kdewebdev');
  apidox4('pykde-4.1','PyKDE');
?>
</ul>
</td>
<td valign="top">
<ul li style="list-style-type: none;">
<?php
  apidox('4.2','kdelibs');
  apidox('4.2','kdepimlibs');
  apidox('4.2','kdebase-runtime');
  apidox('4.2','kdebase-workspace');
  apidox('4.2','kdebase-apps');
  apidox('4.2','kdeedu');
  apidox('4.2','kdegames');
  apidox('4.2','kdegraphics');
  apidox('4.2','kdemultimedia');
  apidox('4.2','kdenetwork');
  apidox('4.2','kdepim');
  apidox('4.2','kdeplasma-addons');
  apidox('4.2','kdesdk');
  apidox('4.2','kdeutils');
  apidox('4.2','kdewebdev');
  apidox4('pykde-4.2','PyKDE');
?>
</ul>
</td>
<td valign="top">
<ul li style="list-style-type: none;">
<?php 
  apidox('4.3','kdelibs');
  apidox('4.3','kdepimlibs');
  apidox('4.3','kdebase-runtime');
  apidox('4.3','kdebase-workspace');
  apidox('4.3','kdebase-apps');
  apidox('4.3','kdeedu');
  apidox('4.3','kdegames');
  apidox('4.3','kdegraphics');
  apidox('4.3','kdemultimedia');
  apidox('4.3','kdenetwork');
  apidox('4.3','kdepim');
  apidox('4.3','kdeplasma-addons');
  apidox('4.3','kdesdk');
  apidox('4.3','kdeutils');
  apidox('4.3','kdewebdev');
  apidox4('pykde-4.3','PyKDE');
?> 
</ul>
</td>
</tr>

</table>

<table>
<tr>
<th style="text-align:center" width="25%">
KDE 4.4
</th>
<th style="text-align:center" width="25%">
KDE 4.5
</th>
<th style="text-align:center" width="25%">
KDE 4.6
</th>
<th style="text-align:center" width="25%">
KDE 4.7
</th>
</tr>

<tr>
<td valign="top">
<ul li style="list-style-type: none;">
<?php
  apidox('4.4','kdelibs');
  apidox('4.4','kdepimlibs');
  apidox('4.4','kdebase-runtime');
  apidox('4.4','kdebase-workspace');
  apidox('4.4','kdebase-apps');
  apidox('4.4','kdeedu');
  apidox('4.4','kdegames');
  apidox('4.4','kdegraphics');
  apidox('4.4','kdemultimedia');
  apidox('4.4','kdenetwork');
  apidox('4.4','kdepim');
  apidox('4.4','kdeplasma-addons');
  apidox('4.4','kdesdk');
  apidox('4.4','kdeutils');
  apidox('4.4','kdewebdev');
  apidox4('pykde-4.4','PyKDE');
?>
</ul>
</td>
<td valign="top">
<ul li style="list-style-type: none;">
<?php
  apidox('4.5','kdelibs');
  apidox('4.5','kdepimlibs');
  apidox('4.5','kdebase-runtime');
  apidox('4.5','kdebase-workspace');
  apidox('4.5','kdebase-apps');
  apidox('4.5','kdeedu');
  apidox('4.5','kdegames');
  apidox('4.5','kdegraphics');
  apidox('4.5','kdemultimedia');
  apidox('4.5','kdenetwork');
  apidox('4.5','kdepim');
  apidox('4.5','kdeplasma-addons');
  apidox('4.5','kdesdk');
  apidox('4.5','kdeutils');
  apidox('4.5','kdewebdev');
  apidox4('pykde-4.5','PyKDE');
?>
</ul>
</td>
<td valign="top">
<ul li style="list-style-type: none;">
<?php
  apidox('4.6','kdelibs');
  apidox('4.6','kdepimlibs');
  apidox('4.6','kdebase-runtime');
  apidox('4.6','kdebase-workspace');
  apidox('4.6','kdebase-apps');
  apidox('4.6','kdeedu');
  apidox('4.6','kdegames');
  apidox('4.6','kdegraphics');
  apidox('4.6','kdemultimedia');
  apidox('4.6','kdenetwork');
  apidox('4.6','kdepim');
  apidox('4.6','kdeplasma-addons');
  apidox('4.6','kdesdk');
  apidox('4.6','kdeutils');
  apidox('4.6','kdewebdev');
  apidox4('pykde-4.6','PyKDE');
?>
</ul>
</td>
<td valign="top">
<ul li style="list-style-type: none;">
<?php
  apidox('4.7','kdelibs');
  apidox('4.7','kdepimlibs');
  apidox('4.7','kdebase-runtime');
  apidox('4.7','kdebase-workspace');
  apidox('4.7','kdebase-apps');
  apidox('4.7','kdeedu');
  apidox('4.7','kdegames');
  apidox('4.7','kdegraphics');
  apidox('4.7','kdemultimedia');
  apidox('4.7','kdenetwork');
  apidox('4.7','kdepim');
  apidox('4.7','kdeplasma-addons');
  apidox('4.7','kdesdk');
  apidox('4.7','kdeutils');
  apidox('4.7','kdewebdev');
  apidox4('pykde-4.7','PyKDE');
?>
</ul>
</td>
</tr>

</table>

<?php include 'footer.inc' ?>
