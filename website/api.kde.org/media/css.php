<?php
header("Content-type: text/css");

//  This script accepts the following URL parameters:
//  ?color=<colorname> or ?color=%23<hexadecimal> - Text color
//  ?background=<colorname> or ?background=%23<hexadecimal> - Background color
//  ?link=<colorname> or ?link=%23<hexadecimal> - link color
//  ?color&background&link - use browser colors only
//  ?mode=print - print stylesheet
//  ?mode=flat - show menu at the bottom, not at the left
//  ?site-image=<URL> - use a customised image in the header
//  ?css-inc=<path on this domain> - path to include a custom css.inc file

// New-web-style colors:
$white =      '#ffffff';
$grey_f =     '#f9f9f9';
$orange =     '#f7800a';
$light_grey = '#eeeeee';
$grey_d =     '#dddddd';
$dark_orange ='#cf4913';
$grey_c =     '#cccccc';
$mid_grey =   '#888888';
$middle_red = '#800000';
$dark_grey =  '#535353';
$light_blue = '#4d88c3';
$blue_grey =  '#2f6fab';
$blue =       '#0057ae';
$grape_violet3 = '#644A9B';
$black =      '#000000';

?>
/*
** HTML elements
*/

body {
    margin: 0;
    padding: 0;
    text-align: center;
    font-size: 0.8em;
    font-family: "Bitstream Vera Sans", "Lucida Grande", "Trebuchet MS", sans-serif;
    <?php color ( $dark_grey )?>
    <?php background ( $white )?>
}


/*
** HTML Tags
*/

h1, h2, h3, h4
{
    padding: 0;
    text-align: left;
    font-weight: bold;
    <?php color ( $orange )?>
    <?php background ('transparent')?>
}
h1 {
    margin: 0 0 0.3em 0;
    font-size: 1.7em;
}
h2, h3, h4 {
    margin: 1.3em 0 0 0.3em
}

h2 {
    font-size: 1.5em;
}

h3 {
    font-size: 1.4em;
}

h4 {
    font-size: 1.3em;
}

h5 {
    font-size: 1.2em;
}

a:link {
    padding-bottom: 0;
    text-decoration: none;
    <?php linkcolor ( $blue )?>
}

a:visited {
    padding-bottom: 0;
    text-decoration: none;
    <?php linkcolor ( $grape_violet3 )?>
}


a[href]:hover {
    text-decoration: underline;
}

hr {
    margin: 0.3em 1em 0.3em 1em;
    height: 1px;
    <?php border ( $grey_d, 'dashed')?>
    border-width: 0 0 1px 0;
}

pre {
    display: block;
    margin: 0.3em;
    padding: 0.3em;
    font-size: 1em;
    font-family: Courier, 'Courier New', 'Andale Mono', Monaco, monospace;
    <?php color ( $black )?>
    <?php background ( $grey_f )?>
    <?php border( $blue_grey, 'dashed' )?>
    border-width: 1px;
    overflow: auto;
    line-height: 1.1em;
}

input, textarea, select {
    margin: 0.2em;
    padding: 0.1em;
    <?php color ( $mid_grey )?>
    <?php background ( $white )?>
    border: 1px solid;
}

blockquote {
    margin: 0.3em;
    padding-left: 2.5em;
    <?php background ('transparent')?>
}

del {
    <?php color ( $middle_red ) ?>
    text-decoration: line-through;
}

dt {
    font-weight: bold;
    font-size: 1.05em;
    <?php color ( $blue )?>
}

dd {
    margin-left: 1em;
}

p {
    margin-top: 0.5em;
    margin-bottom: 0.9em;
    text-align: justify;
}
fieldset {
    <?php border ( $grey_c, '1px solid')?>
}

li {
    text-align: left;
}

fieldset {
    margin-bottom: 1em;
    padding: .5em;
}

form {
    margin: 0;
    padding: 0;
}

hr {
    height: 1px;
    <?php border ( $mid_grey, '1px solid')?>
    <?php background ( $mid_grey )?>
    margin: 0.5em 0 0.5em 0 ;
}

img {
    border: 0;
}
table {
    border-collapse: collapse;
    font-size: 1em;
}
th {
    text-align: left;
    padding-right: 1em;
    <?php border ( $grey_c, 'solid')?>
    border-width: 0 0 3px 0;
}


/*
** Header
*/
#header {
    <?php width ('100%')?>
    <?php color ( $dark_grey )?>
    <?php background ( $light_grey )?>
}

#header_top {
    margin: 0 auto;
    padding: 0;
    <?php width ('950px')?>
    vertical-align: middle;
    <?php color ( $white )?>
    <?php background ('url(/media/images/top.jpg) repeat-x bottom',  $blue )?>
}

#header_top div {
    margin: 0 auto;
    padding: 0;
    <?php background ('url(/media/images/top-left.jpg) no-repeat bottom left')?>
}

#header_top div div {
    margin: 0 auto;
    padding: 0;
    vertical-align: middle;
    text-align: left;
    font-size: 1.7em;
    font-weight: bold;
    <?php
        if (isset ($_GET ['site-image']))
            background ('url('.$_GET ['site-image'].') no-repeat bottom right');
        else
            background ('url(/media/images/top-right.jpg) no-repeat bottom right');
    ?>
}

#header_top div div img {
    margin:8px 0px 9px 18px;
    vertical-align: middle;
}

#header_bottom {
    <?php noprint()?>
    margin: 0 auto;
    padding: 0.1em 0em 0.3em 0;
    <?php width ('60em')?>
    <?php minWidth ('770px')?>
    <?php maxWidth ('45em')?>
    vertical-align: middle;
    text-align: right;
    <?php background ( $light_grey )?>
}

#location {
    padding: 0 0 0 1.5em;
    text-align: left;
    line-height: normal;
    font-size: 1.1em;
    font-weight:bold;
    <?php float ('left')?>
}

#location ul {
    display: inline;
    margin: 0;
    padding: 0;
    list-style: none;
}

#location ul li {
    display: inline;
    white-space : nowrap;
    margin: 0;
    padding: 0 1em 0 0;
}

#menu {
    margin: 0 1.5em 0 0;
    text-align: right;
    line-height: normal;
    font-size: 1.1em;
    font-weight: bold;
}

#menu ul {
    display: inline;
    list-style: none;
    margin: 0;
    padding: 0;
    text-align: right;
}

#menu ul li {
    display: inline;
    white-space : nowrap;
    margin: 0;
    padding: 0 0 0 1em;
    text-align: right;
}

.here a:link, .here a:visited {
    text-decoration:underline;
}

.here ul a:link, .here ul a:visited {
    text-decoration:none;
}

#menu ul li a {
    font-weight: bold;
}


/*
** Content
*/

#content {
    <?php width ('100%')?>
}

#main {
    /* padding in px not ex because IE messes up 100% width tables otherwise */
    padding: 10px;
    text-align: left;
}

#body_wrapper {
    margin: 0 auto;
    <?php width ('950px')?>
    <?php border ($grey_d, 'solid')?>
    border-width: 0 0 0 1px;
}

#body {
    <?php float ('left')?>
    margin: 0;
    padding: 0;
    <?php minHeight ('40em')?>
    <?php width ('950px')?>
}

#right {
    <?php float ('right')?>
    margin: 0;
    padding: 0;
    <?php width ('73%')?>
}

/* These are date cells on the front page. */
td.cell_date {
    <?php width('8em') ?>
    <?php minWidth('8em') ?>
}

#hotspot, .sideimage {
    <?php float ('right')?>
    margin: 1ex 1em;
    clear: right;
}

.belowimage, h1, h2, h3, h4, h5 {
    clear: right;
}


/*
** Left Menu
*/

#left {
    <?php noprint()?>
    <?php float ('left')?>
    margin: 0;
    padding: 0;
    <?php width ('20%')?>
}

.menu_box {
    padding: 0.7em 0 0 0;
}

.menu_box ul {
    text-align: left;
}

.menu_box li {
    list-style-type: none;
    text-align: left;
    margin-left: 0.1em;
}

.menu_box ul ul {
margin: 0;
padding-left: 0;
}

.menu_box li li {
margin-left: 1em;
}

.menu_box .active{
    <?php color ( $dark_orange )?>
}

.menutitle {
    margin: 0.6em 0 1.2em 0;
    padding:0;
    <?php color ( $white )?>
    <?php background ('url(/media/images/block_title_mid.png) repeat-y right',  $blue )?>
}
.menutitle div {
    margin: 0;
    padding:0;
    <?php background ('url(/media/images/block_title_top.png) no-repeat top right')?>
}
.menutitle div h2 {
    margin: 0;
    padding: 0.2em 0 0.3em 1.3em;
    line-height:1.2em;
    font-size: 120%;
    font-weight: normal;
    <?php color ( $white )?>
    <?php background ('url(/media/images/block_title_bottom.png) no-repeat bottom right')?>
}

.clearer {
    clear: both;
    height: 1px;
}


/*
** Footer
*/
#footer {
    <?php noprint()?>
    <?php width ('100%')?>
    <?php background ( $light_grey )?>
}

#footer_text {
    margin: 0 auto;
    padding: 1em 0 1em 3.5em;
    <?php width ('51.5em')?>
    text-align: left;
    <?php color ( $black )?>
    <?php background ( $light_grey )?>
}

#footer a:link, #footer a:visited {
    <?php linkcolor ( $light_blue )?>
}

#quicklinks {
font-size: 1em;
padding: 1em;
text-align: center;
margin-top: 0.5em;
margin-left: 0.2em;
margin-bottom: 0.5em;
margin-right: 0.2em;
}

.content .contents {
	text-align: left;
}

<?php

if (isset ($_GET ['css-inc']) && file_exists ($_SERVER ['DOCUMENT_ROOT'].$_GET ['css-inc'].'/css.inc'))
    include $_SERVER ['DOCUMENT_ROOT'].$_GET ['css-inc'].'/css.inc';

function color ($color) {
    if (! isset ($_GET ['color']))
        echo "color: ".$color.";\n";
    elseif ($_GET ['color'])
        echo "color: ".$_GET ['color'].";\n";
    else
       echo "color: WindowText;\n";
}

function background ($background, $color = false) {
    if (! isset ($_GET ['background'])) {
        echo "background: ".$background.";\n";
        if ($color)
            echo "background-color: ".$color.";\n";
    }
    elseif ($_GET ['background'])
        echo "background-color: ".$_GET ['background'].";\n";
    else
       echo "background: Window;\n";
}

function border ($color, $other = '') {
    if (! isset ($_GET ['color']))
        echo "border: ".$color." ".$other.";\n";
    else
        echo "border: ".$_GET ['color']." ".$other.";\n";
}

function linkcolor ($color) {
    if (! isset ($_GET ['link']))
        echo "color: ".$color.";\n";
    elseif ($_GET ['link'])
        echo "color: ".$_GET ['link'].";\n";
}

function noprint () {
    if (isset ($_GET ['mode']) && $_GET ['mode'] == "print")
        echo "display: none;\n";
}

function noflat () {
    if (isset ($_GET ['mode']) && $_GET ['mode'] == "flat")
        echo "display: none;\n";
}

function float ($float) {
    if (! isset ($_GET ['mode']) || $_GET ['mode'] != "flat")
        echo "float: ".$float.";\n";
}

function width ($width) {
    if (! isset ($_GET ['mode']) || $_GET ['mode'] == "normal")
        echo "width: ".$width.";\n";
}

function minWidth ($minwidth) {
    if (! isset ($_GET ['mode']) || $_GET ['mode'] == "normal")
        echo "min-width: ".$minwidth.";\n";
}

function maxWidth ($maxwidth) {
    if (! isset ($_GET ['mode']) || $_GET ['mode'] == "normal")
        echo "max-width: ".$maxwidth.";\n";
}

function minHeight ($minHeight) {
    if (! isset ($_GET ['mode']) || $_GET ['mode'] == "normal")
        echo "min-height: ".$minHeight.";\n";
}

?>
