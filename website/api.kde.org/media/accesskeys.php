<?php
$page_title = "Access Keys within KDE.org";
$site_root = "../";
$site_menus = 0;
$site_external = true;
include "header.inc";
?>

<p>Throughout all KDE sites based on the new layout, the following access keys are defined:</p>
<ul>
 <li>1: Home</li>
 <li>2: Skip to Content</li>
 <li>3: Sitemap</li>
 <li>4: Skip to Search Field</li>
 <li>5: Skip to Link Menu</li>
 <li>6: Help</li>
 <li>7: Website Settings</li>
 <li>8: KDE Home</li>
 <li>9: KDE Accessibility Home</li>
 <li>0: Description of Access Keys</li>
</ul>
<p>
 Browsers capable of using access keys are currently Konqueror, Mozilla, Netscape and
 Internet Explorer. To use them, you can type [Ctrl][Digit], [Alt][Digit], or
 [Alt][Shift][Digit], depending on your browser.
</p>
<p>
If you have more questions about accessibility related issues, please have a look at the
<a href="http://accessibility.kde.org/">KDE Accessibility Project Homepage</a>.
</p>

<?php
  if (isset ($_SERVER['HTTP_REFERER']) && ($_SERVER['HTTP_REFERER'] != ""))
    print '<a href="'.$_SERVER['HTTP_REFERER'].'">Back to previous page</a>';
?>

<?php
include "footer.inc";
?>
