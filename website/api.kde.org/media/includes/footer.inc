<?php 
if (! isset ($showedit) || $showedit == true) {
    echo '<p class="cp-edit">[ <a rel="nofollow" href="';
    echo basename ($_SERVER ['PHP_SELF']) . "?edit";
    echo '">';
    i18n("Edit");
    echo '</a> ]</p>';
}

if (isset ($templatepath))
    include $templatepath."template-bottom1.inc";
else
    include "template-bottom1.inc";

if ($site_menus > 0) {
    if (isset($site) and ($site == "developer"))
    {
        echo $template_menulist1."\n";
        $kde_menu->currentMenu();
        echo $template_menulist3."\n";

        if ($kde_menu->activeSection () != 0) {
            echo $template_menulist1."\n";
            $kde_menu->currentMenu(true);
            echo $template_menulist3."\n";
        }
    }
    else
        $kde_menu->show();
}

?>
<div class="cp-doNotDisplay">
  <h2><?php i18n ('Global navigation links')?></h2>
  <ul>
    <li><a href="http://www.kde.org/" accesskey="8"><?php i18n("KDE Home")?></a></li>
    <li><a href="http://accessibility.kde.org/" accesskey="9"><?php i18n("KDE Accessibility Home")?></a></li>
    <li><a href="/media/accesskeys.php" accesskey="0"><?php i18n("Description of Access Keys")?></a></li>
    <li><a href="#cp-content"><?php i18n("Back to content")?></a></li>
    <li><a href="#cp-menu"><?php i18n("Back to menu")?></a></li>
  </ul>
</div>

<?php
if (isset ($templatepath))
    include $templatepath."template-bottom2.inc";
else
    include "template-bottom2.inc";

?>
</body>
</html>
