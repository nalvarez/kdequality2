<?php
$site_title = "KDE::Enterprise Homepage";
$site_external = true;
$name="enterprise.kde.org Webmaster";
$mail="webmaster@kde.org";

function P($arg) { echo htmlspecialchars($arg); }
function S($arg) { return mysql_real_escape_string($arg); }

function POST($arg)
{
   if (get_magic_quotes_gpc())
      return stripslashes($_POST[$arg]);
   return $_POST[$arg];
}

function OPTIONS($options, $selected)
{
   $count = count($options);
   for ($i = 0; $i < $count; $i++) {
      if ($selected == $options[$i])
         echo "<option selected=\"selected\">";
      else
         echo "<option>";
      echo "$options[$i]</option>\n";
   }
}

function CHECKBOX($selected)
{
   if ($selected)
      echo " checked=\"checked\" ";
}

function EXTERNAL_LINK($url, $caption)
{
  if ($url)
     echo '<a href="' . htmlspecialchars($url) . '" target="_blank">';
  P($caption);
  if ($url)
     echo '</a>';
}

?>
