<?php
/*
 * Filename: 	functions.php
 * Description: supplies functions for the KDE Homepage.
 * Functions: 	printNews(items)
 * Author: 	Sebastian Faubel
 * Modified by: Jason Bainbridge and Chris Howells
 * Comments: Added some caching code for the rdf generation.
 * NB. Those functions should be merged together at sometime
 *     for the sake of cleaner code
 *	   + the html needs to be cleaned up
 */

include_once("classes/class_rdf.inc");
include_once("classes/class_faq.inc");
include_once("classes/class_image_gallery.inc");
include_once("classes/class_appinfo.inc");
include_once("classes/class_hotspot.inc");

function setup_site ()
{
  global $site_root;
  global $document_root;
  global $url_root;
  global $current_relativeurl;

  // start with empty site_root...
  $site_root = "";

  // start with values queried from php...
  $document_root = getcwd();
  $url_root = dirname($_SERVER['PHP_SELF']);

  // search for site.inc, add hard limit here, just to be 1000% sure this will finish...
  // each site must have a site.inc, really, or be in it's own documentroot, both will work!
  $i = 0;
  while ($i < 32)
  {
    // webroot reached
    if ($url_root == "" || $url_root == "/")
      break;

    // site.inc found, root...
    if (file_exists ("$document_root/site.inc"))
      break;

    $document_root = dirname ($document_root);
    $url_root = dirname ($url_root);
    $site_root .= "../";

    $i++;
  }

  // if no site_root, prefix at least with ., else, remove the trailing /
  if ($site_root == "")
    $site_root = ".";
  else
    $site_root = substr ($site_root, 0, strlen($site_root) - 1);

  // no trailing / in url_root
  if ($url_root == "/")
    $url_root = "";

  // calculate the url of the current file relative to $menu_baseurl;
  $current_relativeurl = substr ($_SERVER['PHP_SELF'], strlen($url_root), strlen($_SERVER['PHP_SELF']) - strlen($url_root));
  if ($current_relativeurl[0] == "/")
    $current_relativeurl = substr ($current_relativeurl, 1, strlen($current_relativeurl));
}

function kde_general_news ($file, $items, $summaryonly)
{
  global $site_locale;
  startTranslation($site_locale);

  if ($summaryonly)
    print "<h2 id=\"news\">" . i18n_var("Latest News") . "</h2>\n";

  $news = new RDF();
  $rdf_pieces = $news->openRDF($file);

  if(!$items)
  {
     $items = 5; // default
  }
  $rdf_items = count($rdf_pieces);
  if ($rdf_items > $items)
  {
     $rdf_items = $items;
  }

  //only open the file if it has something in it
  if ($rdf_items > 0)
  {
    /* Don't display the last story twice
    * if there is less than the requested number of stories
    * in the RDF file */
    if ($rdf_items < $items)
    {
      $rdf_items = $rdf_items - 1;
    }

   $alternate = false;

    print "<table class=\"table_box\" cellpadding=\"6\">\n";

    if ($summaryonly)
      print "<tr><th>" . i18n_var("Date") . "</th>\n<th>" . i18n_var("Headline") . "</th></tr>\n";

    $prevDate = "";
    for($x=1;$x<=$rdf_items;$x++)
    {
      $alternate = !$alternate;
      if ($alternate)
      {
        $color = "newsbox1";
      }
      else
      {
        $color = "newsbox2";
      }
      ereg("<title>(.*)</title>", $rdf_pieces[$x], $title);
      ereg("<date>(.*)</date>", $rdf_pieces[$x], $date);
      ereg("<fullstory>(.*)</fullstory>", $rdf_pieces[$x], $fullstory);
      print "<tr>\n";

      $printDate = $date[1];

      $title[1] = utf8_encode($title[1]);

      if ($summaryonly)
      {
        print "<td valign=\"top\" class=\"$color\">".(($printDate == $prevDate) ? "&nbsp;" : "<b>$printDate</b>")."</td>\n";
        print "<td class=\"$color\"><a href=\"news.php#item" . ereg_replace("[^a-zA-Z0-9]", "", $title[1]) . "\">$title[1]</a></td>\n";
        $prevDate = $printDate;
      }
      else
      {
        print "<td class=\"newsbox2\"><h3><a name=\"item" . ereg_replace("[^a-zA-Z0-9]", "", $title[1]) . "\">$printDate: $title[1]</a></h3></td>\n";
        print "</tr><tr><td class=\"newsbox2\">$fullstory[1]</td>\n";
      }

      print "</tr>\n";

    }
    print "</table>\n";
  }
}

function begin_under_construction()
{
  if ($_SERVER["QUERY_STRING"] != "test")
    print("<!-- Still Under Construction\n");
}

function end_under_construction()
{
  if ($_SERVER["QUERY_STRING"] != "test");
    print("     Still Under Construction -->\n");
}

function startTranslation($dictionary)
{
  global $site_root;
  global $site_external;
  $topleveldir = explode("/", $_SERVER['REQUEST_URI']);

  if ($site_root == ".") // if the page is not under a sub-directory
  {
    if (($site_external && ($_SERVER['SERVER_NAME'] != "kde.org")) || !$site_external) // if the page is something like kde.org or konqueror.org
    {
      $dir_file = $site_root . "/media/includes/i18n/" . $dictionary . "/" . "root.inc";
      $media_file = $site_root . "/media/includes/i18n/" . $dictionary . "/" . "media.inc";
    }
    else // something like kde.org/apps/konqueror or kde.org/areas/kde-ev
    {
      $dir_file = "../../" . $site_root . "/media/includes/i18n/" . $dictionary . "/" . "root.inc";
      $media_file = "../../" . $site_root . "/media/includes/i18n/" . $dictionary . "/" . "media.inc";
    }
  }
  else
  {
    if (($site_external && ($_SERVER['SERVER_NAME'] != "kde.org")) || !$site_external) // if the page is something like kde.org or konqueror.org
    {
      $dir_file = $site_root . "/media/includes/i18n/" . $dictionary . "/" . $topleveldir[1] . ".inc";
      $media_file = $site_root . "/media/includes/i18n/" . $dictionary . "/" . "media.inc";
    }
    else // someting like kde.org/apps/konqueror
    {
      $dir_file = "../../" . $site_root . "/media/includes/i18n/" . $dictionary . "/" . $topleveldir[1] . ".inc";
      $media_file = "../../" . $site_root . "/media/includes/i18n/" . $dictionary . "/" . "media.inc";
    }
  }

  if ($dictionary != "en")
  {
    global $text; //needed!
    if (file_exists($media_file))
    {
      include($media_file);
    }
    if (file_exists($dir_file))
    {
      include($dir_file);
    }
  }
}

function i18nDebug()
{
	global $text;
	print "text contains " . count($text) . " items \n";
}

function i18n_var($translate)
{
	global $text;
	global $site_locale;
	if ($site_locale == "en")
	{
		return $translate;
	}
	else
	{
		$translated = $text[$translate];
		if ($translated == "")
		{
			return $translate;
		}
		else
		{
			return $translated;
		}
	}
}

function i18n($translate)
{
	global $text;
	global $site_locale;
	if ($site_locale == "en")
	{
		print $translate;
	}
	else
	{
		$translated = $text[$translate];
		if ($translated == "")
		{
			print $translate;
		}
		else
		{
			print $translated;
		}
	}
}

function niceFileSize($path)
{
	if (file_exists($path))
	{
		$size = filesize($path);
		if ($size > (1024 * 1024))
		{
			echo "(" . round($size/(1024*1024)) . "MB)";
		}
		else
		if ($size > 1024)
		{
			echo "(" . round($size/1024) . "KB)";
		}
		else
		{
			echo "(" . round($size) . "B)";
		}
	}
	else
	{
		echo "(File not available on this server, use <a href=\"http://printing.kde.org\">printing.kde.org</a> or <a href=\"http://www.koffice.org\">koffice.org</a> instead)";
	}
}

function siteLogo($path, $width, $height)
{
	return "src=\"$path\" width=\"$width\" height=\"$height\"";
}

?>
