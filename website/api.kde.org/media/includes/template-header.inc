<?php // This file contains content for the html header
?>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="Content-Style-Type" content="text/css" />
  <link rel="meta" href="http://www.kde.org/labels.rdf" type="application/rdf+xml" title="ICRA labels" />
  <meta name="trademark" content="KDE e.V." />
  <meta name="description" content="K Desktop Environment Homepage, KDE.org" />
  <meta name="MSSmartTagsPreventParsing" content="true" />
  <meta name="robots" content="all" />
  <meta name="no-email-collection" content="http://www.unspam.com/noemailcollection" />
  <link rel="shortcut icon" href="/media/images/favicon.ico" />
<?php
  if (isset($rss_feed_link))
    print "<link rel=\"alternate\" type=\"application/rss+xml\" title=\"$rss_feed_title\" href=\"$rss_feed_link\" />\n";
  if (isset($rss_feed2_link))
    print "<link rel=\"alternate\" type=\"application/rss+xml\" title=\"$rss_feed2_title\" href=\"$rss_feed2_link\" />\n";
?>

<?php
  if (isset($kde_stylesheets))
    $kde_stylesheets->show();
  else
  {
    if (!$rtl)
      echo '<link rel="stylesheet" media="screen" type="text/css" title="Default Layout" href="/media/styles/standard.css" />';
    else
      echo '<link rel="stylesheet" media="screen" type="text/css" title="Default Layout" href="/media/styles/standard-rtl.css" />';
  }
?>

  <link rel="alternate stylesheet" media="screen, aural" type="text/css" title="High Contrast Dark" href="/media/styles/darkbackground.css" />
  <link rel="alternate stylesheet" media="screen, aural" type="text/css" title="High Contrast Yellow on Blue" href="/media/styles/yellowonblue.css" />

<?php
  if (!$rtl)
    echo '<link rel="stylesheet" media="print, embossed" type="text/css" href="/media/styles/print.css" />';
  else
    echo '<link rel="stylesheet" media="print, embossed" type="text/css" href="/media/styles/print-rtl.css" />';
?>