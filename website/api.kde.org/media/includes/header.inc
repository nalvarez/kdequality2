<?php
  // include functions we need in the normal case
  // this must be first thing we do, as site.inc may already use this set of functions
  include_once ("functions.inc");

  // setup $site_root; $document_root; $url_root; $current_relativeurl;
  setup_site ();

  // take a look: have we any locale.inc there (to hardcode language for locale pages)?
  if (file_exists ("$document_root/locale.inc"))
    include_once ("$document_root/locale.inc");

  // do some processing based on the servername
  $servername = $_SERVER['SERVER_NAME'];

  // the cookie domain we use
  $cookiedomain = "." . $servername;

  // if a locale is set, this overwrites all other stuff!!!!
  if (!isset($site_locale))
  {
    if (isset($_GET['site_locale']))
    {
      $site_locale = $_GET['site_locale'];
      setcookie('locale', $site_locale, time()+31536000, '/', $cookiedomain);
    }
    else
    {
      if (isset($_REQUEST['locale']))
      {
        $site_locale = $_REQUEST['locale'];
      }
      else
      {
        $site_locale = "en";
      }
    }
  }

  // set php locale
  setlocale(LC_ALL, $site_locale);

  // start up our i18n stuff
  startTranslation($site_locale);

  // IMPORTANT: now that i18n are up and running, include the site specific config include
  // include the site.inc of this page! this is a more sane way than having this xyz.inc's in the include dir!
  if (file_exists ("$document_root/site.inc"))
    include_once ("$document_root/site.inc");

  // figure out if we are external
  if (isset($site) && $site != "www")
    $site_external = true;

  // left corner logo, e.g. KDE Crystal Gear
  if (!isset($site_logo_left))
    $site_logo_left = siteLogo("/media/images/kde_gear_64.png", "64", "64");

  // count of menu sidebars
  if (!isset ($site_menus))
    $site_menus = 1;

  // create menu if not already around!
  if (!isset($kde_menu))
  {
    // now try to find right menu implementation for this page
    if (isset($site) and ($site == "developer"))
      include_once("classes/class_menu_developer.inc");
    else
      include_once("classes/class_menu.inc");

    // construct the menu
    $kde_menu = new BaseMenu ($document_root, $url_root, $current_relativeurl);
  }

  if (isset($_REQUEST['sitestyle']))
    $site_style = $_REQUEST['sitestyle'];
  else if (isset($_REQUEST['site_style']))
    $site_style = $_REQUEST['site_style'];
  else $site_style = "windowcolors";

  if (!isset($site_title))
    $site_title = i18n_var("K Desktop Environment");

  if (isset($page_title))
    $title = "$site_title - $page_title";
  else
    $title = $site_title;

  if (! isset ($templatepath))
    $templatepath = "";

  print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
  print "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n";
?>

<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $site_locale; ?>" xml:lang="<?php print $site_locale; ?>">
<head>
  <title><?php print $title; ?></title>

<style type="text/css">
  .cp-doNotDisplay { display: none; }
  @media aural, braille, handheld, tty { .cp-doNotDisplay { display: inline; speak: normal; }}
  .cp-edit { text-align: right; }
  @media print, embossed { .cp-edit { display: none; }}
</style>

<?php
if (isset ($templatepath))
    include $templatepath."template-header.inc";
else
    include "template-header.inc";

if (file_exists ("$document_root/site-header.inc"))
  include_once ("$document_root/site-header.inc");
?>

</head>

<body <?php
echo ' id="cp-site-'.htmlspecialchars (str_replace(".", "", $servername)).'"';
if(isset($rtl) && $rtl) print " dir=\"rtl\"";
if(isset($body_onload)) print " onload=\"".$body_onload."\"";
if(isset($body_onunload)) print " onunload=\"".$body_onunload."\"";
?>>

<ul class="cp-doNotDisplay">
  <li><a href="#cp-content" accesskey="2"><?php i18n("Skip to content")?></a></li>
  <li><a href="#cp-menu" accesskey="5"><?php i18n("Skip to link menu")?></a></li>
</ul>


<?php
if (isset ($templatepath))
    include $templatepath."template-top1.inc";
else
    include "template-top1.inc";
?>
<a name="cp-content" />
<?php
  if (isset ($_REQUEST ['edit']))
    print "<h1>Edit Page: \"$page_title\"</h1>\n";
  elseif (isset($page_title) && ($page_title != ""))
    print "<h1>$page_title</h1>\n";

if (isset ($templatepath))
    include $templatepath."template-top2.inc";
else
    include "template-top2.inc";

  if (isset ($_REQUEST ['edit'])) {
    include ('edit.inc');
    include ('footer.inc');
    die();
  }
?>
