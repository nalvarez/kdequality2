<?php

/**
 * Written by Matthias Me�er <bmlmessmer@web.de>, and modified by Chris Howells to make it more flexible for koffice.org
 * based on code by Chris Howells <howells@kde.org>,
 * which is based on code by Christoph Cullmann <cullmann@kde.org>,
 * which is based on the cool menu code from usability.kde.org written by Simon Edwards <simon@simonzone.com>
 *
 * usage:
 * <?php
 *   $appinfo = new AppInfo( "KMyProgram" );
 *   $appinfo->setVersion( "0.1" );
 *   $appinfo->setCopyright( "2001", "Klaus Mustermann and his Team" );
 *   $appinfo->setLicense("gpl");
 *   $appinfo->setIcon( "../pics/projects/hi32-app-flashkard.png", "32", "32" );
 *   $appinfo->addAuthor( "Klaus Mustermann", "mustermann AT mail DOT xx" ); // mail addy can be omitted
 *   $appinfo->addContributor( "Susan Somename", "somename AT mail DOT xx", "coding and testing" );
 *   $appinfo->addContributor( "Andrew Anyname", "andrew AT mail DOT xx" ); // job can be omitted
 *   $appinfo->show(); // no output w/o this command
 *   $appinfo->showIconAndCopyright(); // another output function, for printing copyright
 * ?>
 */

class AppInfo
{
	var $title;
	var $authors = array();
	var $maintainers = array();
	var $contributors = array();
	var $thankstos = array();
	var $copy_year;
	var $copy_by;
	var $version;
	var $icon;
	var $license;
	var $width;
	var $height;

	// tmp vars
	var $author;
	var $maintainer;
	var $contributor;

	function AppInfo($title)
	{
		$this->title = $title;
	}

	function setVersion($version)
	{
		$this->version = $version;
	}

	function setCopyright($year, $by)
	{
		$this->copy_year = $year;
		$this->by = $by;
	}

	function addAuthor($name, $email="")
	{
		$author = new Author($name, $email);
		array_push($this->authors, $author);
	}

	function addMaintainer($name, $email="")
	{
		$contributor = new Author($name, $email);
		array_push($this->maintainers, $contributor);
	}

	function addContributor($name, $email="", $job="")
	{
		$contributor = new Contributor($name, $email, $job);
		array_push($this->contributors, $contributor);
	}

	function addThanksTo($name, $email="", $job="")
	{
		$contributor = new Contributor($name, $email, $job);
		array_push($this->thankstos, $contributor);
	}

	function setIcon($icon, $width, $height)
	{
		$this->icon = $icon;
		$this->width = $width;
		$this->height = $height;
	}

	function setLicense($license)
	{
		if ($license == "gpl")
		{
			$this->license = "$this->title is distributed under the terms of the <a href=\"http://www.gnu.org/licenses/gpl.html\">GNU General Public License (GPL), Version 2</a>.\n";
		}
		else
		if ($license == "lgpl")
		{
			$this->license = "$this->title is distributed under the terms of the <a href=\"http://www.gnu.org/licenses/lgpl.html\">GNU Lesser General Public License (LGPL), Version 2</a>.\n";
		}
		else
		{
			$this->license = "Unknown license, valid licenses for setLicense() are 'gpl', 'lgpl'. Update www/media/includes/classes/class_appinfo.inc if another is required (contact Chris Howells <howells@kde.org>.\n";
		}
	}

        function showIconAndCopyright()
	{
		if( $this->icon != "" )
		{
			print "<img src=\"$this->icon\" alt=\"$this->title\" width=\"$this->width\" height=\"$this->height\" style=\"float:left;margin-right:10px;margin-bottom:10px\" />\n";
		}
		print "<b>Version $this->version</b><br />\n";
		$date = getdate();
		print "<b>&copy; $this->copy_year-". $date['year'] . " $this->by</b><br />\n";
                print "<br style=\"clear:both\" />\n";
       }

       function show()
       {
               print "<div>\n";
               $this->showIconAndCopyright();

                if (isset($this->license))
                {
                        print "$this->license<br />&nbsp;<br />\n";
                }
                else
                {
                        print "License not defined, don't forget to use setLicense().<br />&nbsp;<br />\n";
                }

		if (count($this->authors))
		{
			print "<b>Original Author";
			if( count( $this->authors ) > 1 )
			{
				 print "s";
			}
			print ":</b>\n";
			print "<ul>\n";

			for ($i=0; $i < count( $this->authors ); $i++)
			{
				$this->authors[$i]->show();
			}
			print "</ul>\n";
		}

		if (count($this->maintainers))
		{
			print "<b>Maintainer";
			if( count( $this->maintainers ) > 1 )
			{
				 print "s";
			}
			print ":</b>\n";
			print "<ul>\n";

			for ($i=0; $i < count( $this->maintainers ); $i++)
			{
				$this->maintainers[$i]->show();
			}
			print "</ul>\n";
		}

		if (count($this->contributors))
		{
			print "<b>Contributor";
			if( count( $this->contributors ) > 1 )
			{
				print "s";
			}
			print ":</b>\n";
			print "<ul>\n";

			for ($i=0; $i < count( $this->contributors ); $i++)
			{
				$this->contributors[$i]->show();
			}
			print "</ul>\n";
		}

		if (count($this->thankstos))
		{
			print "<b>Thanks To:</b>\n";
			print "<ul>\n";

			for ($i=0; $i < count( $this->thankstos ); $i++)
			{
				$this->thankstos[$i]->show();
			}
			print "</ul>\n";
		}
		print "</div>\n";
	}
}

class Author
{
	var $name;
	var $email;

	function Author($name, $email)
	{
		$this->name = $name;
		$this->email = $email;
	}

	function show()
	{
		print "<li>$this->name";
		if($this->email != "")
		{
			print "&nbsp;&lt;<a href=\"mailto:$this->email\">$this->email</a>&gt;";
		}
		print "</li>\n";
	}
}

class Contributor
{
	var $name;
	var $email;
	var $job;

	function Contributor($name, $email, $job)
	{
		$this->name = $name;
		$this->email = $email;
		$this->job = $job;
	}

	function show()
	{
		print "<li>$this->name";
		if( $this->email != "" )
		{
			print "&nbsp;&lt;<a href=\"mailto:$this->email\">$this->email</a>&gt;";
		}
		if( $this->job != "" )
		{
			print ":&nbsp;$this->job";
		}
		print "</li>\n";
	}
}
