<?php
/*
 * Filename: 	classNews.php
 * Description: automatically fetches a configured RDF.
 * Functions: 	openRDF(file)
 * Author: 	Sebastian Faubel (modified original)
 */

class RDF {
	/* opens and returns the content of a specified RDF-File in "pieces" */

	function openRDF($rdf_file) {

	global $site_locale;
	startTranslation($site_locale);

	$file = @fopen($rdf_file, "r");

	if ($file) {
	$rf = fread($file, 32000); // read 32K
	$grab = eregi("<item>(.*)</item>", $rf, $printing);
	$rdf_pieces = explode("<item>", $printing[0]);
	}
	else echo "<b>" . i18n_var("Error while opening $rdf_file") . "</b>\n";

	return $rdf_pieces;
	}

}
?>
