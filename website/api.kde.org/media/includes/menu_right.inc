<div class="menu_box">
<h2 id="t_kdefamily">KDE Family</h2>
<h3 id="t_newsinformation">&#187; News &amp; Information</h3>
<ul>
  <li><a href="http://dot.kde.org/" title="The KDE News">KDE News</a></li>
  <li><a href="http://events.kde.org/" title="KDE Events">KDE Events</a></li>
  <li><a href="http://kdemyths.urbanlizard.com/" title="Myths About KDE">KDE Myths</a></li>
</ul>

<h3 id="t_projects">&#187; KDE Projects</h3>
<ul>
  <li><a href="http://accessibility.kde.org/" title="Accessibility Homepage">Accessibility</a></li>
  <li><a href="http://i18n.kde.org/" title="Internationalization Homepage">Internationalization</a></li>
  <li><a href="http://quality.kde.org/" title="KDE Quality Team Homepage">Quality</a></li>
  <li><a href="http://usability.kde.org/" title="Usability Homepage">Usability</a></li>
  <li><a href="http://appeal.kde.org/" title="Appeal Desktop Project">Appeal</a></li>
  <li><a href="http://women.kde.org/" title="KDE Women Homage">Women</a></li>
  <li><a href="http://freebsd.kde.org/" title="KDE on FreeBSD Homepage">KDE on FreeBSD</a></li>
  <li><a href="http://solaris.kde.org/" title="KDE on Solaris Homepage">KDE on Solaris</a></li>
</ul>

<h3 id="t_community">&#187; Community</h3>
<ul>
  <li><a href="http://www.kde-apps.org/" title="www.kde-apps.org">KDE Apps</a></li>
  <li><a href="http://www.kde-look.org/" title="www.kde-look.org">KDE Look</a></li>
  <li><a href="http://www.kde-files.org/" title="www.kde-files.org">KDE Files</a></li>
  <li><a href="http://www.kde-artists.org/" title="www.kde-artists.org">KDE Artists</a></li>
  <li><a href="http://wiki.kde.org/" title="wiki.kde.org">Community Wiki</a></li>
  <li><a href="http://www.kde-forum.org/" title="www.kde-forum.org" rel="nofollow">Community Forums</a></li>
  <li><a href="http://www.kdedevelopers.org/" title="www.kdedevelopers.org">Developer Journals</a></li>
  <li><a href="http://www.planetkde.org/" title="www.planetkde.org">Planet KDE</a></li>
  <li><a href="http://www.spreadkde.org/" title="www.spreadkde.org">Spread KDE</a></li>
</ul>

<h3 id="t_applications">&#187; Applications</h3>
<ul>
  <li><a href="http://konqueror.kde.org/" title="Konqueror Homepage">Konqueror</a></li>
  <li><a href="http://www.koffice.org/" title="KOffice Homepage">KOffice</a></li>
  <li><a href="http://www.kontact.org/" title="Kontact Homepage">Kontact</a></li>
  <li><a href="http://www.kdevelop.org/" title="KDevelop Homepage">KDevelop</a></li>
  <li><a href="http://games.kde.org/" title="KDE Games Homepage">Games</a></li>
  <li><a href="http://edu.kde.org/" title="Edutainment Homepage">Edutainment</a></li>
  <li><a href="http://multimedia.kde.org/" title="Multimedia Homepage">Multimedia</a></li>
  <li><a href="http://printing.kde.org/" title="Everything about printing in KDE applications">Printing</a></li>
  <li><a href="http://kdewebdev.org/" title="KDE Web Development Homepage">Web Development</a></li>
  <li><a href="http://extragear.kde.org/" title="KDE Extragear Home">KDE Extragear</a></li>
</ul>
</div>
