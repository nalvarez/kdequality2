<?php

include("classes/class_handler404.inc");

$handler = new Handler404();

$handler->add("/tutorial", "http://techbase.kde.org/Development/Tutorials/API_Documentation");
$handler->add("/tutorial/index.php", "http://techbase.kde.org/Development/Tutorials/API_Documentation");
$handler->add("/tutorial/guidelines.php", "http://techbase.kde.org/Development/Tutorials/API_Documentation");

// do it ;)
$handler->execute();

?>
