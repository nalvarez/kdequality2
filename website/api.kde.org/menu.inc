<?php
$this->setName( "KDE API Reference" );

$section =& $this->appendSection("Navigation");

$section->appendLink("Main Page","/index.php",false);
$section->appendLink("Old KDE4 Versions","/history4.php",false);

$section =& $this->appendSection("Related");

$section->appendLink("API Doc Tutorial","http://techbase.kde.org/Development/Tutorials/API_Documentation",false);
$section->appendLink("KDE TechBase","http://techbase.kde.org/",false);
$section->appendLink("KDE CMake Modules","/cmake/modules.html",false);

