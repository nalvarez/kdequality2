<?php
  $page_title = "API Reference Index";
  include 'header.inc';
?>

<p>
The reference guides for the KDE APIs -- for KDE2 all the way to the
current development version -- are collected here.
We assume you are already familiar with Trolltech's&trade; excellent 
<a href="http://doc.qt.nokia.com/latest/index.html" target="_new">Qt4</a>
documentation and their
<a href="http://qt.nokia.com/developer" target="_new">Developer Pages</a>.
<a href="http://techbase.kde.org" target="_new">TechBase</a>
is the right place to start looking for general development
information for KDE.
There are only reference guides here.
</p>
<p>
To obtain a gzip compressed tar file containing the documentation, click on the <img src="/img/14x14save.png" alt="[download]"> images, which are immediately adjacent to many of the listed items.
</p>
<p>
To obtain a version of the documentation for use in
<a href="http://doc.qt.nokia.com/latest/assistant-manual.html">Nokia Qt Assistant</a>, click on the "[qch]" links, which are immediately adjacent to some of the listed items. (In Qt assistant, go into Edit->Preferences->Documentation and [Add] the .qch file.)
</p>
<p>
Man pages are also provided for some modules. Click on the "[man]" links, also immediately adjacent to some of the listed items to download a bzip2 compressed tar file containing the man pages for the corresponding module. (Uncompress and untar thes files into a standard MANPATH directory.)
</p>

<table>
<tr>
<th style="text-align:center" width="36%">
KDE 4.x (unstable)
</th>
<th style="text-align:center" width="36%">
KDE 4.8 (stable)
</th>
<th style="text-align:center" width="28%">
<a href="kdesupport-api/kdesupport.tar.gz">
<img src="/img/14x14save.png" alt="[download]" title="Download kdesupport APIDOX tarball."></a>
KDE Support
</th>
</tr>
<tr>
<?php
  function apidox($v,$module) {
    print '<li><a href="' . $v . '-api/' . $module . '.tar.gz">';
    print '<img src="/img/14x14save.png" alt="[download]" title="Download ' . $module . ' APIDOX tarball."></a>';
    print '&nbsp;<a href="' . $v . '-api/' . $module . '-apidocs/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a></li>\n\n";
  }

  function apidoxqch($v,$module) {
    print '<li><a href="' . $v . '-api/' . $module . '.tar.gz">';
    print '<img src="/img/14x14save.png" alt="[download]" title="Download ' . $module . ' APIDOX tarball."></a>';
    print '&nbsp;<a href="' . $v . '-api/' . $module . '-apidocs/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a>";
    print '&nbsp;[<a href="qch/' . $module . '-' . $v . '.qch">qch</a>]</li>' . "\n\n";
  }

  function apidoxmanonly($name) {
    print '<li>';
    print $name;
    print '&nbsp;[<a href="man/' . $name . '-man.tar.bz2">man</a>]</li>' . "\n\n";
  }

  function frameworksqchman($v,$module) {
    print '<li><a href="' . $v . '-api/' . $module . '.tar.gz">';
    print '<img src="/img/14x14save.png" alt="[download]" title="Download ' . $module . ' frameworks' . ' APIDOX tarball."></a>';
    print '&nbsp;<a href="' . $v . '-api/' . $module . '-apidocs/" title="View online APIDOX for ' . $module . ' frameworks' . '">';
    print $module . ' <b>frameworks</b>' . "</a>";
    print '&nbsp;[<a href="qch/' . $module . '-' . $v . '.qch">qch</a>][<a href="man/' . $module . '-' . $v . '-man.tar.bz2">man</a>]</li>' . "\n\n";
  }

  function apidoxqchman($v,$module) {
    print '<li><a href="' . $v . '-api/' . $module . '.tar.gz">';
    print '<img src="/img/14x14save.png" alt="[download]" title="Download ' . $module . ' APIDOX tarball."></a>';
    print '&nbsp;<a href="' . $v . '-api/' . $module . '-apidocs/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a>";
    print '&nbsp;[<a href="qch/' . $module . '-' . $v . '.qch">qch</a>][<a href="man/' . $module . '-' . $v . '-man.tar.bz2">man</a>]</li>' . "\n\n";
  }

  function apidox2($v,$module) {
    print '<li><a href="' . $v . '-api/' . $v . '-apidocs/' . $module . '/html/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a></li>\n\n";
  }

  function apidox2qch($v,$module) {
    print '<li><a href="' . $v . '-api/' . $v . '-apidocs/' . $module . '/html/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a>";
    print '&nbsp;[<a href="qch/' . $module . '-' . $v . '.qch">qch</a>]</li>' . "\n\n";
  }

  function apidox3($v,$module) {
    print '<li><a href="' . $v . '-api/' . $module . '-apidocs/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a></li>\n\n";
  }

  function apidox4($v,$module) {
    print '<li><a href="' . $v . '-api/" title="View online APIDOX for ' . $module . '">';
    print $module . "</a></li>\n\n";
  }


?>
<td valign="top">
<ul li style="list-style-type: none;">
<?php
  frameworksqchman('4.x','kdelibs');
  apidoxqchman('4.x','kdepimlibs');
  apidox('4.x','kdeaccessibility');
  apidox('4.x','kde-baseapps');
  apidox('4.x','kde-runtime');
  apidox('4.x','kde-workspace');
  apidox('4.x','kdeedu');
  apidox('4.x','kdegames');
  apidox('4.x','kdegraphics');
  apidox('4.x','kdemultimedia');
  apidox('4.x','kdenetwork');
  apidox('4.x','kdepim');
  apidox('4.x','kdepim-runtime');
  apidox('4.x','kdeplasma-addons');
  apidox('4.x','kdesdk');
  apidox('4.x','kdeutils');
  apidox('4.x','kdewebdev');
  apidox('4.x','plasma-qml');
  apidox4('pykde-4.7','PyKDE');
?>
</ul>
</td>
<td valign="top">
<ul li style="list-style-type: none;">
<?php
  apidoxqchman('4.8','kdelibs');
  apidoxqchman('4.8','kdepimlibs');
  apidox('4.8','kdeaccessibility');
  apidox('4.8','kde-baseapps');
  apidox('4.8','kde-runtime');
  apidox('4.8','kde-workspace');
  apidox('4.8','kdeedu');
  apidox('4.8','kdegames');
  apidox('4.8','kdegraphics');
  apidox('4.8','kdemultimedia');
  apidox('4.8','kdenetwork');
  apidox('4.8','kdepim');
  apidox('4.8','kdeplasma-addons');
  apidox('4.8','kdesdk');
  apidox('4.8','kdeutils');
  apidox('4.8','kdewebdev');
  apidox4('pykde-4.7','PyKDE');
?>
</ul>
</td>
<td valign="top">
<ul>
<?php
  apidox('kdesupport','akonadi');
  apidox('kdesupport','attica');
  apidox('kdesupport','kdewin');
  apidox('kdesupport','libqzeitgeist');
  apidoxqch('kdesupport','phonon');
  apidox('kdesupport','polkit-qt-1');
  apidox('kdesupport','prison');
  apidox('kdesupport','qca');
  apidox('kdesupport','qimageblitz');
  apidox('kdesupport','soprano');
  apidoxmanonly('kde-qt');
?>
</ul>
</td>
</tr>

<tr>
<th style="text-align:center" width="36%">
Bundled Apps
</th>
<th style="text-align:center" width="36%">
Extragear
</th>
<th style="text-align:center" width="28%">
Playground
</th>
</tr>
<tr>
<td valign="top">
<ul li style="list-style-type: none;">
<?php
  apidox('bundled-apps', 'calligra');
  apidox('bundled-apps', 'koffice');
?>
</ul>
</td>
<td valign="top">
<ul li style="list-style-type: none;">
<?php
  apidox('extragear','accessibility');
  apidox('extragear','base');
  apidox('extragear','edu');
  apidox('extragear','games');
  apidox('extragear','graphics');
  apidox('extragear','kdevelop');
  apidox('extragear','libs');
  apidox('extragear','multimedia');
  apidox('extragear','network');
  apidox('extragear','office');
  apidox('extragear','pim');
  apidox('extragear','sdk');
  apidox('extragear','sysadmin');
  apidox('extragear','utils');
?>
</ul>
</td>
<td valign="top">
<ul li style="list-style-type: none;">
<?php
  apidox('playground','base');
  apidox('playground','edu');
  apidox('playground','games');
  apidox('playground','graphics');
  apidox('playground','libs');
  apidox('playground','pim');
  apidox('playground','sysadmin');
  apidox('playground','utils');
?>
</ul>
</td>
</tr>

<tr>
<th style="text-align:center" width="36%">
KDE 3.5
</th>
<th style="text-align:center" width="36%">
Historical
</th>
</tr>
<tr>
<td valign="top">
<ul li style="list-style-type: none;">
<?php
  apidox('3.5','kdelibs');
  apidox('3.5','kdebase');
  apidox('3.5','kdeedu');
  apidox('3.5','kdegames');
  apidox('3.5','kdegraphics');
  apidox('3.5','kdemultimedia');
  apidox('3.5','kdenetwork');
  apidox('3.5','kdepim');
?>
</ul>
</td>
<td valign="top">
<ul>
<li><a href="/history4.php">Old KDE4 Versions</a></li>
<li><a href="3.1-api/classref">KDE 3.1 kdelibs</a></li>
<li><a href="3.0-api/classref">KDE 3.0 kdelibs</a></li>
<li><a href="2.2-api/classref">KDE 2.2 kdelibs</a></li>
<li><a href="2.1-api/classref">KDE 2.1 kdelibs</a></li>
<li><a href="2.0-api/classref">KDE 2.0 kdelibs</a></li>
</ul>
</td>
</tr>

</table>

<?php include 'footer.inc' ?>

