<?php
  $site_title = "KDE API Reference";
  $site_search = "custom";
  $site_external = true;
  $name = "Adriaan de Groot and Allen Winter";
  $mail = "apidox@kde.org";
  $templatepath = "newlayout/";
  $showedit = false;
