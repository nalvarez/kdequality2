#content p {
margin-top: 1ex;
}

.right {
float: right;
margin: 1ex 1em;
clear: both;
}

#content ul li { margin-top: 1ex; }

.abstract {
margin-left: 2em;
margin-right: 2em;
margin-bottom: 20px;
min-height: 40px;
border: 2px solid #418ADE;
background: #DFE7F3;
}

.abstract p {
padding-left: 20px;
padding-right: 20px;
font-size: x-small;
font-shape: italic;
padding-bottom: 0px;
}

.abstract p em {
font-weight: bold;
}

div.figure, div.note, div.dox , div.warn, div.build, div.broken, div.shell {
margin: 1ex 35px 2ex 35px;
padding: 6px 0.5em 6px 55px;
border: 2px solid #418ADE;
background-color: #DFE7F3;
background-position: 8px 8px;
background-repeat: no-repeat;
font-size: x-small;
min-height: 40px;
}

div.note { background-image: url(<?php echo $_GET['css-inc'] ?>/img/info.png); }
div.dox { background-image: url(<?php echo $_GET['css-inc'] ?>/img/example.png); white-space : pre; }
div.warn { background-image: url(<?php echo $_GET['css-inc'] ?>/img/warning.png); }
div.build { background-image: url(<?php echo $_GET['css-inc'] ?>/img/build.png); margin-top: 2ex; white-space : pre; }
div.broken { background-image: url(<?php echo $_GET['css-inc'] ?>/img/broken.png); }
div.shell { background-image: url(<?php echo $_GET['css-inc'] ?>/img/shellscript.png); white-space: pre; }

li div.figure, li div.note, li div.dox ,li  div.warn, li div.build {
margin-left: 0px;
}

div.dox div.credit {
float: right;
font-size: xx-small;
font-style: italic;
}

div.figure { margin-right: 0px; padding-left: 0.5em; }

div.clear { clear: both; height: 1px; }

.note img {
float: left;
margin: 0px 0px 0px -60px;
}

