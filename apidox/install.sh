#!/bin/sh
#
# Install APIDOX tools. The API documentation tools are packaged in SVN
# as src/ and as data/ (to distinguish what's what) but are installed
# into a single directory.
#
# Usage:
#    install [--prefix=/dir]
 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA


# change TOP to whatever you like for your top-level installation directory
if ( test `hostname | egrep -c www` -gt 0 ) then
  EBN=1
  TOP=/mnt/ebn
else
  EBN=0
  TOP=/usr/local/EBN
fi

# check command-line argument
case "$1" in
--prefix=*)
  TOP=`echo "$1" | sed -e 's+--prefix=++'`
  ;;
*)
  ;;
esac

mkdir -p $TOP/bin $TOP/lib $TOP/share/apidox
for i in bin lib share/apidox
do
	test -d $TOP/$i || { echo "Expected $TOP/$i to be a directory." ; exit 1 ; }
	test -w $TOP/$i || { echo "Cannot write to directory $TOP/$i." ; exit 1 ; }
done


BINDIR=$TOP/bin
DATADIR=$TOP/share/apidox

# install the programs
cd src
perl Makefile.PL PREFIX=$TOP && \
make && \
make install && \
make realclean
#don't need the dust bunnies laying around
for i in nudox.pl nudox-helpqt.pl doxylog2html.pl nudox-help1.pl update-dox.pl
do
	rm -f $TOP/lib/perl5/site_perl/*/$i
done

for i in nudox-helpqt.pl \
	nudox.sh \
	doxygen4.sh \
	gendox.sh \
	nudox-help1.pl
do
	cp $i $BINDIR
	chmod 755 $BINDIR/$i
done

cd ..

# install the templates
cd data
cp capacity-footer.html capacity-header.html \
   capacity-toplevel-footer.html capacity-toplevel-header.html \
   $DATADIR

cp kde3-footer.html kde3-header.html \
   kde3-toplevel-footer.html kde3-toplevel-header.html \
   $DATADIR
cp nudox-online*.html \
   $DATADIR

cp qt*.tag $DATADIR

cd ..

chmod 755 $BINDIR
chmod 755 $BINDIR/nudox.pl
chmod 755 $BINDIR/doxylog2html.pl

#EBN-specific post-installation stuff
if ( test $EBN -eq 1 ) then
  chmod ug+w $BINDIR
  chmod ug+w $BINDIR/nudox.pl
  chmod ug+w $BINDIR/doxylog2html.pl
fi
