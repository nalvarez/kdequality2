#!/usr/bin/perl -w

###############################################################################
# Converts doxygen log files into pretty HTML for the EBN.                    #
# Copyright (C) 2006-2007,2009-2012 by Allen Winter <winter@kde.org>          #
#                                                                             #
# This program is free software; you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation; either version 2 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc.,     #
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.               #
#                                                                             #
###############################################################################
#
# A program to beautify doxygen log files (which contain the doxygen errors
# found in source files) into HTML for the EnglishBreakfastNetwork (EBN).
#
# Program options:
#   --help:          display help message and exit
#   --version:       display version information and exit
#   --explain:       if issues found, print an explanation along with
#                    solving instructions at the end of each test
#   --export [text|ebn|html]
#                    output in one of the following formats:
#                      text (default)
#                      ebn -> English Breakfast Network style
#                      html -> plain old html
#   --title:         give the output a project title.
#   --cms:           component/module/subdir triple for html and ebn exports
#   --rev:           subversion revision number
#

use strict;
use Getopt::Long;
use File::Basename;
use Tie::IxHash;
use Text::Wrap;
use HTML::Entities;
use POSIX qw (strftime);

my($Prog) = 'doxylog2html.pl';
my($Version) = '1.94';

my($help) = '';
my($explain) = '';
my($version) = '';
my($export) = 'text';
my($title) = "APIDOX Results";
my($cms) = "kde-4.6/kdepim";
my($rev) = '';

exit 1
if (!GetOptions('help' => \$help, 'version' => \$version,
		'explain' => \$explain,'export=s' => \$export,
		'title=s' => \$title, 'cms=s' => \$cms, 'rev=s' => \$rev));

&Help() if ($help);
if ($#ARGV < 0){ &Help(); exit 0; }
&Version() if ($version);

# hash of all issues we need to keep track of
tie my(%Issues), "Tie::IxHash";

my($f,$num_issues,$tot_issues);
$tot_issues=0;
for $f (@ARGV) {
  &initIssues();
  $num_issues = &processFile($f);
  $tot_issues += $num_issues;

  &printHeader($f,$num_issues);
  if (&printResults() != $num_issues) {
    print "ERROR: Number of incorrect number of issues printed.  Bad programmer!\n";
    exit 1;
  }
  &printFooter();
}
exit $tot_issues;

#==============================================================================
# Help function: print help message and exit.
sub Help {
  &Version();
  print "Converts doxygen log files into pretty HTML for the EBN.\n\n";
  print "Usage: $Prog [OPTION]... FILE...\n";
  print "  --help             display help message and exit\n";
  print "  --version          display version information and exit\n";
  print "  --explain          print explanations with solving instructions\n";
  print "  --export [text|ebn|html]\n";
  print "                     output in one of the following formats:\n";
  print "                       text (default)\n";
  print "                       ebn -> English Breakfast Network style\n";
  print "                       html -> plain old html\n";
  print "  --title            give the output a project title\n";
  print "  --cms              component/module/subdir triple for html and ebn exports\n";
  print "  --rev:             subversion revision number\n";
  print "\n";
  exit 0 if $help;
}

# Version function: print the version number and exit.
sub Version {
  print "$Prog, version $Version\n";
  exit 0 if $version;
}

sub initIssues() {

  ###
  $Issues{'UNDOC'}{'issue'} = 'Check for undocumented methods';
  $Issues{'UNDOC'}{'explain'} = 'You must document all methods according to policy.';
  $Issues{'UNDOC'}{'count'} = 0;
  $Issues{'UNDOC'}{'files'} = '';

  ###
  $Issues{'MISSING'}{'issue'} = 'Check for undocumented parameters';
  $Issues{'MISSING'}{'explain'} = 'Please make sure all methods are properly documented.';
  $Issues{'MISSING'}{'count'} = 0;
  $Issues{'MISSING'}{'files'} = '';

  ###
  $Issues{'BADPARM'}{'issue'} = 'Check for unknown parameters';
  $Issues{'BADPARM'}{'explain'} = 'Do not document parameters that are not in the methods\'s argument list.';
  $Issues{'BADPARM'}{'count'} = 0;
  $Issues{'BADPARM'}{'files'} = '';

  ###
  $Issues{'BADDOC'}{'issue'} = 'Check for methods that are undeclared or undefined';
  $Issues{'BADDOC'}{'explain'} = 'Please make sure that you declare or define all methods and functions. Then document them.';
  $Issues{'BADDOC'}{'count'} = 0;
  $Issues{'BADDOC'}{'files'} = '';

  ###
  $Issues{'BADCOMMAND'}{'issue'} = 'Check for bad doxygen commands';
  $Issues{'BADCOMMAND'}{'explain'} = 'Bad doxygen commands will prevent proper processing of the in-line documentation.';
  $Issues{'BADCOMMAND'}{'count'} = 0;
  $Issues{'BADCOMMAND'}{'files'} = '';

  ###
  $Issues{'BADREF'}{'issue'} = 'Check for bad references';
  $Issues{'BADREF'}{'explain'} = 'Do not reference entities that do not exist in the documentation.';
  $Issues{'BADREF'}{'count'} = 0;
  $Issues{'BADREF'}{'files'} = '';

  ###
  $Issues{'MATCHCLASS'}{'issue'} = 'Check for mismatched class members';
  $Issues{'MATCHCLASS'}{'explain'} = 'Make sure that class method signatures match.';
  $Issues{'MATCHCLASS'}{'count'} = 0;
  $Issues{'MATCHCLASS'}{'files'} = '';

  ###
  $Issues{'MATCHFILE'}{'issue'} = 'Check for mismatched file members';
  $Issues{'MATCHFILE'}{'explain'} = 'Make sure that file function signatures match.';
  $Issues{'MATCHFILE'}{'count'} = 0;
  $Issues{'MATCHFILE'}{'files'} = '';

  ###
  $Issues{'NONAMEMEM'}{'issue'} = 'Check for members without names';
  $Issues{'NONAMEMEM'}{'explain'} = 'Make sure that class members have names.';
  $Issues{'NONAMEMEM'}{'count'} = 0;
  $Issues{'NONAMEMEM'}{'files'} = '';

  ###
  $Issues{'BADTAG'}{'issue'} = 'Check for bad tags';
  $Issues{'BADTAG'}{'explain'} = 'Do not use undefined XML or HTML tags in the documentation.';
  $Issues{'BADTAG'}{'count'} = 0;
  $Issues{'BADTAG'}{'files'} = '';

  ###
  $Issues{'BADLINK'}{'issue'} = 'Check for bad links';
  $Issues{'BADLINK'}{'explain'} = 'Make sure that explicit link requests can be resolved.';
  $Issues{'BADLINK'}{'count'} = 0;
  $Issues{'BADLINK'}{'files'} = '';

  ###
  $Issues{'BADBLOCK'}{'issue'} = 'Check for bad blocks';
  $Issues{'BADBLOCK'}{'explain'} = 'Do not end the comment block before closing XML or HTML tags (like <em>).';
  $Issues{'BADBLOCK'}{'count'} = 0;
  $Issues{'BADBLOCK'}{'files'} = '';

  ###
  $Issues{'BADLIST'}{'issue'} = 'Check for bad lists';
  $Issues{'BADLIST'}{'explain'} = 'Make sure your lists contain some items.';
  $Issues{'BADLIST'}{'count'} = 0;
  $Issues{'BADLIST'}{'files'} = '';

  ###
  $Issues{'BADTOKEN'}{'issue'} = 'Check for bad tokens';
  $Issues{'BADTOKEN'}{'explain'} = 'An unexpected token was encountered.';
  $Issues{'BADTOKEN'}{'count'} = 0;
  $Issues{'BADTOKEN'}{'files'} = '';

  ###
  $Issues{'BADFUNC'}{'issue'} = 'Check for undocumented functions';
  $Issues{'BADFUNC'}{'explain'} = 'An undocumented function or template was encountered.';
  $Issues{'BADFUNC'}{'count'} = 0;
  $Issues{'BADFUNC'}{'files'} = '';

  ###
  $Issues{'BADDEFINE'}{'issue'} = 'Check for undocumented defines';
  $Issues{'BADDEFINE'}{'explain'} = 'An undocumented define was encountered.';
  $Issues{'BADDEFINE'}{'count'} = 0;
  $Issues{'BADDEFINE'}{'files'} = '';

  ###
  $Issues{'INTERNAL'}{'issue'} = 'Check for internal inconsistencies';
  $Issues{'INTERNAL'}{'explain'} = 'An internal inconsistency was encountered.  Usually this is because the scope of a class can not be found.';
  $Issues{'INTERNAL'}{'count'} = 0;
  $Issues{'INTERNAL'}{'files'} = '';

  ###
  $Issues{'NOENDIF'}{'issue'} = 'Check for missing @endif';
  $Issues{'NOENDIF'}{'explain'} = 'Found endif without matching start command.';
  $Issues{'NOENDIF'}{'count'} = 0;
  $Issues{'NOENDIF'}{'files'} = '';

  ###
  $Issues{'BADARG'}{'issue'} = 'Check for bad argument in a @class, @struct, or @union statement';
  $Issues{'BADARG'}{'explain'} = 'You are using an argument for a @class, @struct, or @union that is not an input file.';
  $Issues{'BADARG'}{'count'} = 0;
  $Issues{'BADARG'}{'files'} = '';

  ###
  $Issues{'BADCOPY'}{'issue'} = 'Check for bad @copydoc';
  $Issues{'BADCOPY'}{'explain'} = 'You are using a copydoc command for a function or method that cannot be found.';
  $Issues{'BADCOPY'}{'count'} = 0;
  $Issues{'BADCOPY'}{'files'} = '';

  ###
  $Issues{'NAMESPACE'}{'issue'} = 'Missing argument after @namespace';
  $Issues{'NAMESPACE'}{'explain'} = 'Give the namespace tag an argument.';
  $Issues{'NAMESPACE'}{'count'} = 0;
  $Issues{'NAMESPACE'}{'files'} = '';

  ###
  $Issues{'CONFUSE'}{'issue'} = 'Found \';\' while parsing initializer list';
  $Issues{'CONFUSE'}{'explain'} = 'doxygen could be confused by a macro call without semicolon.';
  $Issues{'CONFUSE'}{'count'} = 0;
  $Issues{'CONFUSE'}{'files'} = '';

  ###
  $Issues{'GARBAGE'}{'issue'} = 'Check for errors of dubious value';
  $Issues{'GARBAGE'}{'explain'} = 'Doxygen messages were encountered that are less than helpful, but may need to be fixed anyway.  Please investigate.';
  $Issues{'GARBAGE'}{'count'} = 0;
  $Issues{'GARBAGE'}{'files'} = '';

  ###
  $Issues{'UNKNOWN'}{'issue'} = 'Check for Unknown errors';
  $Issues{'UNKNOWN'}{'explain'} = 'An unknown error was encountered.  Please contact <mailto:winter@kde.org> to report this.';
  $Issues{'UNKNOWN'}{'count'} = 0;
  $Issues{'UNKNOWN'}{'files'} = '';

}

sub processFile() {
  my($in) = @_;
  my($line,$result);
  my($ln)=0;
  my($fname,$fline);
  print STDERR "Processing $in:\n";
  open(IN, "$in") || die "Couldn't open $in";
  while ($line = <IN>) {
    $ln++;
    chomp($line);
    if ($line !~ m+[Ww]arning:+) {
      next if ($line =~ m/^\*/);
      next if ($line =~ m/^$/);
      next if ($line =~ m/^\s+/);
      next if ($line =~ m/^mkdir/);
      next if ($line =~ m/^grep/);
      next if ($line =~ m/tag:/);
      next if ($line =~ m/ignoring unknown tag/);
      next if ($line =~ m/Skipping/);
      next if ($line =~ m/^Search/);
      next if ($line =~ m/^Sorting/);
      next if ($line =~ m/^Freeing/);
      next if ($line =~ m/^Appending/);
      next if ($line =~ m/^Determining/);
      next if ($line =~ m/termining/);
      next if ($line =~ m/^Adding/);
      next if ($line =~ m/^Inheriting/);
      next if ($line =~ m/^Generati/);
      next if ($line =~ m/^enerating/);
      next if ($line =~ m/^Counting/);
      next if ($line =~ m/^Finding/);
      next if ($line =~ m/^Resolving/);
      next if ($line =~ m/^Combining/);
      next if ($line =~ m/^Reading/);
      next if ($line =~ m/^Preprocessi/);
      next if ($line =~ m/^Parsing/);
      next if ($line =~ m/^Please/);
      next if ($line =~ m/^Possible/);
      next if ($line =~ m/^QGDict/);
      next if ($line =~ m/^GError/);
      next if ($line =~ m/fontconfig/);
      next if ($line =~ m/^Bui/);
      next if ($line =~ m/^Searching/);
      next if ($line =~ m/^Associating/);
      next if ($line =~ m/^Computing/);
      next if ($line =~ m/^Flushing/);
      next if ($line =~ m/^Creating/);
      next if ($line =~ m/^g\s/);
    }
    $result = &analyze($line);
    if ($result =~ m/^ERROR:/) {
      print "$ln: $result\n";
      exit 1;
    } else {
      if ($result && $line) {
	$line =~ s+::++;
	($fname,$fline) = split(":",$line);
	if ($fline) {
	  ($fline) = split(" ",$fline);
	  $fname =~ s+.*/++;
	  $fname =~ s+/mnt/src/kde/++; # basename($fname);
	  $fname =~ s+$cms+.+;
	  if (exists($Issues{$result}{$fname})) {
	    $Issues{$result}{$fname} .= ",$fline";
	  } else {
	    $Issues{$result}{'files'} .= "$fname,";
	    $Issues{$result}{$fname} = "$fline";
	  }
	  $Issues{$result}{'count'}++;
	}
      }
    }
  }
  close(IN);

  my($guy);
  my($total)=0;
  foreach $guy (keys %Issues) {
    if ($guy ne "GARBAGE") {
      $total += $Issues{$guy}{'count'};
    }
  }
  return $total;
}

sub printResults() {
  my($guy);
  my($check_num)=0;
  my($tot)=0;
  my($cline,$rline);
  my($f,@files);
  foreach $guy (keys %Issues) {
    $check_num++;
    $cline = '';
    $cline .= "$check_num. " if ($export eq "text");
    $cline .= "$Issues{$guy}{'issue'}... ";

    if ($Issues{$guy}{'count'}) {
      $rline = '';
      $rline .= "OOPS! $Issues{$guy}{'count'} issues found!";
      $rline .= " (Not counted in total)" if ($guy eq "GARBAGE");
      &printCheck($cline,$rline);
      if ($guy ne "GARBAGE") {
	$Issues{$guy}{'files'} =~ s/,$//;
	(@files) = split(",",$Issues{$guy}{'files'});
	print "\n<ul>\n" if ($export ne "text");
	foreach $f (sort @files) {
	  print "<li>" if ($export ne "text");
	  print "\t$f: ";
	  $tot += &printLines($Issues{$guy}{$f});
	  print "\n";
	  print "</li>\n" if ($export ne "text");
	}
      }
      print "</ul>\n" if ($export ne "text");
      if ($explain) {
	&printExplain(wrap("        ", "        ", $Issues{$guy}{'explain'}));
      }
    } else {
      &printCheck($cline,"okay!");
    }
    print "\n";
  }
  return $tot;
}

# printCheck function: print the "check" and "result" strings, according
# to export type.
sub printCheck(){
  my($check,$result) = @_;
  if ($export eq "text") {
    print "$check $result\n";
  } else {
    # export is "ebn" or "html"
    print "<li>";
    print "<span class=\"toolmsg\">" if ($export eq "ebn");
    print "$check\n<b>$result</b>";
    print "</span>" if ($export eq "ebn");
  }
}

sub printLines() {
  my($lines) =  @_;
  my(@list) = split(",",$lines);
  my(%seen) = ();
  my(@uniq) = ();
  my($item);
  my($num) = 0;
  foreach $item (sort @list) {
    $num++;
    unless ($seen{$item}) {
      $seen{$item} = 0;
      push(@uniq,$item);
    }
    $seen{$item}++;
  }
  my($i)=0;
  @uniq = sort {$a <=> $b} @uniq if ($#uniq >= 1);
  foreach $item (@uniq) {
    $i++;
    print "$item";
    if($seen{$item} > 1) {
      print "[$seen{$item}]";
    }
    if ($i <= $#uniq){
      print ", ";
    }
  }
  print " ($num)";
  return $num;
}

sub analyze() {
  my($line) = @_;

#  return "ERROR: Strangeness detected: $line"
#    unless ($line =~ m/Warning:/ || $line =~ m/\s+parameter/);

  if ($line =~ m+[Ww]arning:+) {
    if ($line =~ m+^<.*>:+) {
      return "GARBAGE";
    }
    if ($line =~ m+not documented+) {
      return "UNDOC";
    }
    if ($line =~ m+are not \(all\) documented+) {
      return "MISSING";
    }
    if ($line =~ m+ missing argument after \\namespace+) {
      return "NAMESPACE";
    }
    if ($line =~ m+Found ';' while parsing initializer list+) {
      return "CONFUSE";
    }
    if ($line =~ m+not found in the argument list+) {
      return "BADPARM";
    }
    if ($line =~ m+was not declared or defined+) {
      return "BADDOC";
    }
    if ($line =~ m+no uniquely matching class member+) {
      return "BADDOC";
    }
    if ($line =~ m+unknown command+) {
      return "BADCOMMAND";
    }
    if ($line =~ m+unable to resolve reference+) {
      return "BADREF";
    }
    if ($line =~ m+no matching class member+) {
      return "MATCHCLASS";
    }
    if ($line =~ m+no matching file member+) {
      return "MATCHFILE";
    }
    if ($line =~ m+member with no name found+) {
      return "NONAMEMEM";
    }
    if ($line =~ m+Unsupported xml/html tag+) {
      return "BADTAG";
    }
    if ($line =~ m+explicit link request+) {
      return "BADLINK";
    }
    if ($line =~ m+end of comment block while expecting command+) {
      return "BADBLOCK";
    }
    if ($line =~ m+End of list marker found without any preceding list items+) {
      return "BADLIST";
    }
    if ($line =~ m+token instead!+) {
      return "BADTOKEN";
    }
    if ($line =~ m+documented function+ && $line =~ m+was not defined+) {
      return "BADFUNC";
    }
    if ($line =~ m+documentation for unknown define+) {
      return "BADDEFINE";
    }
    if ($line =~ m+Internal inconsistency+) {
      return "INTERNAL";
    }
    if ($line =~ m+endif without matching start+) {
      return "NOENDIF";
    }
    if ($line =~ m+supplied as the+ && $line =~ m+argument in the+) {
      return "BADARG";
    }
    if ($line =~ m+of \\copydoc command not found+) {
      return "BADCOPY";
    }
    if ($line =~ m+has become obsolete+) {
      return "";
    }
    if ($line =~ m+not a readable file or directory+) {
      return "";
    }
    if ($line =~ m+string ran past end of line+) {
      return "";
    }
    if ($line =~ m+found more than one \\mainpage comment block+) {
      return "";
    }
    return "UNKNOWN";
  }
  return "";
}

# asOf function: return nicely formatted string containing the current time
sub asOf{
  return strftime("%B %d %Y %H:%M:%S", localtime(time()));
}

# printHeader function: print the header string, according to export type.
sub printHeader{
  my($f,$num_issues)=@_;
  my($component,$module,$subdir)=split("/",$cms);
  $component =~ s/apidox-//;
  my ($upcomp) = uc($component);
  $upcomp =~ s/-/ /;

  if ($export eq "ebn") {
    print "<html>\n";
    print "<head>\n";
    print "<title>$title</title>\n";
    print "<link rel=\"stylesheet\" type=\"text/css\" title=\"Normal\" href=\"/style.css\" />\n";
    print "</head>\n";
    print "<body>\n";
    print "<div id=\"title\">\n";
    print "<div class=\"logo\">&nbsp;</div>\n";
    print "<div class=\"header\">\n";
    print "<h1><a href=\"/\">English Breakfast Network</a></h1>\n";
    print "<p><a href=\"/\">Almost, but not quite, entirely unlike tea.</a></p>\n";
    print "</div>\n";
    print "</div>\n";
    print "<div id=\"content\">\n";
    print "<div class=\"inside\">\n";

    # Breadcrumbs
    print "<p style=\"font-size: x-small;font-style: sans-serif;\">\n";
    print "<a href=\"/index.php\">Home</a>&nbsp;&gt;&nbsp;\n";
    print "<a href=\"/apidocs/index.php\">API Documentation Results</a>&nbsp;&gt;&nbsp;\n";
    print "<a href=\"/apidocs/index.php?component=$component\">$upcomp</a>&nbsp;&gt;&nbsp;\n" if ($component);
    print "<a href=\"/apidocs/index.php?component=$component&module=$module\">$module</a>&nbsp;&gt;&nbsp;\n" if ($component && $module);
    print "$subdir\n" if ($subdir);
    print "</p>\n";

    # Links to other available reports
    if ($component && $module && $subdir) {
      print "<p style=\"font-size: x-small;font-style: sans-serif;\">\n";
      print "Other $module/$subdir reports:\n";
      print "[<a href=\"/krazy/reports/$component/$module/$subdir/index.html\">Krazy</a>]\n";
      print "[<a href=\"/sanitizer/reports/$component/$module/$subdir/index.html\">Docs</a>]\n";
      print "</p>\n";
    }

    print "<h1>$title</h1>\n";
    if ($num_issues) {
      print "Total Issues = $num_issues";
    } else {
      print "No Issues Found!";
    }
    print " ...as of "; print &asOf();
    print " (SVN revision $rev)" if ($rev);
    print "</p>\n";
    my($bf) = basename($f);
    print "<p>For more details, please read the <a href=\"$bf\">doxygen logfile</a> used to generate this report.</p>\n";
    print "<ol>\n";
  } else {
    if ($export eq "html") {
      print "<html>\n";
      print "<head>\n";
      print "<title>$title</title>\n";
      print "</head>\n";
      print "<body>\n";
      print "<h1>$title</h1>\n";
      if ($num_issues) {
        print "Total Issues = $num_issues";
      } else {
        print "No Issues Found!";
      }
      print " ...as of "; print &asOf();
      print " (SVN revision $rev)" if ($rev);
      print "</p>\n";
      print "<ol>\n";
    } else {
      # text export
      print "\n$title\n" if ($title);
      if ($num_issues) {
        print "Total Issues = $num_issues";
      } else {
        print "No Issues Found!";
      }
      print " ...as of "; print &asOf();
      print " (SVN revision $rev)" if ($rev);
      print "\n\n";
    }
  }
}

# printFooter function: print the footer string, according to export type.
sub printFooter{

  if ($export eq "ebn") {
    print "</ol>\n";
    print "</div>\n";
    print "</div>\n";
    print "<div id=\"footer\">\n";
    print "<p>Site content Copyright 2005-2011 by Adriaan de Groot,<br/>\n";
    print "except images as indicated.</p>\n";
    print "</div>\n";
    print "</body>\n";
    print "</html>\n";
  } else {
    if ($export eq "html") {
      print "</ol>\n";
      print "</body>\n";
      print "</html>\n";
    } else {
      # text export
      print "";
    }
  }
}

# printExplain function: print the explanation lines, according to export type.
sub printExplain{
  if ($export eq "ebn") {
    print "<p class=\"explanation\">";
    print &htmlify($_[0]);
    print "</p>\n</li>\n";
  } else {
    if ($export eq "html"){
      print "<p>";
      print "<p><p><b>Why should I care?</b>";
      print &htmlify($_[0]);
      print "</p>\n</li>\n";
    } else {
      # text export
      print "$_[0]\n";
    }
  }
}

# htmlify function: turn plain text into html
sub htmlify{
  my($s) = @_;
  $s = encode_entities($s);
  $s =~ s+&lt;http:(.*?)&gt;+<a href="http:$1">http:$1</a>+gs;
  $s =~ s+&lt;mailto:(.*?)&gt;+<a href="mailto:$1">$1</a>+gs;
  $s =~ s=\*(\S+)\*=<strong>$1</strong>=g; # *word* becomes boldified
  $s =~ s=\b\_(\S+)\_\b=<em>$1</em>=g;     # _word_ becomes italicized
  return($s);
}

__END__
