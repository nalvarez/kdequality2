#! /usr/bin/env perl

# Copyright 2010 by Adriaan de Groot <groot@kde.org>
#
# Driver script that calls nudox with appropriate arguments derived
# from a config file. See $usage, below.
#
#
# This file is released under the terms of the GNU General Public License
# (GPL) version 2 or, at your option, version 3 or any later version
# approved by the membership of KDE e.V.

use strict;
use Config::IniFiles;
use File::Basename;
use Getopt::Long;
use Template;

# Find out where this script lives (needed to find nudox and lib / share
# directories as needed.
use Cwd 'abs_path';
my $scriptpath = abs_path($0);
die '! Cannot find my own path.\n' unless -f $scriptpath;

my $prog = 'update-dox';
my $VERSION = '0.1';
my $usage = 
'# update-dox.pl [--verbose] [--dry-run] 
#		[--sections=s[,s...]]
#               [--modules=s[,s...]]
#               [--srcprefix=path] [--destprefix=path]
#		configfile
#
# The configfile is an ini-style file with sections [apidox] and
# [apidox name] (multiple named sections are possible) as described
# in README.apidox. If you pass in --verbose, then the script will
# print lots of debugging information. If you pass --dry_run, it 
# will not execute any commands; use --verbose --dry-run to find out
# what the commands would actually be. If you use --sections, you
# will generate APIDOX for exactly those named sections (use comma-
# separated names as they appear in the configfile), otherwise
# those sections that have an enabled=1 key will be generated.
#
# A section corresponds to a checkout of a whole KDE branch;
# it contains SVN modules (like kdelibs or kdebase).
#
# By default, all modules in a section are processed; pass in
# a list of modules if you want to restrict that. For instance,
# to generate only kdelibs module from section (collection) trunk,
# use --sections=trunk --modules=kdelibs
#
# Verbose will make the script print out debug messages with a #
# Dry_run will make the script print the commands it will execute,
# but not run them. Without dry run, verbose will print and run them.
';


my $verbose = 0;
my $dry_run = 0;
# You can enable only specific sections (apidox collections) for generation
# by passing in the names.
my $section_names;
my %sections;
my $module_names;
my %modules;

# Other overrides for config values:
my ($srcprefix,$destprefix);

die $usage unless GetOptions(
  'verbose'    => \$verbose,
  'dry-run'    => \$dry_run,
  'sections=s' => \$section_names,
  'modules=s'  => \$module_names,
  'srcprefix=s'  => \$srcprefix,
  'destprefix=s' => \$destprefix,
);
  
die $usage unless exists $ARGV[0];

my $configfile = $ARGV[0];
die $usage . '! Cannot find configuration file "' . $configfile . '"\n'
  unless -f $configfile;

my $config = new Config::IniFiles( -file => $configfile );
die $usage . '! No [apidox] section in config file.\n'
  unless $config->SectionExists('apidox');

$verbose += $config->val('apidox','verbose') ? 1 : 0;


### Get the path to the nudox script, either from the configfile
### or in the same directory.
my $nudox = $config->val('apidox','nudox');
$nudox = dirname($scriptpath) . '/nudox.pl' unless $nudox;

die '! Cannot find a nudox.pl via configuration file or in my own directory.\n' 
  unless -f $nudox;


### Figure out a suitable datadir based on config, idealized install
### location and the source layout.
my $datadir = $config->val('apidox','datadir');
$datadir = abs_path($datadir) if $datadir;
unless ($datadir) {
  print "# No datadir set, trying defaults.\n" if $verbose;
  $datadir = abs_path(dirname($scriptpath) . "/../share/apidox/");
  print "# ../share/apidox unsuitable.\n" if $verbose and not $datadir;
  $datadir = abs_path("./data/") unless $datadir;
  print "# ./data/ unsuitable.\n" if $verbose and not $datadir;
}
die ('! Cannot find a suitable datadir.' . 
   $datadir ? "(Tried $datadir)\n" : "\n") unless -d $datadir;

### Find a tagdir, based on config or the datadir.
my $tagdir = $config->val('apidox','tagdir');
$tagdir = abs_path($tagdir) if $tagdir;
unless ($tagdir) {
  print "# Setting tagdir to datadir ($datadir)\n" if $verbose;
  $tagdir = $datadir;
}
die '! Tag directory ' . $tagdir . " is not a directory.\n"
  unless -d $tagdir;

$srcprefix = $config->val('apidox','srcprefix') unless $srcprefix;
$destprefix = $config->val('apidox','destprefix') unless $destprefix;

### Inform the use what we've found so far
print "# script  = $nudox\n" if $verbose;
print "# datadir = $datadir\n" if $verbose;
print "# tagdir  = $tagdir\n" if $verbose;

### If --sections is used, convert to a list of section names,
### normalise to [apidox name] and store those as the acceptable
### sections; otherwise go through the config and find the ones
### that are enabled.
my $s;
if ($section_names) {
  for $s (split /,/,$section_names) {
    print "# Enabling section $s\n" if $verbose;
    if ($s =~ /^apidox /) { 
      $sections{$s}=1; 
    } else {
      $sections{"apidox $s"}=1;
    }
  }
} else {
  for $s ($config->GroupMembers('apidox')) {
    $sections{$s}=1 if $config->val($s,'enabled');
  }
}

if ($module_names) {
  for $s (split /,/,$module_names) {
    print "# Enabling module $s\n" if $verbose;
    $modules{$s}=1;
  }
}


### Preliminaries: list, check suitability, create directories.
&listSections() if $verbose;
my %commands = &checkSections();

### Do the actual work: generate shell commands and execute them.
my ($i,@cs,$cmd);
for $i (keys %commands) {
  @cs = &produceCommand($commands{$i});
  for $cmd (@cs) {
    print "$cmd\n" if $verbose;
    system($cmd) unless $dry_run;
  }
}

# If we processed the entire collection, then generate an index, too.
&createIndex() unless $module_names;

# Just print out the modules from the config file, listing them
# with a * if they are enabled. Debugging / verbose purposes only.
sub listSections() {
  my $first = 1;
  my $i;
  my ($enabled, $name);
  for $i ($config->GroupMembers('apidox')) {
    print $first ? '# modules = ' : '#           ';
    $enabled = acceptSection($i) ? '* ' : '  ';
    $name = $i;
    $name =~ s/^apidox //;
    print $enabled . $name . "\n";
    $first = 0;
  }
}

# Look in $section for $key; if that is not there, fall back
# to the name of the section (with apidox removed and characters
# cleaned up). Then modify relative directories to use the given
# prefix.
sub getDirectoryWithPrefix($$$$) {
  my ($config,$section,$key,$prefix) = @_;
  my $v;

  $v = $config->val($section,$key);
  unless ($v) {
    my $name = $section;
    $name =~ s/apidox //;
    $name =~ s/[^-a-zA-Z0-9_]//g;
    $v = $name;
  }
  unless ($v =~ /^\//) {
    # This is a relative path, so prepend the prefix
    $v = $prefix . '/' . $v;
  }
  return $v;
}

# Check that each section is defined sensibly. Checks if files
# and directories exist, creates directories as needed, and dies
# if something goes wrong. Returns a hash keyed by section name,
# with lists as values; each list starts with the source, destination
# directories and the tag file, followed by the names of the modules
# under that directory.
sub checkSections() {
  my $i;
  my $fail = 0;

  my %commands = ();

  for $i ($config->GroupMembers('apidox')) {
    next unless acceptSection($i);
    my ($src,$dest,$tag);
    $src = getDirectoryWithPrefix($config,$i,'srcdir',$srcprefix);
    unless (-d $src) {
      print "! $src is not a directory in $i.\n";
      print "! ... no valid source directory found.\n";
      $fail = 1;
      next;
    }

    $dest = getDirectoryWithPrefix($config,$i,'destdir',$destprefix);
    if (-e $dest and ! -d $dest) {
      print "! Destination $dest is not a directory.\n";
      $fail = 1;
      next;
    }
    mkdir $dest;
    unless (-d $dest) {
      print "! Destination $dest is not a directory.\n";
      $fail = 1;
      next;
    }

    $tag = $config->val($i,'qttag');
    unless ($tag) {
      print "# No explicit Qt tag for $i, using default.\n" if $verbose;
      $tag = $config->val('apidox','qttag');
      unless ($tag) {
	print "! No default value for qttag given in apidox, needed for $i.\n";
	$fail = 1;
	next;
      }
    }
    unless (-f $tag) {
      # Adjust the tag against the tagdir
      $tag = $tagdir . '/' . $tag;
    }
    # Using the possibly adjusted value
    unless (-f $tag) {
      print "! No tag file found for $i.\n";
      $fail = 1;
      next;
    }

    my @c = ($src,$dest,$tag);
    print "# Checking for modules under $src.\n" if $verbose;

    # Now find the sub-modules of that top-level directory to
    # determine all of the nudox invocations that we'll need.
    my @subdirs = &scansubdirsforfile($src,'Mainpage.dox',%modules);
    push @c,@subdirs;

    $commands{$i}=\@c;
  }

  die "! Errors found in configuration.\n" if $fail;

  return %commands;
}

sub scansubdirsforfile($$$) {
  my ($dir,$filename,%filter) = @_;

  my @subdirs = ();
  unless (opendir(DIR, $dir)) {
    print "! Cannot read $dir to find modules.\n";
    return @subdirs;
  }
  @subdirs = grep { -f "$dir/$_/$filename" and not $_ =~ /^\./ } readdir(DIR);
  closedir(DIR);
  print "# ... " . (0+@subdirs) . " candidate modules.\n" if $verbose;
  if (%filter) {
    @subdirs = grep { $filter{$_} } @subdirs;
    print "# ... filtered down to " . (0+@subdirs) . " modules.\n" if $verbose;
  }

  return @subdirs;
}


# Turn the list ($src,$dest,$tag,...) into a list of nudox commands
# that will do the actual generation.
sub produceCommand {
  my(@a) = @{$_[0]};
  my ($src,$dest,$tag) = ($a[0],$a[1],$a[2]);
  my @commands = ();

  my $verbose_arg = $verbose ? '--verbose' : '';
  my $i = 3; # Skip the src, dest, tag items in the list
  while ($i <= $#a) {
    push @commands,("cd '$dest' ; perl '$nudox' $verbose_arg --qtdocdir=http://doc.qt.nokia.com/ --qtdoctag='$tag' --doxdatadir='$datadir' '$src/$a[$i]'");
    $i++;
  }

  return @commands;
}

# Use the globally-populated %sections hash to check if the given
# section (e.g. [apidox trunk]) is enabled for generation.
sub acceptSection($) {
  my $name = $_[0];
  return $sections{$name};
}

sub createIndex()
{
  my $tt;
  
  for $i ($config->GroupMembers('apidox')) {
    next unless acceptSection($i);
    print "# Creating index page for $i.\n" if $verbose;

    my $section_name = $i;
    $section_name =~ s/apidox //;

    my $dest;
    $dest = getDirectoryWithPrefix($config,$i,'destdir',$destprefix);

    # Look for modules generated there
    my @subdirs = &scansubdirsforfile($dest,'index.html',%modules);

    $tt = Template->new({
      OUTPUT_PATH => $dest,
      RELATIVE => 1,
      ABSOLUTE => 1,
      STRICT   => 1,
      DEBUG    => 'undef',
      PRE_PROCESS => "$datadir/capacity-index-header.html",
      POST_PROCESS => "$datadir/capacity-index-footer.html",
      }) || die "! ", Template->error(), "\n";

    my $date = gmtime;
    my $vars = {
      date        => $date,
      site        => "http://www.kde.org",
      collection  => $section_name,
      modules     => \@subdirs,
    };

    unless ($tt->process( "$datadir/capacity-index-content.html", $vars, "index.html" )) {
      die "! ", $tt->error(), "\n";
    }
  }
}

