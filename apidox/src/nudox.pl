#! /usr/bin/env perl

# Copyright (C) 2006,2010 by Adriaan de Groot
#
# This file is released under the terms of the GNU General Public License
# (GPL) version 2.

use strict;
use Getopt::Long;
use File::Basename;
use File::Path;
use Cwd 'abs_path';
use Template;

my $Prog = 'nudox';
my
$VERSION = '0.9';

my $doxdir = abs_path(dirname($0));

# Flags for command-line args that do not influence the
# functionality of the program.
my $help = 0;
my $version = 0;
my $verbose = 0;

my $qtdocdir = '';
my $qtdoctag = '';
my $style = 'capacity';
# If set to 1, remove file artifacts from the APIDOX process
my $cleanup = 0;
# If set to 1, stop after processing the first subdirectory;
# this is useful for quick debugging. Undocumented option.
my $short_run = 0;

exit 1 if ( not GetOptions(
  'style=s'        => \$style,
  'doxdatadir=s'   => \$doxdir,
  'qtdocdir=s'     => \$qtdocdir,
  'qtdoctag=s'     => \$qtdoctag,
  'cleanup'        => \$cleanup,
  'help'           => \$help,
  'version'        => \$version,
  'verbose'        => \$verbose,
  'short'          => \$short_run,
   ) );

# Grab the first left-over argument; if there is none, act as if --help given.
my $srcdir = $ARGV[0];
$help=1 unless $srcdir;

&Version() if $version;
&Help() if $help;
exit 0 if $version or $help;


my $module = basename( $srcdir );

-d $srcdir or die "! Source '$srcdir' is not a directory.";
$doxdir and $module and $srcdir or die "! Insufficient arguments";

# This maps DOXYGEN_* (not DOXYGEN_SET_*) assignments
# to valid Doxyfile settings.
my %doxysettingsmap = (
  "NAME" => "PROJECT_NAME",
  "VERSION" => "PROJECT_NUMBER",
  ) ;
# These are DOXYGEN_* assignments that take special processing
# and are not converted to Doxyfile settings.
my %nudoxsettings = (
  "ENABLE" => 1,
  "REFERENCES" => 1,
  ) ;

# This is a map of deprecated DOXYGEN_SET_* entries, so where
# a Mainpage.dox uses e.g. DOXYGEN_SET_PROJECT_NAME it should
# use DOXYGEN_NAME instead.
my %deprecateddoxysettings = (
  "PROJECT_NAME" => "NAME",
  ) ;

# These are the settings extracted from the top-level doxyfile
# and applied to all the lower-level ones.
my @doxytoplevelsettings = ( "PROJECT_NAME", "PROJECT_NUMBER" );


&createoutputdir();
-d $module or die "! Target '$module' is not a directory.";

&lookforqttags();
&generatedoxyfile();
my @subdirs = &findsubdirs();
&createsubdirsfile();
&createsubdirsmenu();
my %doxy_toplevel = extract_doxysettings($srcdir,'Mainpage.dox');
&createlocaldoxyfiles();
&rundoxygen(1);
&createHTMLtemplates();
&updatedoxyfilesforpass2();
&rundoxygen(2);
&postprocess();
&cleanupfiles() if $cleanup;

###
#
# Create the directory that we will write all the APIDOX into.
#
sub createoutputdir() {
  mkpath("$module");
  die "! Target directory '$module' can not be created." unless -d $module;
}

###
#
# Search for QT tagfile
#
# As documented:
#  Return 0 = TAG exists and is a file and QTDOCDIR is set
#  Return 1 = TAG doesn't exist
#
# Funtion looktorqttags() does the work, the other bits are
# helpers for various messages.

sub tagsnotfound() {
  print "! Qt documentation can not be found.\n";
  die "! Use --qtdocdir and --qtdoctag.\n";
}

sub tagsexist() {
  print "# Using Qt tag file '$qtdoctag'\n" if $verbose;
  system("cp \"$qtdoctag\" \"$module\"/qt.tag");
  writetags();
}

sub tagsneeded() {
  print "# Generating Qt tag file from '$qtdocdir'\n" if $verbose;
  system("doxytag -t \"$module\"/qt.tag \"$qtdocdir\"");
  writetags();
}

sub writetags() {
  open TAGFILE, ">$module/tagfiles" or die "! Can not write tagfiles\n";
  print TAGFILE "\"qt.tag=$qtdocdir\"";
  close TAGFILE;
}

# Look for the QT tag file and exit(1) if not found.
sub lookforqttags() {
  if ( -f $qtdoctag ) {
    print "# Checking Qt tags file '$qtdoctag'\n" if $verbose;
    if (!open TAGF, "<$qtdoctag") {
      print "! Can not read given file '$qtdoctag'\n";
      tagsnotfound();
    }

    my $firstline = <TAGF>;
    if (! $firstline =~ /tagfile/ ) {
      print "! File '$qtdoctag' does not seem to be a tag file\n";
      tagsnotfound();
    }

    if ( $qtdocdir =~ /^$/ ) {
      print "! No documentation directory given\n";
      tagsnotfound();
    }

    if ( not -d $qtdocdir ) {
      print "# Qt documentation directory is not a directory,\n" if $verbose;
      print "# but I'll use it anyway because of the tag file.\n" if $verbose;
    }

    tagsexist();
    return 0;
  }

  if ( -d $qtdocdir and -f "$qtdocdir/index.html" and -f "$qtdocdir/qstring.html" ) {
    print "# Qt documentation will be generated from '$qtdocdir'\n" if $verbose;
    tagsneeded();
    return 0;
  }

  print "! No documentation directory given\n";
  tagsnotfound();
}


###
#
# Create an global doxygen file with settings that apply to
# all the parts to be generated; in rare cases we will 
# override these settings with the local doxyfile.
#
sub generatedoxyfile() {
  print "# Generating initial Doxygen file ...\n" if $verbose;
  # $data is for copying file contents
  my $data;
  open( DOXYFILE, ">$module/Doxyfile.global" ) or 
    die "! Can not write Doxyfile.global\n";
  open( DOXYGLOBAL, "<$doxdir/Doxyfile.global" ) or
    die "! Can not open Doxyfile.global in $doxdir.\n";
  read DOXYGLOBAL, $data, 16384;
  close DOXYGLOBAL;
  print DOXYFILE "# From $doxdir/Doxyfile.global.\n";
  print DOXYFILE $data;

  if ((-f "$srcdir/doc/api/Doxyfile.local") and 
      (open ( DOXYLOCAL, "<$srcdir/doc/api/Doxyfile.local" ))) {
    print "# Adding doc/api/Doxyfile.local.\n" if $verbose;
    # Assume file is less than 10k
    read DOXYLOCAL, $data, 10240;
    close DOXYLOCAL;
    print DOXYFILE "# From $srcdir/doc/api/Doxyfile.local.\n";
    print DOXYFILE $data;
  }

  # Dynamic settings
  print DOXYFILE "STRIP_FROM_PATH = " . dirname($srcdir) . "\n";
  close( DOXYFILE );
}


###
#
# Find those subdirs that should generate APIDOX. These are
# (in KDE4) those directories that have a Mainpage.dox.
#
sub findsubdirs() {
  open OUTDIRS, ">$module/subdirs.in" or
    die "! Can not write subdirs.in list.\n";
  open SUBDIRS, "find $srcdir -name Mainpage.dox |" or
    die "! Can not start reading subdirectories for Mainpage.dox\n";

  my @subdirlist = ();
  while (<SUBDIRS>) {
    chomp;
    s+^$srcdir++;
    s+/Mainpage.dox$++;
    next if /^$/;
    next if /^Mainpage.dox/;
    # Skip KDE3 directories
    next if -f "$srcdir/$_/Makefile.am";
    push @subdirlist, $_;
  }

  close SUBDIRS;

  print OUTDIRS sort(@subdirlist);
  close OUTDIRS;

  return sort(@subdirlist);
}


###
#
# We need the list of subdirs in a file for later use by the shellscript.
#
sub createsubdirsfile() {
  open( SUBDIRS, ">$module/subdirs" ) or
    die "! Can not create subdir list\n";
  print SUBDIRS join("\n",@subdirs);
  close( SUBDIRS );
}


###
#
# Scan a Mainpage.dox for DOXYGEN_* assignments.
#
sub extract_doxysettings($$) {
  my ($dirname,$filename) = @_;
  my %v = ();
  open( MAINPAGE, "<$dirname/$filename" ) or die "Can not open '$filename'.";
  while ( <MAINPAGE> ) {
    next unless /^\/\/ DOXYGEN_/;
    chomp;
    s/^.. DOXYGEN_//;
    my $name = $_;
    my $assignment = $_;
    $name =~ s/[\s=+].*//;
    $assignment =~ s/$name\s*//;

    # We have the convention that DOXYGEN_SET_* writes raw
    # Doxygen settings to the file, while DOXYGEN_* for other
    # values are "special" and get mapped around.
    if ($name =~ /^SET_/) {
      $name =~ s/^SET_//;
      if (exists($deprecateddoxysettings{$name})) {
	print "! Using deprecated DOXYGEN_SET_$name in $filename.\n";
	print "! ... use DOXYGEN_" . $deprecateddoxysettings{$name} . " instead.\n";
      } else {
        # Nothing to do, keep name as is.
      }
    } else {
      # This ought to be a special one.
      if (exists $doxysettingsmap{$name}) {
        $name = $doxysettingsmap{$name};
      } elsif (exists $nudoxsettings{$name}) {
        # That's fine, keep the name; it doesn't need remapping
      } else {
	print "! Unknown special setting $name in $filename.\n";
	next;
      }
    }

    # Start accumulating possible multi-line value
    my $tvalue = $assignment;
    my $line = $assignment;
    while ($line =~ /\\$/) {
      $line = <MAINPAGE>;
      chomp $line;
      die "Invalid continuation line in $filename.\n" unless $line =~ /^\/\//;
      $line =~ s/^\/\/\s*//;
      $tvalue .= "\\\n\t$line";
    }

    $v{$name} = $tvalue;
  }
  close( MAINPAGE );
  return %v;
}

###
#
# Write the HTML menu code for all the subdirs.
#
sub createsubdirsmenu() {
  open( SUBDIRHTML, ">$module/subdirs.inc" ) or die "! Can not create $module/subdirs.inc.\n";
  my @l = ();
  my $depth = 0;
  for my $dir (@subdirs) {
    my @dirs = split( "/", $dir );
    my $i = 0;
    while ($l[$i] eq $dirs[$i]) {
      $i++;
    }
    if ($i>$depth) {
      my $j=$depth;
      while ($j<$i) {
        print SUBDIRHTML "<ul class='submenu' style='margin-left: 1ex;'>";
        $j++;
      }
    }
    if ($i<$depth) {
      my $j=$i;
      while ($j<$depth) {
        print SUBDIRHTML "</ul>";
        $j++;
      }
    }
    print SUBDIRHTML "<li><a href=\"[%top%]" . $dir . '/index.html">';
    print SUBDIRHTML $dirs[$i];
    print SUBDIRHTML "</a></li>\n";

    @l = @dirs;
    $depth = $i;
  }
  if (0<$depth) {
    while (0<$depth) {
      print SUBDIRHTML "</ul>";
      $depth--;
    }
  }
  close( SUBDIRHTML );
}


# Doxygen settings are stored in the doxy_toplevel and
# other settings hashes with the assignment operator (= or +=)
# intact; remove that and return the bare value.
sub sanitize_doxyvar($) {
  return undef unless $_[0];
  my $v = $_[0];
  $v =~ s/^[\s=+]*//;
  return $v;
}

###
#
# Create the header and footer files for each subdirectory,
# also create Doxyfile.local for each subdirectory to read
# those header and footers.
#
sub createlocaldoxyfiles() {
  my $hdr = "header.html";
  my $ftr = "footer.html";

  for my $dir (@subdirs,'.') {
    # Skip directories that are still KDE3 structure (used in playground)
    next if -f "$srcdir/$dir/Makefile.am";
    my $subdir_name = basename($dir);

    print SUBDIRS "$dir\n";
    mkpath("$module/$dir");

    print "# Creating Doxyfile.local for '$dir'\n" if $verbose;
    open( DOXYFILE , ">$module/$dir/Doxyfile.local" ) or die "Can not write Doxygen settings in '$dir'.";

    print DOXYFILE "RECURSIVE        = NO\n" if $dir eq ".";
    print DOXYFILE "INPUT            = \"$srcdir/$dir/\"\n";
    print DOXYFILE "OUTPUT_DIRECTORY = \"$module/$dir/\"\n";
    print DOXYFILE "GENERATE_TAGFILE = \"$module/$dir/$subdir_name.tag\"\n";

    my %doxy_local = extract_doxysettings($srcdir,"$dir/Mainpage.dox");
    print DOXYFILE "# Local settings\n";
    foreach my $k (keys(%doxy_local)) {
      print DOXYFILE $k . $doxy_local{$k} . "\n" unless exists $nudoxsettings{$k};
    }
    # The entries in nudoxsettings need special handling.
    if (exists $doxy_local{'REFERENCES'}) {
      print DOXYFILE "# Local tagfiles.\n";
      my @refs = split /\s+/,$doxy_local{'REFERENCES'};
      my @refstrings = ( "TAGFILES =" );
      for my $r (@refs) {
        next if $r eq "=";
        my $tagfile = basename($r);
        push @refstrings,"$module/$r/$tagfile.tag=../$r";
      }
      # And add the Qt tagfile too
      push @refstrings,"$module/qt.tag=$qtdocdir";
      print DOXYFILE join(" \\\n\t",@refstrings) . "\n";
    }
    print DOXYFILE "# Toplevel settings\n";
    foreach my $k (@doxytoplevelsettings) {
      if (exists($doxy_toplevel{$k}) and not exists($doxy_local{$k})) {
        print DOXYFILE $k . $doxy_toplevel{$k} . "\n";
      }
    }

    print DOXYFILE "# Calculated settings\n";
    my @subsubs = ();
    for my $d (@subdirs) {
      next unless $d =~ /^$dir\//;
      push @subsubs, "$srcdir/$d/";
    }

    print DOXYFILE "EXCLUDE = ", join( " \\\n", @subsubs ), "\n" if @subsubs;
    print DOXYFILE "HTML_HEADER=\"$module/$dir/$hdr\"\n";
    print DOXYFILE "HTML_FOOTER=\"$module/$dir/$ftr\"\n";

    close( DOXYFILE );

    # Create bogus header and footer files
    open( OUTF, ">$module/$dir/$hdr" ) or 
      die "! Could not create header in $module/$dir\n";
    close( OUTF );
    open( OUTF, ">$module/$dir/$ftr" ) or 
      die "! Could not create footer in $module/$dir\n";
    close( OUTF );

    # Concatenate the global and the local files to a Doxyfile
    open (INFGLOBAL, "<$module/Doxyfile.global" ) or
      die "! Could not read $module/Doxyfile.global.\n";
    open (INFLOCAL,  "<$module/$dir/Doxyfile.local" ) or
      die "! Could not read $module/$dir/Doxyfile.local.\n";
    open (DOXYFILE, ">$module/$dir/Doxyfile" ) or
      die "! Could not create $module/$dir/Doxyfile.\n";
    my $data;
    print DOXYFILE "### Doxyfile.global\n";
    read INFGLOBAL, $data, 16384;
    close INFGLOBAL;
    print DOXYFILE $data;
    print DOXYFILE "\n### Doxyfile.local\n";
    read INFLOCAL, $data, 16384;
    close INFLOCAL;
    print DOXYFILE $data;
    print DOXYFILE "\n";
    close DOXYFILE;
  }
}


# Each subdir has a number of index-like files; this function
# creates a component-menu file that links to those indexes.
sub createcomponentmenu($) {
  my $dir = $_[0];

  print '# Creating menu in ' . $dir . "\n" if $verbose;

  # This is a crufty way to give keys in a hash an order:
  # each key has a two-digit code prepended to the key
  # itself; we use the key as user-visible string for
  # the documentation, and the value the key points to
  # as the filename.
  my %componententries = (
    '00Classes' => 'hierarchy.html',
    # '00Classes' => 'classes.html',
    '01Class List' => 'annotated.html',
    '02Files' => 'files.html',
    # '03Functions' => 'functions.html',
    );

  open MENUFILE,">$dir/menu.inc" or
    die "! Could not write $dir/menu.inc.\n";
  print MENUFILE "<ul class='submenu' style='margin-left: 1ex;'>\n";
  my @ks = keys(%componententries);
  foreach my $k (sort @ks) {
    my $name = $k;
    # Undo the crufty encoding to keep entries in order
    $name =~ s/^[0-9][0-9]//;
    my $filename = $componententries{$k};
    print MENUFILE "<li><a href='$filename'>$name</a></li>\n" if -f "$dir/$filename";
  }
  print MENUFILE "</ul>\n";
  close MENUFILE;
}

sub createHTMLtemplates() {
  my $tt = Template->new({
    OUTPUT_PATH => $module,
    RELATIVE => 1,
    ABSOLUTE => 1,
    STRICT   => 1,
    DEBUG    => 'undef',
    }) || die "! ", Template->error(), "\n";
  my $hdr = "header.html";
  my $ftr = "footer.html";


  for my $dir (@subdirs,'.') {
    # Skip directories that are still KDE3 structure (used in playground)
    next if -f "$srcdir/$dir/Makefile.am";
    my $subdir_name = basename($dir);

    createcomponentmenu($module . '/' . $dir);

    my $breadcrumb_path = "";
    my @l = ();
    my @up = ();
    for my $i (split("/","$module/$dir")) {
      next unless $i;
      push @l,$i;
      push @up,"..";
    }
    pop @up;
    my $toplevel = join( "/", @up );

    # Toplevel has an extra bogus . at the end.
    pop @l if $dir eq ".";
    pop @up if $dir eq ".";
    # Construct a breadcrumb list for all the uplevels
    for my $i (@l) {
      my $item=$i;
      $item="<a href=\"" . join("/",@up) . "/index.html\">" . $i . "</a>" if @up;
      $breadcrumb_path .= "<li>$item</li>\n";
      pop @up;
    }
    $toplevel="." if $dir eq ".";

    my $date = gmtime;
    my $module_name = sanitize_doxyvar($doxy_toplevel{'PROJECT_NAME'}) or $module;
    my $module_version = sanitize_doxyvar($doxy_toplevel{'PROJECT_NUMBER'});
    $module_version = 'v.XX' unless $module_version;

    my $vars = {
      date        => $date,
      top         => $toplevel,
      site        => "http://www.kde.org",
      module      => $module,
      module_name => $module_name,
      version     => $module_version,
      dir         => $dir,
      subdir      => $subdir_name,
      breadcrumbs => $breadcrumb_path
    };

    my $flavour = "";
    $flavour = "toplevel-" if $dir eq ".";

    unless ($tt->process( "$doxdir/$style-$flavour$hdr", $vars, "$dir/$hdr" )) {
      die "! ", $tt->error(), "\n";
    }

    unless ($tt->process( "$doxdir/$style-$flavour$ftr", $vars, "$dir/$ftr" )) {
      die "! ", $tt->error(), "\n";
    }
  }
}

sub updatedoxyfilesforpass2() {
  for my $dir (@subdirs) {
    open DOXYFILE,">>$module/$dir/Doxyfile" or
      die "! Could not re-open $dir/Doxyfile in pass 2.\n";
    print DOXYFILE "# Pass 2 additions.\n";
    my $subdir_name = basename($dir);
    print DOXYFILE "
# Enable Qt Help
GENERATE_QHP = YES
QCH_FILE = $subdir_name.qch
QHP_NAMESPACE = kde.org.$module
QHG_LOCATION = /usr/bin/qhelpgenerator
" if -x '/usr/bin/qhelpgenerator';
    print DOXYFILE "\n";
    close DOXYFILE;
  }
}


###
#
# Actually run Doxygen on all of the directories we have.
#
sub rundoxygen($) {
  # Create empty file
  open( OUTF, ">$module/Doxygen.log" ) or die "! Can not open doxygen logfile.\n";
  close( OUTF );

  # Generate all of them
  for my $dir (@subdirs) {
    &rundoxygen_subdir($dir,$_[0]);
    last if $short_run;
  }
  rundoxygen_subdir('.',$_[0]);
}

sub rundoxygen_subdir($$) {
  my $dir = $_[0];
  my $pass = $_[1];
  print "# Generating APIDOX in $dir (pass $pass)\n" if $verbose;
  my $outfile = '/dev/null';
  $outfile = "\"$module/Doxygen.log\"" if $pass > 1;
  my $cmd;
  $cmd = "doxygen \"$module/$dir/Doxyfile\" >> $outfile 2>&1";
  print "$cmd\n" if $verbose; 
  system( $cmd );
}

sub postprocess() {
  # No longer any post-processing necessary.
}

sub cleanupfiles() {
  print "# Cleaning up build artifacts.\n" if $verbose;
  my @subdirfiles = (
    'menu.inc',
    'Doxyfile',
    'Doxyfile.local',
    'footer.html',
    'header.html',
    'installdox',
  );
  my @toplevelfiles = (
    'subdirs',
    'subdirs.in',
    'subdirs.inc',
    'Doxyfile',
    'Doxyfile.global',
    'Doxyfile.local',
    'Doxygen.log',
    'tagfiles',
    'qt.tag',
    'menu.inc',
    'footer.html',
    'header.html',
  );


  for my $dir (@subdirs) {
    for my $f (@subdirfiles) {
      unlink "$module/$dir/$f";
    }
  }
  for my $f (@toplevelfiles) {
    unlink "$module/$f";
  }
}

sub Help() {
  &Version();

  print "# nudox.pl [options] src-dir
#
# --doxdatadir=dir   Use dir as the source of global Doxygen files.
# --qtdocdir=dir     Dir (or URL) where Qt documentation is.
# --qtdoctag=tagfile Qt documentation tag file.
# --style=name       Use HTML style <name> (default none).
# --cleanup          Remove build artifacts from output tree.
# --verbose          Be verbose, print commands before execution.
# --help             Print this help.
# --version          Print version and exit.
#
# If the Qt tag file exists, you can use --qtdocdir to point to a URL; 
# otherwise it must be the Qt HTML documentation directory.
#
";
}

sub Version() {
  print "$Prog, version $VERSION\n";
  exit 0 if $version;
}
