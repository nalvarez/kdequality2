#! /usr/bin/env python

import re
import os
import string
import sys
import getopt

class File:
	def __init__(self,string):
		self.qmlFile = string + ".qml"
		self.fullPath = ""
		self.className = string
		self.properties = {}
		self.inherits = []
		self.imports = []
		self.description = ""
		self.signals = {}
		self.methods = {}
		self.newItem = True
		self.error = False
		self.errorLine = 0
	def createApi(self):
		print self.className
		f = open(self.fullPath, "r")
		self.apiFlags = {"Inherits:" : False, "Imports:" : False, "Description:" : False, "Properties:" : False, "Signals:" : False, "Methods:": False}
		insideApi = False
		inherits = {}
		i = 0
		self.error = False
		try:
			for line in f:
				if "/**" in line:
					insideApi = True
					self.newItem = True
				elif "**/" in line:
					insideApi = False
				if insideApi == True:
					for key in sorted(self.apiFlags.keys()):
						if line.strip() == key:
							self.apiFlags[key] = True
						#It has the form of an apiflag, but is not this one, so set to false
						elif re.match("^[\w]+:$", line.strip()):
							self.apiFlags[key] = False
						elif self.apiFlags[key] == True and line != "\n" :
							if line.strip() != key:
								self.retrieveInherits(line.strip())
								self.retrieveImports(line.strip())
								self.retrieveDescription(line.strip())
								self.retrieveProperties(line.strip())
								self.retrieveSignals(line.strip())
								self.retrieveMethods(line.strip())

						#when there is an empty line we have a new property/method/section
						if line == "\n":
							self.newItem = True
				i = i + 1
		except:
			self.error = True
			self.errorLine = i

	def retrieveInherits(self,string):
		if self.apiFlags["Inherits:"] == True:
			self.inherits.append(string)
	
	def retrieveImports(self,string):
		if self.apiFlags["Imports:"] == True:
			self.imports.append(string)
	
	def retrieveDescription(self,string):
		if self.apiFlags["Description:"] == True:
			self.description = self.description + string +"\n"

	def retrieveProperties(self,string):
		isType = False
		isName = False
		isDesc = False
		propertyType = ""
		propertyName = ""
		propertyDescription = ""
		if self.apiFlags["Properties:"] == True:
			if self.newItem and string.endswith(":"):
				string = re.sub("^\*", "", string)
				propertyType = string.split()[0]
				propertyName = re.sub(":$", "", string).split()[1]
				self.properties[len(self.properties)] = {"name" : propertyName, "type" : propertyType, "description" :""}
			#a line with only the description
			else:
				if len(self.properties) > 0:
					self.properties[len(self.properties) -1 ]["description"] = self.properties[len(self.properties) -1 ]["description"] + string + "\n"
				else:
					self.properties[len(self.properties)] = {"name" : "", "type" : "", "description" : string + "\n"}
			self.newItem = False


	def retrieveSignals(self,string):
		signal_name = ""
		signal_description = ""
		if self.apiFlags["Signals:"] == True:
			if string.endswith(":"):
				#remove eventual * and : at start and end
				signal_name = re.sub("^\*", "", re.sub(":$", "", string))
				self.signals[len(self.signals.keys())] = { "name" : signal_name, "description" : signal_description }
			# we have a line with
			# signalName: description
			elif (re.match("[^ ]+:", string)):
				self.signals[len(self.signals)] = {"name" : re.sub(":.*", "", string),  "description" : re.sub("^[^ ]+:", "", string) + "\n"}
			#a line with only the description
			else:
				if len(self.signals) > 0:
					self.signals[len(self.signals) -1 ]["description"] = self.signals[len(self.signals) -1 ]["description"] + string + "\n"
				else:
					self.signals[len(self.signals)] = {"name" : "", "type" : "", "description" : string + "\n"}
			self.newItem = False
	
	def retrieveMethods(self,string):
		isType = False
		isName = False
		isDesc = False
		methodType = ""
		methodName = ""
		propertyDescription = ""
		if self.apiFlags["Methods:"] == True:
			if string.endswith(":"):
				string = re.sub("^\* *", "", string)
				methodType = string.split()[0]
				#dump last : and the first word (the type)
				methodName = re.sub(":$", "", re.sub("^[^ ]+ ", "", string))
				self.methods[len(self.methods)] = {"name" : methodName, "type" : methodType, "description" :""}
			#a line with only the description
			else:
				if len(self.methods) > 0:
					self.methods[len(self.methods) -1 ]["description"] = self.methods[len(self.methods) -1 ]["description"] + string + "\n"
				else:
					self.methods[len(self.methods)] = {"name" : "", "type" : "", "description" : string + "\n"}
			self.newItem = False
		

	def inheritsList(self):
		return self.inherits
	
	def importsList(self):
		return self.imports
	def descriptionList(self):
		return self.description
	def propertyList(self):
		return self.properties
	def methodsList(self):
		return self.methods
	def signalsList(self):
		return self.signals

	def normalizeString(self, string):
		return string.replace("\n", "<br/>\n").replace("<code>", '<div class="fragment"><pre class="fragment">').replace("</code>", "</pre></div>").replace("@arg", "<strong>Argument:</strong>")

	def writeHtml(self):
		contentsString = ""
		
		if self.error:
			contentsString += ("<p>Warning: parse error at line " + self.errorLine)
			return
		
		contentsString += ("<h1>" + self.className + "</h1>")
		contentsString += ("<h2>Description</h2>")
		contentsString += ("<p>")
		contentsString += (self.descriptionList().replace("\n", "<br/>\n"))
		contentsString += ("</p>")


		contentsString += ("<h2>Inherits</h2>")
		contentsString += ("<ul>")
		for i in self.inheritsList():
			contentsString += ("<li>" + i + "</li>")
		contentsString += ("</ul>")

		contentsString += ("<h2>Imports</h2>")
		contentsString += ("<ul>")
		for i in self.importsList():
			contentsString += ("<li>" + i + "</li>")
		contentsString += ("</ul>")

		if len(self.propertyList()) > 0:
			contentsString += ("<h2>Properties</h2>")
			contentsString += '<ul>'
			for key in range(len(self.propertyList())):
				contentsString += ('<li><a href="#' + self.propertyList()[key]["name"] + '">' + self.propertyList()[key]["name"] + "</a></li>")
			contentsString += '</ul>'

		if len(self.methodsList()) > 0:
			contentsString += ("<h2>Methods</h2>")
			contentsString += '<ul>'
			for key in range(len(self.methodsList())):
				contentsString += ('<li><a href="#' + self.methodsList()[key]["name"] + '">' + self.methodsList()[key]["name"] + "</a></li>")
			contentsString += '</ul>'

		if len(self.signalsList()) > 0:
			contentsString += ("<h2>Signals</h2>")
			contentsString += '<ul>'
			for key in range(len(self.signalsList())):
				contentsString += ('<li><a href="#' + self.signalsList()[key]["name"] + '">' + self.signalsList()[key]["name"] + "</a></li>")
			contentsString += '</ul>'

		if len(self.propertyList()) > 0:
			contentsString += ("<h2>Properties</h2>")
			for key in range(len(self.propertyList())):
				contentsString += ('<div class="memitem" id="' + self.propertyList()[key]["name"] + '"><div class="memproto"><table class="memname"><tr><td>' + self.propertyList()[key]["type"] + '</td><td class="memname">&nbsp;<strong>' + self.propertyList()[key]["name"] + "</strong></td></tr></table></div>")
				contentsString += ("<p>" + self.normalizeString(self.propertyList()[key]["description"]) + "</p></div>")

		if len(self.methodsList()) > 0:
			contentsString += ("<h2>Methods</h2>")
			for key in range(len(self.methodsList())):
				contentsString += ('<div class="memitem" id="' + self.methodsList()[key]["name"] + '"><div class="memproto"><table class="memname"><tr><td class="memname">' + self.methodsList()[key]["type"] + " " + self.methodsList()[key]["name"] + "</td></tr>")
				contentsString += ('<tr><td class="paramkey">Returns&nbsp;</td><td class="paramkey">' + self.methodsList()[key]["type"] + "</td></tr></table></div>")
				contentsString += ("<p>" + self.normalizeString(self.methodsList()[key]["description"]) + "</p></div>")

		if len(self.signalsList()) > 0:
			contentsString += ("<h2>Signals</h2>")
			for key in range(len(self.signalsList())):
				contentsString += ('<div class="memitem" id="' + self.signalsList()[key]["name"] + '"><div class="memproto"><table class="memname"><tr><td class="memname">' + self.signalsList()[key]["name"] + "</td></tr></table></div>")
				contentsString += ("<p>" + self.normalizeString(self.signalsList()[key]["description"]) + "</p></div>")

		outFile = open(outPath + '/' + self.className + '.html',"w")
		varg = (self.className, version, linksHtml, contentsString)
		outString = templateString.format(*varg)
		outFile.write(outString)
		outFile.close()


qmlPath = '.'
outPath = '.'
templatePath = '.'
version = '4.x'

try:                                
	opts, args = getopt.getopt(sys.argv[1:], "hi:o:t:v:", ["help"]) 
except getopt.GetoptError:           
	print "Usage: qmlapigenerator.py -v KDEversion -t /path/to/html/template -i /qml/files/path -o /html/output/path"                 
	sys.exit(2) 
for opt, arg in opts:                
	if opt in ("-h", "--help"):      
		print "Usage: qmlapigenerator.py -v KDEversion -t /path/to/html/template -i /qml/files/path -o /html/output/path"                     
		sys.exit()                  
	elif opt == '-i':                
		qmlPath = arg
	elif opt == "-o": 
		outPath = arg 
	elif opt == "-t":
		templatePath = arg
	elif opt == "-v":
		version = arg

templateFile = open(templatePath + '/qmlapidox-template.html', "r")
templateString = templateFile.read()
templateFile.close()

classes = list()
for root, subFolders, files in os.walk(qmlPath):
	for fileName in files:
		if fileName.endswith('.qml'):
			if (root.split('/')[-1] in ("test", "gallery", "touch", "private")): continue
			classes.append(fileName.replace(".qml", ""))

linksHtml = '<ul>\n'
for className in sorted(classes):
	linksHtml += '<li><a href="./' + className + '.html">' + className + '</a></li>\n'
linksHtml += '</ul>\n'

for root, subFolders, files in os.walk(qmlPath):
	for fileName in files:
		if fileName.endswith('.qml'):
			if (root.split('/')[-1] in ("test", "gallery", "touch", "private")): continue
			className = fileName.replace(".qml", "")
			parser = File(className)
			parser.fullPath = root + "/" + fileName
			parser.createApi()
			parser.writeHtml()

outFile = open(outPath + '/index.html',"w")
varg = ('Class list',version,linksHtml,linksHtml)
outFile.write(templateString.format(*varg))
outFile.close()



