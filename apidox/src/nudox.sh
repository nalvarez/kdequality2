#! /bin/sh
#
# Copyright (C) 2006 by Adriaan de Groot
#
# This file is released under the terms of the GNU General Public License
# (GPL) version 2.

# This is a script to generate APIDOX for a KDE SVN module.

# Output lines:
#
#     # comment
#     ! error
#     * progress information
#


with_tags=true

USAGE="Usage: doxygen.sh [options] SVN-module-path [application ...]"

DOXDATA=""
PREFIX=""
STYLE=""
# These might be set as environment variables, for backwards-compatibility
# QTDOCDIR=""
# QTDOCTAG=""

# Process command-line arguments. Include backwards-compatibility
# arguments from the older doxygen.sh.
while true ; do
case "x$1" in
x--without-tags )
	with_tags=false
	;;
x--with-tags )
	with_tags=true
	;;
x--no-cleanup|x--no-recurse|x--recurse|x--no-modulename|x--modulename)
	echo "# Ignored deprecated argument '$1'"
	;;
x--doxdatadir=* )
	DOXDATA=`echo "$1" | sed -e 's+--doxdatadir=++'`
	;;
x--installdir=*)
	PREFIX=`echo "$1" | sed -e 's+--installdir=++'`
	;;
x--qtdocdir=*)
	QTDOCDIR=`echo "$1" | sed -e 's+--qtdocdir=++'`
	;;
x--qtdoctag=*)
	QTDOCTAG=`echo "$1" | sed -e 's+--qtdoctag=++'`
	;;
x--style=*)
	STYLE=`echo "$1" | sed -e 's+--style=++'`
	;;
x--help)
	cat << _EOF
# $USAGE
#
# --without-tags     Do not generate tags file for module.
#                    Useful if the tags are already there.
# --doxdatadir=dir   Use dir as the source of global Doxygen files.
# --installdir=dir   Use dir as target for installation.
# --qtdocdir=dir     Dir (or URL) where Qt documentation is.
# --qtdoctag=tagfile Qt documentation tag file.
# --style=name       Use HTML style <name> (default none).
#
# Instead of --qtdocdir and --qtdoctag you may use the environment
# variables QTDOCDIR and QTDOCTAG. If the tag file exists, you can
# use --qtdocdir to point to a URL; otherwise it must be the
# Qt HTML documentation directory.
#
_EOF
	exit 0
	;;
x--*)
	echo "! $USAGE"
	echo "! Unknown option '$1' given."
	exit 1
	;;
x )
	echo "! $USAGE"
	echo "! No SVN-module-path given."
	exit 1
	;;
* )
	break
	;;
esac
shift
done


test -d "$1" || {
	echo "! $USAGE"
	echo "! Given path is not a directory."
	exit 1
}
test -f "$1"/Mainpage.dox || {
	echo "! $USAGE"
	echo "! The directory '$1' has no Mainpage.dox. No APIDOX will be generated."
	exit 1
}
test -z "$DOXDATA" && DOXDATA=`dirname "$0"`
test -d "$DOXDATA" || {
	echo "! $USAGE"
	echo "! The given doxdata '$DOXDATA' is not a directory. No APIDOX will be generated"
	exit 1
}


SRCDIR="$1"
MODULE=`basename "$1"`

# rm -rf "$MODULE"
mkdir "$MODULE"
test -d "$MODULE" || {
	echo "! Target directory '$MODULE/' can not be created."
	echo ""
	exit 1
}

# As documented:
#	Return 0 = TAG exists and is a file
#	Return 1 = TAG doesn't exist
perl `dirname "$0"`/nudox-helpqt.pl "$QTDOCDIR" "$QTDOCTAG" "$SRCDIR" "$MODULE" || exit 1



( cd "$SRCDIR" && find . -name Mainpage.dox ) | sed 's+/Mainpage.dox$++' | grep -v '^.$' | sort > "$MODULE/"subdirs.in


perl `dirname "$0"`/nudox-help1.pl "$DOXDATA" "$STYLE" "$MODULE" "$SRCDIR" < "$MODULE/"subdirs.in


for i in `cat "$MODULE/subdirs"`
do
	echo "# Generating APIDOX in $i"
	cat "$MODULE/Doxyfile.in" "$MODULE/$i/Doxyfile.local" > "$MODULE/$i/Doxyfile"
	doxygen "$MODULE/$i/Doxyfile"
done 2> "$MODULE/Doxygen.log"

cat "$MODULE/Doxyfile.in" "$MODULE/Doxyfile.local" > "$MODULE/Doxyfile"


