#include "exporthelper.h"

#include <QtCore/QDateTime>
#include <QtCore/QDebug>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>

/// ExportHelper::Private

struct ExportHelper::Private
{
  QSqlDatabase *mDatabase;

public: /// Functions
  /**
   * Returns the id of the last check for @param component or -1 if no check
   * is found.
   */
  qlonglong lastComponentCheckId( const QString &component );

  /**
   * Returns the id of the module submodule combination or -1 if no such combination
   * is found.
   */
  qlonglong moduleSubmoduleId( const QString &module, const QString &submodule );
};

qlonglong ExportHelper::Private::lastComponentCheckId( const QString &component )
{
  const QString queryStr( "SELECT id, date FROM component_check WHERE component=:comp" );
  QSqlQuery query( *mDatabase );
  query.prepare( queryStr );
  query.bindValue( ":comp", component );
  query.exec();

  qlonglong id = -1;
  QDateTime lastDateTime;
  while ( query.next() ) {
    QDateTime currentDateTime = QDateTime::fromString( query.value( 1 ).toString(), "MMMM dd yyyy hh:mm:ss" );
    if ( lastDateTime.isNull() || currentDateTime > lastDateTime ) {
      lastDateTime = currentDateTime;
      id = query.value( 0 ).toLongLong();
    }
  }

  return id;
}

qlonglong ExportHelper::Private::moduleSubmoduleId( const QString &module,
                                                    const QString &submodule )
{
  const QString queryStr( "SELECT id FROM module_submodule WHERE module=:mod AND submodule=:submod" );
  QSqlQuery query( *mDatabase );
  query.prepare( queryStr );
  query.bindValue( ":mod", module );
  query.bindValue( ":submod", submodule );
  query.exec();

  if ( query.next() )
    return query.value( 0 ).toLongLong();

  return -1;
}

/// ExportHelper

ExportHelper::ExportHelper( QSqlDatabase *database )
  : d( new ExportHelper::Private )
{
  d->mDatabase = database;
}

ExportHelper::~ExportHelper()
{
  delete d;
}

QStringList ExportHelper::components() const
{
  const QString queryStr( "SELECT DISTINCT component FROM component_check;" );
  QSqlQuery query( *d->mDatabase );
  query.exec( queryStr );

  QStringList components;
  while ( query.next() )
    components.append( query.value( 0 ).toString() );

  return components;
}

QStringList ExportHelper::languages() const
{
  const QString queryStr( "SELECT DISTINCT language FROM module_submodule_sloc;" );
  QSqlQuery query( *d->mDatabase );
  query.exec( queryStr );

  QStringList languages;
  while ( query.next() )
    languages.append( query.value( 0 ).toString() );

  return languages;
}

QStringList ExportHelper::modules() const
{
  const QString queryStr( "SELECT DISTINCT module FROM module_submodule;" );
  QSqlQuery query( *d->mDatabase );
  query.exec( queryStr );

  QStringList modules;
  while ( query.next() )
    modules.append( query.value( 0 ).toString() );

  return modules;
}

QStringList ExportHelper::submodules( const QString &module ) const
{
  const QString queryStr( "SELECT DISTINCT submodule FROM module_submodule WHERE module=:mod;" );
  QSqlQuery query( *d->mDatabase );
  query.prepare( queryStr );
  query.bindValue( ":mod", module );
  query.exec();

  QStringList submodules;
  while ( query.next() )
    submodules.append( query.value( 0 ).toString() );

  return submodules;
}

QStringList ExportHelper::toolsForLastComponentCheck( const QString &component ) const
{
  const qlonglong lastComponentCheckId = d->lastComponentCheckId( component );
  if ( lastComponentCheckId == -1 ) {
    qDebug() << "No check found for" << component;
    return QStringList();
  }

  const QString queryStr(
      "SELECT DISTINCT sub_query.tool\n"
      "FROM ( SELECT DISTINCT t1.plugin_id,\n"
      "                       t3.name AS tool\n"
      "       FROM checked_file_results AS t1\n"
      "       JOIN plugins as t2\n"
      "       ON t1.plugin_id = t2.id\n"
      "       JOIN tools as t3\n"
      "       ON t2.tool_id = t3.id\n"
      "       WHERE t1.component_check_id=:compId\n"
      "     ) AS sub_query;" );

  QSqlQuery query( *d->mDatabase );
  query.prepare( queryStr );
  query.bindValue( ":compId", lastComponentCheckId );
  query.exec();

  QStringList tools;
  while ( query.next() )
    tools.append( query.value( 0 ).toString() );

  return tools;
}

QStringList ExportHelper::plugins( const QString &tool ) const
{
  const QString queryStr(
      "SELECT DISTINCT t2.name\n"
      "FROM tools AS t1\n"
      "JOIN plugins AS t2\n"
      "ON t1.id = t2.tool_id\n"
      "WHERE t1.name = :toolName;" );

  QSqlQuery query( *d->mDatabase );
  query.prepare( queryStr );
  query.bindValue( ":toolName", tool );
  query.exec();

  QStringList plugins;
  while ( query.next() )
    plugins.append( query.value( 0 ).toString() );

  return plugins;
}

int ExportHelper::sloc( const QString &component, const QString &language ) const
{
  int sloc = 0;

  const QStringList modules = ExportHelper::modules();
  foreach ( const QString &module, modules )
    sloc += slocPerModule( component, module, language );

  return sloc;
}

int ExportHelper::slocPerModule( const QString &component, const QString &module,
                                 const QString &language ) const
{
  int sloc = 0;

  const QStringList submodules = ExportHelper::submodules( module );
  foreach ( const QString &submodule, submodules )
    sloc += slocPerSubmodule( component, module, submodule, language );

  return sloc;
}

int ExportHelper::slocPerSubmodule( const QString &component, const QString &module,
                                    const QString &submodule, const QString &language ) const
{
  const qlonglong lastComponentCheckId = d->lastComponentCheckId( component );
  if ( lastComponentCheckId == -1 ) {
    qDebug() << "No check found for" << component;
    return 0;
  }

  const qlonglong moduleSubmoduleId = d->moduleSubmoduleId( module, submodule );
  if ( moduleSubmoduleId == -1 )
    return 0;

  QSqlQuery query( *d->mDatabase );
  if ( language.isNull() ) {
    const QString queryStr = "SELECT SUM(sloc) FROM module_submodule_sloc WHERE "
                             "component_check_id=:compCheckId "
                             "AND module_submodule_id=:modSubModId;";
    query.prepare( queryStr );
  } else {
    const QString queryStr = "SELECT SUM(sloc) FROM module_submodule_sloc "
                             "WHERE component_check_id=:compCheckId "
                             "AND module_submodule_id=:modSubModId "
                             "AND language=:lang";
    query.prepare( queryStr );
    query.bindValue( ":lang", language );
  }

  query.bindValue( ":compCheckId", lastComponentCheckId );
  query.bindValue( ":modSubModId", moduleSubmoduleId );
  query.exec();

  if ( query.next() )
    return query.value( 0 ).toInt();

  return 0;
}

int ExportHelper::issues( const QString &component,
                          const QString &plugin ) const
{
  const qlonglong lastComponentCheckId = d->lastComponentCheckId( component );
  if ( lastComponentCheckId == -1 ) {
    qDebug() << "No check found for" << component;
    return 0;
  }
  QString queryStr;
  if ( plugin.isNull() ) {
    queryStr = "SELECT SUM(issue_count) FROM checked_file_results "
               "WHERE component_check_id=:compCheckId;";
  } else {
    Q_ASSERT_X( false, "ExportHelper::issues", "Not implemented" );
  }

  QSqlQuery query( *d->mDatabase );
  query.prepare( queryStr );
  query.bindValue( ":compCheckId", lastComponentCheckId );
  query.exec();

  if ( query.next() )
    return query.value( 0 ).toInt();

  return 0;
}

int ExportHelper::issuesPerModule( const QString &component,
                                   const QString &module,
                                   const QString &plugin ) const
{
  const qlonglong lastComponentCheckId = d->lastComponentCheckId( component );
  if ( lastComponentCheckId == -1 ) {
    qDebug() << "No check found for" << component;
    return 0;
  }

  QString queryStr;
  if ( plugin.isNull() ) {
    // Nicely format the query string to simplify debugging.
    queryStr =
      "SELECT sub_query.component,\n"
      "       sub_query.module,\n"
      "       SUM(sub_query.issue_count)\n"
      "FROM ( SELECT t1.id as component,\n"
      "              t4.module,\n"
      "              t2.issue_count\n"
      "       FROM component_check AS t1\n"
      "       JOIN checked_file_results AS t2\n"
      "       ON t1.id = t2.component_check_id\n"
      "       JOIN checked_files AS t3\n"
      "       ON t2.checked_files_id = t3.id\n"
      "       JOIN module_submodule AS t4\n"
      "       ON t3.module_submodule_id = t4.id\n"
      "       JOIN plugins AS t5\n"
      "       ON t2.plugin_id = t5.id\n"
      "     ) AS sub_query\n"
      "WHERE component=:compId\n"
      "AND module=:mod;";
//    qDebug() << queryStr;
  } else {
    // Nicely format the query string to simplify debugging.
    queryStr =
      "SELECT sub_query.component,\n"
      "       sub_query.module,\n"
      "       sub_query.plugin,\n"
      "       SUM(sub_query.issue_count)\n"
      "FROM ( SELECT t1.id as component,\n"
      "              t4.module,\n"
      "              t5.name AS plugin,\n"
      "              t2.issue_count\n"
      "       FROM component_check AS t1\n"
      "       JOIN checked_file_results AS t2\n"
      "       ON t1.id = t2.component_check_id\n"
      "       JOIN checked_files AS t3\n"
      "       ON t2.checked_files_id = t3.id\n"
      "       JOIN module_submodule AS t4\n"
      "       ON t3.module_submodule_id = t4.id\n"
      "       JOIN plugins AS t5\n"
      "       ON t2.plugin_id = t5.id\n"
      "     ) AS sub_query\n"
      "WHERE component=:compId\n"
      "AND module=:mod\n"
      "AND plugin=:plugin";
  }

  QSqlQuery query( *d->mDatabase );
  query.prepare( queryStr );
  query.bindValue( ":compId", lastComponentCheckId );
  query.bindValue( ":mod", module );
  if ( !plugin.isNull() )
    query.bindValue( ":plugin", plugin );

  query.exec();

  if ( query.next() )
    return query.value( plugin.isNull() ? 2 : 3 ).toInt();

  return 0;
}

int ExportHelper::issuesPerSubmodule( const QString &component,
                                      const QString &module,
                                      const QString &submodule,
                                      const QString &plugin ) const
{
  const qlonglong lastComponentCheckId = d->lastComponentCheckId( component );
  if ( lastComponentCheckId == -1 ) {
    qDebug() << "No check found for" << component;
    return 0;
  }

  const qlonglong moduleSubmoduleId = d->moduleSubmoduleId( module, submodule );
  if ( moduleSubmoduleId == -1 )
    return 0;

  QSqlQuery query( *d->mDatabase );
  if ( plugin.isNull() ) {
    const QString queryStr = "SELECT SUM(sloc) FROM module_submodule_sloc WHERE "
                             "component_check_id=:compCheckId "
                             "AND module_submodule_id=:modSubModId;";
    query.prepare( queryStr );
  } else {
    const QString queryStr = "SELECT SUM(sloc) FROM module_submodule_sloc "
                             "WHERE component_check_id=:compCheckId "
                             "AND module_submodule_id=:modSubModId "
                             "AND language=:lang";
    query.prepare( queryStr );
    query.bindValue( ":lang", plugin );
  }

  query.bindValue( ":compCheckId", lastComponentCheckId );
  query.bindValue( ":modSubModId", moduleSubmoduleId );
  query.exec();

  if ( query.next() )
    return query.value( 0 ).toInt();

  return 0;
}
