set(db2manyeyes_SRCS
  exporthelper.cpp
  main.cpp
)

add_executable(db2manyeyes ${db2manyeyes_SRCS})
target_link_libraries(db2manyeyes
  storage
)

