/**
 * Application to store krazy xml output in a (sqlite) database.
 *
 * Copyright 2009-2010 by Bertjan Broeksema <b.broeksema@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "toolrunresultparser.h"

#include <QtCore/QDebug>
#include <QtCore/QDir>

#include <libstorage/database.h>
#include <libstorage/plugin.h>

ToolRunResultParser::ToolRunResultParser(Tool const &tool,
                                         qlonglong componentCheckId,
                                         QString const &xmlFile)
  : XmlParser(xmlFile), mTool(tool), mComponentCheckId(componentCheckId)
{ }

void ToolRunResultParser::setModule(QString const &module)
{
  mModule = module;
}

void ToolRunResultParser::setSubModule(QString const &subModule)
{
  mSubModule = subModule;
}

void ToolRunResultParser::parse(Database *database)
{
  qlonglong moduleSubmoduleId = database->registerModuleSubmodule( mModule, mSubModule );
  int fileTypeCount = mDocument->count("/tool-result/file-types/file-type");
  for (int i = 0; i < fileTypeCount; ++i) {
    QString path("/tool-result/file-types/file-type["+ QString::number(i + 1) + "]/");
    QString fileType = mDocument->readString( path + "string(@value)");

    int checkCount = mDocument->count(path + "check");
    for (int j = 0; j < checkCount; ++j) {
      QString checkPath = path + "check[" + QString::number(j + 1) + "]/";
      QString checkName = mDocument->readString(checkPath + "string(@name)");
      if (checkName.isEmpty()) {
        // Try harder to find a checkname
        QString desc = mDocument->readString(checkPath + "string(@desc)");
        Q_ASSERT(!desc.isEmpty());

        QRegExp exp("\\[(\\w+)\\]");
        Q_ASSERT(exp.indexIn(desc) != -1);

        checkName = exp.cap(1);
      }

      int fileCount = mDocument->count(checkPath + "file");
      for (int k = 0; k < fileCount; ++k) {
        QString filePath = checkPath + "file[" + QString::number(k + 1) + "]/";

        QString fileName = mDocument->readString(filePath + "string(@name)");
        QFileInfo file(fileName);
        if (file.isAbsolute())
          fileName = strip(fileName);

        int issueCount = mDocument->count(filePath + "issues/line");
        if (issueCount > 0 && !fileName.startsWith(mModule)) {
          if (mTool.plugin(checkName).name() != checkName) {
            qDebug() << "Plugin:" << checkName << "was not registered, ignoring.";
          } else {
            qlonglong pluginId = mTool.plugin(checkName).id();
            qlonglong fileId = database->registerFile(moduleSubmoduleId, fileName, fileType);
            database->registerFileResult(fileId, mComponentCheckId, pluginId, issueCount);
          }
        }
      }
    }
  }
}

QString ToolRunResultParser::strip(QString const &filePath)
{
  if (!mModule.isEmpty()) {
    QString result = filePath;
    int index = result.indexOf(mModule);
    if (index >= 0) {
      result = result.right(result.size() - (index + mModule.size() + 1));
    }

    if (!mSubModule.isEmpty()) {
      index = result.indexOf(mSubModule);
      if (index >= 0)
        result = result.right(result.size() - (index + mSubModule.size() + 1));
    }
    return result;
  }

  return filePath;
}
