#ifndef SLOCPARSER_H
#define SLOCPARSER_H

#include <QtCore/QSharedDataPointer>

class QFile;
class QString;
class QStringList;
template <typename T> class QList;

class SLOCForDirData;
class SLOCParserPrivate;

class SLOCForDir
{
public:
  SLOCForDir( const QString &dir );
  SLOCForDir( const SLOCForDir &other );
  ~SLOCForDir();

  SLOCForDir &operator=( const SLOCForDir &other );

  /// Store @param count loc for @param language
  void put( const QString &language, int count );

  /// Returns the directory containing the files that where counted.
  QString dir() const;

  /// Returns a list of lanugages for which loc was reported
  QStringList languages() const;

  /// Returns the total loc in this directory
  int count() const;
  /// Returns the loc for given language in this directory
  int count( const QString &language ) const;

private:
  QSharedDataPointer<SLOCForDirData> d;
};

/**
 * Parses the output of a sloccount run. We assume sloccount is run in a directory
 * and has an output similar like:
 *
 * SLOC    Directory       SLOC-by-Language (Sorted)
 * 8372    kttsd           cpp=6141,xml=2190,sh=41
 * 5805    ksayit          cpp=5704,xml=100,sh=1
 * 5202    kmouth          cpp=5199,sh=3
 * 2069    kmag            cpp=2068,sh=1
 * 970     kmousetool      cpp=969,sh=1
 * 54      IconThemes      ruby=54
 * 0       ColorSchemes    (none)
 * 0       cmake           (none)
 * 0       doc             (none)
 * 0       top_dir         (none)
 *
 * which is the result of running sloccount in kdeaccessability. All lines before
 * and all lines after this part are ignored. The line starting with "SLOC"
 * serves as seperator and the first blank line after that is seen as
 * the end of input.
 */
class SLOCParser
{
public:
  SLOCParser();
  ~SLOCParser();

  bool parse( QFile *slocoutput );
  QString errorMessage() const;

  QList<SLOCForDir> result() const;

private:
  class SLOCParserPrivate * const d_ptr;
  Q_DECLARE_PRIVATE( SLOCParser )
  Q_DISABLE_COPY( SLOCParser )
};

#endif // SLOCPARSER_H
