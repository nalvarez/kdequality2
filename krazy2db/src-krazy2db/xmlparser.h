/**
 * Application to store krazy xml output in a (sqlite) database.
 *
 * Copyright 2009-2010 by Bertjan Broeksema <b.broeksema@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef XMLPARSER_H
#define XMLPARSER_H

#include "xmldocument.h"

class Database;
class QFile;
class QString;

class XmlParser
{
  public:
    XmlParser(QString const &fileName);

    virtual ~XmlParser();

    virtual void parse(Database *db) = 0;

  protected:
    XmlDocument *mDocument;

  private:
    QFile *mXmlFile;
};

#endif // XMLPARSER_H
