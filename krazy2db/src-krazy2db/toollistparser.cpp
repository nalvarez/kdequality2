/**
 * Application to store krazy xml output in a (sqlite) database.
 *
 * Copyright 2009-2010 by Bertjan Broeksema <b.broeksema@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "toollistparser.h"

#include <QtCore/QDebug>

#include <libstorage/database.h>
#include <libstorage/plugin.h>
#include <libstorage/tool.h>

class ToolListParser::ToolListParserPrivate
{
  public:
    Tool mTool;
};

ToolListParser::ToolListParser(QString const &fileName)
  : XmlParser(fileName), d(new ToolListParserPrivate)
{ }

ToolListParser::~ToolListParser()
{
  delete d;
}

void ToolListParser::parse(Database *database)
{
  QString toolName = mDocument->readString("/tool/string(@name)");
  QString toolVersion("Unknown");
  if ( mDocument->has("/tool/string(@version)") )
    toolVersion = mDocument->readString("/tool/string(@version)");
  // TODO: add/parse tool description

  d->mTool.setName(toolName);
  d->mTool.setVersion(toolVersion);
  qlonglong toolId = database->registerTool(d->mTool);
  if (toolId >= 0)
    d->mTool.setId(toolId);
  else
    return; // Registering the tool failed.

  int fileTypeCount = mDocument->count("/tool/file-types/file-type");
  for (int i = 0; i < fileTypeCount; ++i)
  {
    QString path = "/tool/file-types/file-type[" + QString::number(i + 1) + "]/";
    QString fileType = mDocument->readString(path + "string(@name)");

    int pluginCount = mDocument->count(path + "plugin");
    for (int j = 0; j < pluginCount; ++j) {
      QString pluginPath = path + "plugin[" + QString::number(j + 1) + "]/";
      QString pluginName = mDocument->readString(pluginPath + "string(@name)");
      QString pluginVersion = mDocument->readString(pluginPath + "string(@version)");
      QString pluginDesc = mDocument->readString(pluginPath + "string(@short-desc)");

      Plugin plugin(pluginName, pluginVersion);
      plugin.setShortDescription(pluginDesc);
      plugin.setFileType(fileType);
      plugin.setIsExtra(false);
      qlonglong pluginId = database->registerPlugin(d->mTool.id(), plugin);
      Q_ASSERT( pluginId >= 0 );
      plugin.setId(pluginId);

      d->mTool.addPlugin(plugin);
    }

    path += "extra/plugin";
    pluginCount = mDocument->count(path);
    for (int j = 0; j < pluginCount; ++j) {
      QString pluginPath = path + '[' + QString::number(j + 1) + "]/";
      QString pluginName = mDocument->readString(pluginPath + "string(@name)");
      QString pluginVersion = mDocument->readString(pluginPath + "string(@version)");
      QString pluginDesc = mDocument->readString(pluginPath + "string(@short-desc)");

      Plugin plugin(pluginName, pluginVersion);
      plugin.setShortDescription(pluginDesc);
      plugin.setFileType(fileType);
      plugin.setIsExtra(true);
      qlonglong pluginId = database->registerPlugin(d->mTool.id(), plugin);
      Q_ASSERT( pluginId >= 0 );
      plugin.setId(pluginId);

      d->mTool.addPlugin(plugin);
    }
  }
}

Tool ToolListParser::tool() const
{
  return d->mTool;
}
