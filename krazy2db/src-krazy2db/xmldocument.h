/**
 * Application to store krazy xml output in a (sqlite) database.
 *
 * Copyright 2009-2010 by Bertjan Broeksema <b.broeksema@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef XMLDOCUMENT_H
#define XMLDOCUMENT_H

#include <QtCore/QString>

class QFile;

class InvalidXmlFile
{
public:
    InvalidXmlFile( const QString &fileName );
    QString fileName() const;
private:
    QString mFileName;
};

/**
 * Helper class for validating and getting values from xml files.
 */
class XmlDocument
{
public:
    /**
     * Creates an XmlDocument and throws InvalidXmlFile when @param file does
     * not contain valid xml.
     */
    XmlDocument(QFile * const xmlFile);

    ~XmlDocument();

    /**
     * Returns the number of elements for given path.
     */
    int count(QString const &path) const;

    /**
     * Returns whether or not the given path exists.
     */
    bool has(QString const &path) const;

    /**
     * Returns a bool representing the value at given path. The value must be
     * either "true" or "false".
     */
    bool readBool(QString const &path) const;

    /**
     * Returns an int representing the value at given path.
     */
    int readInt(QString const &path) const;

    /**
     * Returns a string representing the value at given path.
     */
    QString readString(QString const &path) const;

    /**
     * The namespace used by default when executing queries.
     */
    void setDefaultNamespace(QString const &xmlNamespace);

    /**
     * Sets the path to the xmllint, which will be used to validate this
     * document.
     */
    void setXmllintExecutable(QString const &xmllintPath);

    /**
     * Sets the path to the schema which must be used to validate this document.
     */
    void setXmlSchema(QString const &schema);

    /**
     * Validates the document using xmllint. If no executable for xmllint is set
     * or if no schema is set this will return false.
     */
    bool isValid();

  private:
    class XmlDocumentPrivate;
    XmlDocumentPrivate *d;
};

#endif // XMLDOCUMENT_H
