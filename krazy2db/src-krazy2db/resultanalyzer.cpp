/**
 * Application to store krazy xml output in a (sqlite) database.
 *
 * Copyright 2009-2010 by Bertjan Broeksema <b.broeksema@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "resultanalyzer.h"

#include <iostream>
#include <QtCore/QDateTime>
#include <QtCore/QDir>

#include <libstorage/database.h>
#include <libstorage/tool.h>

#include "toolrunresultparser.h"

using namespace std;

ResultAnalyzer::ResultAnalyzer() : mDatabase(0)
{ }

qlonglong ResultAnalyzer::anylyze(Tool const &tool)
{
  Q_ASSERT(mDatabase != 0);
  Q_ASSERT(mComponentInfo.contains("component"));
  Q_ASSERT(mComponentInfo.contains("module"));
  Q_ASSERT(mComponentInfo.contains("revision"));
  Q_ASSERT(QFileInfo(mResultDir).exists());

  QString const component = mComponentInfo.value("component");
  QString const module = mComponentInfo.value("module");
  QString const revision = mComponentInfo.value("revision");

  QString const dateTime = QDateTime::currentDateTime().toString("MMMM dd yyyy hh:mm:ss");
  qlonglong componentCheckId = mDatabase->registerComponentCheck(component, revision, dateTime);
  if (componentCheckId == -1) { // Failure
    cerr << "Failed to register/retrieve component check id." << endl;
    return -1;
  }

  QDir resultDir(mResultDir);
  Q_ASSERT(resultDir.exists());

  resultDir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);
  foreach (QString const &submodule, resultDir.entryList()) {
    scanDir(componentCheckId, tool, submodule);
  }

  return componentCheckId;
}

void ResultAnalyzer::scanDir(qlonglong componentCheckId, Tool const &tool, QString const &path)
{
  QDir dir(mResultDir + QDir::separator() + path);
  dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);
  foreach (QString const &dirName, dir.entryList()) {
    scanDir(componentCheckId, tool,  path + QDir::separator() + dirName);
  }

  dir.setFilter(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
  dir.setNameFilters(QStringList() << "*.xml");
  foreach (QString const &xmlFile, dir.entryList()) {
    parse(componentCheckId, tool, path, xmlFile);
  }
}

void ResultAnalyzer::parse(qlonglong componentCheckId,
                           Tool const &tool,
                           QString const &path,
                           QString const &xmlFile)
{
  QString fileName = mResultDir + QDir::separator() + path + QDir::separator() + xmlFile;

  // At this point the path should at least have the form: submodule
  // optionally with extra dirs between the file and submodule.

  QString const module = mComponentInfo.value("module");
  QString const submodule = path.split(QDir::separator()).first();

  try {
    ToolRunResultParser parser(tool, componentCheckId, fileName);
    parser.setModule(module);
    parser.setSubModule(submodule);

    cout << "Parsing: " << qPrintable(fileName) << endl;

    parser.parse(mDatabase);
  } catch ( const InvalidXmlFile &exception ) {
    cerr << "Skipping invalid tool result file:" << qPrintable(fileName) << endl;
  }
}

void ResultAnalyzer::setComponentInfo(QMap<QString,QString> const &componentInfo)
{
  mComponentInfo = componentInfo;
}

void ResultAnalyzer::setDatabase(Database * const db)
{
  mDatabase = db;
}

void ResultAnalyzer::setResultDir(QString const &resultDir)
{
  mResultDir = resultDir;
}
