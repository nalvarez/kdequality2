/**
 * Application to store krazy xml output in a (sqlite) database.
 *
 * Copyright 2009-2010 by Bertjan Broeksema <b.broeksema@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include <iostream>

#include <QtCore/QCoreApplication>
#include <QtCore/QDir>
#include <QtCore/QFileInfo>
#include <QtCore/QMap>
#include <QtCore/QRegExp>

#include <libstorage/dbconfig.h>
#include <libstorage/database.h>
#include <libstorage/tool.h>

#include "resultanalyzer.h"
#include "slocparser.h"
#include "toollistparser.h"

using namespace std;

static const char *sToolListFileName = "tool-list.xml";
static const char *sComponentInfo = "component.info";
static const char *sSLOCOutputFileName = "sloccount.output";

QMap<QString, QString> readComponentInfo(QString const &componentInfoFileName)
{
  cout << "Parsing: " << sComponentInfo << "..." << endl;
  QFile file(componentInfoFileName);
  file.open(QIODevice::ReadOnly);
  if (!file.isOpen()) {
    cerr << "Could not open " << sComponentInfo << endl;
    return QMap<QString, QString>();
  }

  QString line;
  QRegExp reg("\\w=.*");
  QMap<QString, QString> result;
  while (!file.atEnd()) {
    line = file.readLine();
    if (reg.indexIn(line) != -1) {
      QStringList list = line.split('=');
      if (list.size() == 2) {
        result.insert(list.first(), list.last().trimmed());
      }
    }
  }

  file.close();
  return result;
}

Tool processList(Database *database, QString const &toolListFileName)
{
  try {
    ToolListParser parser(toolListFileName);
    cout << "Parsing: " << sToolListFileName << "..." << endl;
    parser.parse(database);
    return parser.tool();
  } catch ( const InvalidXmlFile &exception ) {
    cerr << "Skipping tool results due to invalid tool file: " << qPrintable( exception.fileName() );
    return Tool();
  }
}

int main(int argc, char* argv[])
{
  QCoreApplication app(argc,argv);
  if (argc < 2 || argc > 3) {
    cerr << "Usage: " << argv[0] << " {RESULT_DIR} {OPTIONAL_DB_SETTINGS_FILE}" << endl;
    return 1;
  }

  QFileInfo resultDirInfo(argv[1]);
  if (!resultDirInfo.exists()) {
    cerr << qPrintable(resultDirInfo.absolutePath()) << " does not exists." << endl;
    return 2;
  } else if (!resultDirInfo.isDir()) {
    cerr << qPrintable(resultDirInfo.absolutePath()) << " is not a directory." << endl;
    return 3;
  }

  QFileInfo info(QString(argv[1]) + QDir::separator() + sToolListFileName);
  if (!info.exists()) {
    cerr << "Could not find required file " << sToolListFileName << endl;
    return 4;
  }

  info = QFileInfo(QString(argv[1]) + QDir::separator() + sComponentInfo);
  if (!info.exists()) {
    cerr << "Could not find required file " << sComponentInfo << endl;
    return 4;
  }

  QMap<QString, QString> componentInfo = readComponentInfo(QString(argv[1]) + QDir::separator() + sComponentInfo);
  if (!componentInfo.contains("component")) {
    cerr << sComponentInfo << " invalid: missing 'component'" << endl;
    return 5;
  } else if (!componentInfo.contains("module")) {
    cerr << sComponentInfo << " invalid: missing 'module'" << endl;
    return 5;
  } else if (!componentInfo.contains("revision")) {
    cerr << sComponentInfo << " invalid: missing 'revision'" << endl;
    return 5;
  }

  QScopedPointer<DbConfig> dbConfig;
  if( argc == 3) {
    QFileInfo settingsFile( argv[2] );
    if ( !settingsFile.exists() ) {
      cerr << "Settings file " << argv[2] << " does not exists." << endl;
      return 5;
    }
    if ( !settingsFile.isReadable() ) {
      cerr << "Can not open settings file " << argv[2] << " for reading." << endl;
      return 5;
    }

    QSettings dbSettings( settingsFile.absoluteFilePath(), QSettings::IniFormat );
    if ( dbSettings.status() != QSettings::NoError ) {
      cerr << "Could not parse settings file " << argv[2] << "." << endl;
      return 6;
    }
    dbConfig.reset( DbConfig::configuredDatabase( dbSettings ) );
  } else {
    // Create a default configuration, by passing an invalid QSetting
    QSettings useDefaultSettings;
    dbConfig.reset( DbConfig::configuredDatabase( useDefaultSettings ) );
  }

  Database db;
  if ( !db.init( *dbConfig ) ) {
    cerr << "Failed to initialize database." << endl;
    return 7;
  }

  Tool t = processList( &db, QString( argv[1] ) + QDir::separator() + sToolListFileName );
  if ( t.name() == "unknown" )
    return 8;

  ResultAnalyzer ra;
  ra.setComponentInfo( componentInfo );
  ra.setDatabase( &db );
  ra.setResultDir( argv[1] );
  qlonglong compCheckId = ra.anylyze( t );

  const QString slocFileName( QString( argv[1] ) + QDir::separator() + sSLOCOutputFileName );
  QFile slocOutputFile( slocFileName );
  if ( !slocOutputFile.exists() ) {
    cerr << "sloccount output file " << qPrintable( slocFileName ) << " does not exists." << endl;
    return 9;
  }

  if ( !slocOutputFile.open( QIODevice::ReadOnly ) ) {
    cerr << "sloccount output file " << qPrintable( slocFileName ) << " could not be opened for reading." << endl;
    return 10;
  }

  cout << "Parsing: " << qPrintable( slocFileName ) << "..." << endl;
  SLOCParser parser;
  if ( !parser.parse( &slocOutputFile ) ) {
    cerr << "sloccount output file " << qPrintable( slocFileName ) << " could not be parsed." << endl;
    return 11;
  }

  const QList<SLOCForDir> result = parser.result();
  const QString module = componentInfo["module"];

  foreach ( const SLOCForDir &sloc, result ) {
    const QString submodule = sloc.dir();
    qlonglong moduleSubmoduleId = db.registerModuleSubmodule( module, submodule );
    foreach ( const QString &language, sloc.languages() )
      db.registerSubmoduleSLOC( compCheckId, moduleSubmoduleId, language, sloc.count( language ) );
  }

  return 0;
}
