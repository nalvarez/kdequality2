/**
 * Application to store krazy xml output in a (sqlite) database.
 *
 * Copyright 2009-2010 by Bertjan Broeksema <b.broeksema@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "database.h"

#include <QtCore/QDebug>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtSql/QSqlError>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>

#include "plugin.h"
#include "dbconfig.h"
#include "tool.h"

static QString sToolsTable = "tools";
static QString sPluginsTable = "plugins";
static QString sComponentCheckTable = "component_check";
static QString sCheckedFilesTable = "checked_files";
static QString sCheckedFileResultsTable = "checked_file_results";

struct DatabasePrivate
{
  bool mIsPostgres;
  QSqlDatabase mDatabase;

  /// Functions
  qint64 insertId( QSqlQuery &query ) const;
};

qint64 DatabasePrivate::insertId( QSqlQuery &query ) const
{
  if ( mIsPostgres ) {
    query.next();
    return query.record().value( QLatin1String("id") ).toLongLong();
  } else {
    const QVariant v = query.lastInsertId();
    if ( !v.isValid() ) return -1;
    bool ok;
    const qint64 insertId = v.toLongLong( &ok );
    if ( !ok ) return -1;
    return insertId;
  }
  return -1;
}



Database::Database() : d_ptr( new DatabasePrivate )
{
  Q_D( Database );
  d->mIsPostgres = false;
}

Database::~Database()
{
  delete d_ptr;
}

bool Database::init( const DbConfig &dbConfig )
{
  Q_D( Database );

  dbConfig.apply( d->mDatabase );
  if ( !d->mDatabase.isOpen() )
    return false;

  d->mIsPostgres = d->mDatabase.driverName() == "QPSQL";

  QStringList tables = d->mDatabase.tables();
  if ( tables.isEmpty() )
    return dbConfig.createTables( d->mDatabase );

  bool result = true;
  if ( !tables.contains( sToolsTable ) )
    result &= dbConfig.createTable( d->mDatabase, DbConfig::Tools );
  if ( !tables.contains( sPluginsTable ) )
    result &= dbConfig.createTable( d->mDatabase, DbConfig::Plugins );
  if ( !tables.contains( sComponentCheckTable ) )
    result &= dbConfig.createTable( d->mDatabase, DbConfig::ComponentCheck );
  if ( !tables.contains( sCheckedFilesTable ) )
    result &= dbConfig.createTable( d->mDatabase, DbConfig::CheckedFiles );
  if ( !tables.contains( sCheckedFileResultsTable ) )
    result &= dbConfig.createTable( d->mDatabase, DbConfig::CheckedFileResults );
  return result;
}

qlonglong Database::registerComponentCheck(QString const &component,
                                           QString const &revision,
                                           QString const &date)
{
  Q_D( Database );

  QSqlQuery query( d->mDatabase );
  query.prepare( "SELECT id FROM component_check WHERE component=:comp "
                 "AND revision=:rev" );
  query.bindValue( ":comp", component );
  query.bindValue( ":rev", revision );

  if ( !query.exec() ) {
    qDebug() << "Database::registerComponentCheck: SELECT FAILED";
    qDebug() << "  " << query.lastError().text();
    return -1;
  }

  if (query.next()) {
    return query.value(0).toLongLong();
  } else {
    if ( d->mIsPostgres )
      query.prepare( "INSERT INTO component_check(component, revision, date) "
                     "VALUES ( :comp, :rev, :date )"
                     "RETURNING id" );
    else
      query.prepare( "INSERT INTO component_check(component, revision, date) "
                     "VALUES ( :comp, :rev, :date )" );
    query.bindValue( ":comp", component );
    query.bindValue( ":rev", revision );
    query.bindValue( ":date", date );

    if ( query.exec() ) {
      return d->insertId( query );
    } else {
      qDebug() << "Database::registerComponentCheck: INSERT FAILED.";
      qDebug() << "  " << query.lastError().text();
    }
  }

  return -1;
}

qlonglong Database::registerModuleSubmodule(QString const &module,
                                            QString const &submodule)
{
  Q_D( Database );
  QSqlQuery query( d->mDatabase );
  query.prepare( "SELECT id FROM module_submodule WHERE module=:mod "
                 "AND submodule=:sub" );
  query.bindValue( ":mod", module );
  query.bindValue( ":sub", submodule );

  if ( !query.exec() ) {
    qDebug() << "Database::registerModuleSubmodule: SELECT FAILED";
    qDebug() << "  " << query.lastError().text();
    return -1;
  }

  if (query.next()) {
    return query.value(0).toLongLong();
  } else {
    if ( d->mIsPostgres )
      query.prepare( "INSERT INTO module_submodule(module, submodule) "
                     "VALUES ( :mod, :sub ) RETURNING id" );
    else
      query.prepare( "INSERT INTO module_submodule(module, submodule) "
                     "VALUES ( :mod, :sub )" );
    query.bindValue( ":mod", module );
    query.bindValue( ":sub", submodule );

    if ( query.exec() ) {
      return d->insertId( query );
    } else {
      qDebug() << "Database::registerComponentCheck: INSERT FAILED.";
      qDebug() << "  " << query.lastError().text();
    }
  }

  return -1;
}

void Database::registerSubmoduleSLOC(qlonglong componentCheckId,
                                     qlonglong moduleSubmoduleId,
                                     QString const &language,
                                     int count)
{
  Q_D( Database );
  QSqlQuery query( d->mDatabase );

  query.prepare( "INSERT INTO module_submodule_sloc (component_check_id, module_submodule_id, language, sloc) "
                 "VALUES (:compCheckId, :modSubmodId, :lang, :sloc);" );
  query.bindValue( ":compCheckId", componentCheckId );
  query.bindValue( ":modSubmodId", moduleSubmoduleId );
  query.bindValue( ":lang", language );
  query.bindValue( ":sloc", count );

  if ( !query.exec() ) {
    qDebug() << "Database::registerSubmoduleSLOC: INSERT FAILED";
    qDebug() << "  " << query.lastError().text();
  }
}

qlonglong Database::registerFile(qlonglong moduleSubmoduleId,
                                 QString const &fileName,
                                 QString const &fileType)
{
  Q_D( Database );
  QSqlQuery query( d->mDatabase );
  query.prepare( "SELECT id FROM checked_files WHERE module_submodule_id=:modSubModId "
                 "AND submodule=:submod AND file=:file" );
  query.bindValue( ":modSubModId", moduleSubmoduleId );
  query.bindValue( ":file", fileName );

  if ( !query.exec() ) {
    qDebug() << "Database::registerFile: SELECT FAILED";
    qDebug() << "  " << query.lastError().text();
    return -1;
  }

  if ( query.next() ) {
    return query.value(0).toLongLong();
  } else {
    if ( d->mIsPostgres )
      query.prepare( "INSERT INTO checked_files (module_submodule_id, file, type) "
                     "VALUES (:modSubmodId, :file, :filetype) RETURNING id" );
    else
      query.prepare( "INSERT INTO checked_files(module_submodule_id, file, type) "
                     "VALUES( :modSubmodId, :file, :filetype )" );

    query.bindValue( ":modSubmodId", moduleSubmoduleId );
    query.bindValue( ":file", fileName );
    query.bindValue( ":filetype", fileType );

    if ( query.exec() ) {
      return d->insertId( query );
    } else {
      qDebug() << "Database::registerFile: INSERT FAILED.";
      qDebug() << query.lastQuery();
      qDebug() << "  " << query.lastError().text();
    }
  }

  return -1;
}

void Database::registerFileResult(qlonglong fileId,
                                  qlonglong componentCheckId,
                                  qlonglong pluginId,
                                  int issueCount)
{
  Q_D( Database );

  QSqlQuery query( d->mDatabase );
  query.prepare( "SELECT * FROM checked_file_results "
                 "WHERE checked_files_id=:fileid "
                 "AND component_check_id=:compcheckid "
                 "AND plugin_id=:pluginid" );
  query.bindValue( ":fileid", fileId );
  query.bindValue( ":compcheckid", componentCheckId );
  query.bindValue( ":pluginid", pluginId );

  if ( !query.exec() ) {
    qDebug() << "Database::registerFileResult: SELECT FAILED";
    qDebug() << "  " << query.lastError().text();
    return;
  }

  if ( !query.next() ) { // Only report the resutl when its not yet there.
    query.prepare( "INSERT INTO checked_file_results"
                   "  (checked_files_id, component_check_id, plugin_id, issue_count) "
                   "VALUES (:fileId, :compcheckid, :pluginid, :count)" );
    query.bindValue( ":fileid", fileId );
    query.bindValue( ":compcheckid", componentCheckId );
    query.bindValue( ":pluginid", pluginId );
    query.bindValue( ":count", issueCount );

    if ( !query.exec() ) {
      qDebug() << "Database::registerFileResult: INSERT FAILED.";
      qDebug() << "  " << query.lastQuery();
      qDebug() << "  " << query.lastError().text();
    }
  }
}

qlonglong Database::registerPlugin(qlonglong toolId, Plugin const &plugin)
{
  Q_D( Database );

  QSqlQuery query( d->mDatabase );
  query.prepare( "SELECT id FROM plugins "
                 "WHERE tool_id=:toolid "
                 "AND name=:pluginname "
                 "AND version=:pluginversion" );
  query.bindValue( ":toolId", toolId );
  query.bindValue( ":pluginname", plugin.name() );
  query.bindValue( ":pluginversion", plugin.version() );

  if ( !query.exec() ) {
    qDebug() << "Database::registerPlugin: SELECT FAILED";
    qDebug() << "  " << query.lastError().text();
  }

  if ( query.next() )
    return query.value(0).toLongLong();
  else {
    if ( d->mIsPostgres )
      query.prepare( "INSERT INTO plugins(tool_id, name, version, description, filetype, is_extra) "
                     "VALUES(:toolid, :pluginname, :pluginversion, :desc, :type, :extra)"
                     "RETURNING id" );
    else
      query.prepare( "INSERT INTO plugins(tool_id, name, version, description, filetype, is_extra) "
                     "VALUES(:toolid, :pluginname, :pluginversion, :desc, :type, :extra)" );

    query.bindValue( ":toolId", toolId );
    query.bindValue( ":pluginname", plugin.name() );
    query.bindValue( ":pluginversion", plugin.version() );
    query.bindValue( ":desc", plugin.shortDescription() );
    query.bindValue( ":type", plugin.fileType() );
    query.bindValue( ":extra", plugin.isExtra() );

    if ( query.exec() ) {
      return d->insertId( query );
    } else {
      qDebug() << "Database::registerPlugin: INSERT FAILED.";
      qDebug() << "  " << query.lastError().text();
    }
  }

  return -1;
}

qlonglong Database::registerTool(Tool const &tool)
{
  Q_D( Database );
  Q_ASSERT( !tool.name().isNull() );
  Q_ASSERT( !tool.version().isNull() );

  QSqlQuery query( d->mDatabase );
  query.prepare( "SELECT id FROM tools "
                 "WHERE name=:toolname AND version=:toolversion" );
  query.bindValue( ":toolname", tool.name() );
  query.bindValue( ":toolversion", tool.version() );

  if ( !query.exec() ) {
    qDebug() << "Database::registerTool: SELECT FAILED";
    qDebug() << "  " << query.lastError().text();
    return -1;
  }

  if ( query.next() )
    return query.value(0).toLongLong();
  else {
    if ( d->mIsPostgres )
      query.prepare( "INSERT INTO tools(name, version, description) "
                     "VALUES( :toolname, :toolversion, :tooldesc ) RETURNING id" );
    else
      query.prepare( "INSERT INTO tools(name, version, description) "
                     "VALUES( :toolname, :toolversion, :tooldesc )" );
    query.bindValue( ":toolname", tool.name() );
    query.bindValue( ":toolversion", tool.version() );
    query.bindValue( ":tooldesc", tool.description() );

    if ( query.exec() ) {
      return d->insertId( query );
    } else {
      qDebug() << "Database::registerTool: INSERT FAILED.";
      qDebug() << "  " << query.lastError().text();
    }
  }

  return -1;
}
