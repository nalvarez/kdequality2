/*
    Copyright (c) 2010 Tobias Koenig <tokoe@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "dbconfigsqlite.h"

#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtSql/QSqlDriver>
#include <QtSql/QSqlQuery>

static QString sqliteDataFile()
{
  const QString dbPath = QDir::currentPath() + QDir::separator() + QLatin1String( "krazy.db" );
  if ( !QFile::exists( dbPath ) ) {
    QFile file( dbPath );
    if ( !file.open( QIODevice::WriteOnly ) )
      qFatal( "Unable to create file %s during database initialization.", dbPath.toLocal8Bit().data() );
    file.close();
  }

  return dbPath;
}


DbConfigSqlite::DbConfigSqlite( QSettings &settings )
{
  // determine default settings depending on the driver
  QString defaultDbName = sqliteDataFile();

  // read settings for current driver
  settings.beginGroup( QLatin1String( "QSQLITE" ) );
  mDatabaseName = settings.value( QLatin1String( "Name" ), defaultDbName ).toString();
  mHostName = settings.value( QLatin1String( "Host" ) ).toString();
  mUserName = settings.value( QLatin1String( "User" ) ).toString();
  mPassword = settings.value( QLatin1String( "Password" ) ).toString();
  mConnectionOptions = settings.value( QLatin1String( "Options" ) ).toString();
  settings.endGroup();
}

void DbConfigSqlite::apply( QSqlDatabase &database ) const
{
  database = QSqlDatabase::addDatabase( QLatin1String( "QSQLITE" ) );
  if ( !mDatabaseName.isEmpty() )
    database.setDatabaseName( mDatabaseName );
  if ( !mHostName.isEmpty() )
    database.setHostName( mHostName );
  if ( !mUserName.isEmpty() )
    database.setUserName( mUserName );
  if ( !mPassword.isEmpty() )
    database.setPassword( mPassword );

  database.setConnectOptions( mConnectionOptions );
  database.open();
  Q_ASSERT( database.driver()->hasFeature( QSqlDriver::LastInsertId ) );
}

bool DbConfigSqlite::createTable( QSqlDatabase &database, Table table ) const
{
  QString tableCreateQuery;
  switch( table ) {
  case Tools:
    tableCreateQuery = "CREATE TABLE tools("
                         "id INTEGER PRIMARY KEY ASC AUTOINCREMENT, "
                         "name TEXT, version TEXT, description TEXT );";
    break;
  case Plugins:
    tableCreateQuery = "CREATE TABLE plugins("
                          "id INTEGER PRIMARY KEY ASC AUTOINCREMENT, "
                          "tool_id INTEGER, name TEXT, version TEXT, "
                          "description TEXT, filetype TEXT, is_extra BOOLEAN );";
    break;
  case ComponentCheck:
    tableCreateQuery = "CREATE TABLE module_submodule("
                         "id INTEGER PRIMARY KEY ASC AUTOINCREMENT,"
                         "module TEXT, submodule TEXT );";
    break;
  case ModuleSubmodule:
    tableCreateQuery = "CREATE TABLE component_check("
                         "id INTEGER PRIMARY KEY ASC AUTOINCREMENT,"
                         "component TEXT, revision TEXT, date TEXT );";
    break;
  case CheckedFiles:
    tableCreateQuery = "CREATE TABLE checked_files("
                         "id INTEGER PRIMARY KEY ASC AUTOINCREMENT,"
                         "module_submodule_id INTEGER, submodule TEXT, file TEXT, type TEXT );";
    break;
  case CheckedFileResults:
    tableCreateQuery = "CREATE TABLE checked_file_results("
                         "checked_files_id INTEGER, component_check_id INTEGER, "
                         "plugin_id INTEGER, issue_count INTEGER );";
    break;
  case ModuleSubmoduleSloc:
    tableCreateQuery = "CREATE TABLE module_submodule_sloc("
                         "component_check_id INTEGER, module_submodule_id INTEGER,"
                         "language TEXT, sloc INTEGER);";
    break;
  default:
    Q_ASSERT( false );
  }

  QSqlQuery query( database );
  return query.exec( tableCreateQuery );
}
