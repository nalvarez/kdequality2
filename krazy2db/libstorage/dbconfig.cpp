/*
    Copyright (c) 2010 Tobias Koenig <tokoe@kde.org>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "dbconfig.h"

#include <iostream>

// #include "dbconfigmysql.h"
// #include "dbconfigmysqlembedded.h"
#include "dbconfigpostgres.h"
#include "dbconfigsqlite.h"

using namespace std;

DbConfig::DbConfig()
{ }

DbConfig::~DbConfig()
{ }

DbConfig* DbConfig::configuredDatabase( QSettings &settings )
{
  DbConfig *config;

  // determine driver to use
  const QString defaultDriver = QLatin1String( "QSQLITE" );
  QString driverName = settings.value( QLatin1String( "Driver" ), defaultDriver ).toString();

  if ( driverName.isEmpty() )
    driverName = defaultDriver;

  if ( driverName == QLatin1String( "QSQLITE" ) )
    config = new DbConfigSqlite( settings );
  else if ( driverName == QLatin1String( "QPSQL" ) )
    config = new DbConfigPostgres( settings );
  else
    Q_ASSERT( false );

//   s_DbConfigInstance->

  return config;
}

bool DbConfig::createTables( QSqlDatabase &database ) const
{
  cout << "Creating tables" << endl;

  cout << " * Creating table tools..." << endl;
  if ( !createTable( database, Tools ) ) {
    cout << " * Creating table tools failed!" << endl;
    return false;
  }

  cout << " * Creating table plugins..." << endl;
  if ( !createTable( database, Plugins ) ) {
    cout << " * Creating table plugins failed!" << endl;
    return false;
  }

  cout << " * Creating table component_check..." << endl;
  if ( !createTable( database, ComponentCheck ) ) {
    cout << " * Creating table component_check failed!" << endl;
    return false;
  }

  cout << " * Creating table module_submodule..." << endl;
  if ( !createTable( database, ModuleSubmodule ) ) {
    cout << " * Creating table module_submodule failed!" << endl;
    return false;
  }

  cout << " * Creating table checked_files..." << endl;
  if ( !createTable( database, CheckedFiles ) ) {
    cout << " * Creating table checked_files failed!" << endl;
    return false;
  }

  cout << " * Creating table checked_file_results..." << endl;
  if ( !createTable( database, CheckedFileResults ) ) {
    cout << " * Creating table checked_file_results failed!" << endl;
    return false;
  }

  cout << " * Creating table module_submodule_sloc..." << endl;
  if ( !createTable( database, ModuleSubmoduleSloc ) ) {
    cout << " * Creating table module_submodule_sloc failed!" << endl;
    return false;
  }

  return true;
}
