/**
 * Application to store krazy xml output in a (sqlite) database.
 *
 * Copyright 2009-2010 by Bertjan Broeksema <b.broeksema@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "plugin.h"

class Plugin::PluginPrivate
{
  public:
    QString   mDescription;
    QString   mFileType;
    qlonglong mId;
    bool      mIsExtra;
    QString   mName;
    QString   mVersion;

    PluginPrivate(QString const &name, QString const &version)
        : mName(name), mVersion(version), mId(-1), mIsExtra(false)
    {}
};

Plugin::Plugin() : d(new PluginPrivate("",""))
{ }

Plugin::Plugin(QString const &name, QString const &version)
    : d(new PluginPrivate(name, version))
{ }

Plugin::Plugin(Plugin const &other)
    : d(new PluginPrivate(*other.d))
{ }

Plugin::~Plugin()
{
  delete d;
}

QString Plugin::name() const
{
  return d->mName;
}

QString Plugin::version() const
{
  return d->mVersion;
}

QString Plugin::fileType() const
{
  return d->mFileType;
}

void Plugin::setFileType(QString const &fileType)
{
  d->mFileType = fileType;
}

qlonglong Plugin::id() const
{
  return d->mId;
}

void Plugin::setId(qlonglong id)
{
  d->mId = id;
}

bool Plugin::isExtra() const
{
  return d->mIsExtra;
}

void Plugin::setIsExtra(bool isExtra)
{
  d->mIsExtra = isExtra;
}

QString Plugin::shortDescription() const
{
  return d->mDescription;
}

void Plugin::setShortDescription(QString const &description)
{
  d->mDescription = description;
}
