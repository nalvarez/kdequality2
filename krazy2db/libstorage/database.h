#ifndef DATABASE_H
#define DATABASE_H

#include <QtCore/QtGlobal>

class DbConfig;
class Plugin;
class QString;
class Tool;

class DatabasePrivate;

class Database
{
  public:
    Database();
    ~Database();

    bool init( const DbConfig &dbConfig );

    /**
     * Registers the component check to the database and returns the id of the
     * component check. If the component check is already registered (i.e.
     * component, revision combination) this only returns the id. When
     * the component check could not be registered -1 is returned.
     */
    qlonglong registerComponentCheck(QString const &component,
                                     QString const &revision,
                                     QString const &dateTime);

    qlonglong registerModuleSubmodule(QString const &module,
                                      QString const &submodule);

    /**
     * Registers the file to the database and returns the id of the file. If the
     * file is already registered (i.e. module, submodule, filename combination)
     * this only returns the id. When the file could not be registered -1 is
     * returned.
     */
    qlonglong registerFile(qlonglong moduleSubmoduleId,
                           QString const &fileName,
                           QString const &fileType);

    /**
     * Registers the number of issues found for a file with a specific plugin.
     * If there is already a result registerd for given fileId, componentCheckId
     *, pluginId combination, this method does nothing.
     */
    void registerFileResult(qlonglong fileId,
                            qlonglong componentCheckId,
                            qlonglong pluginId,
                            int issueCount);

    /**
     * Register a "source lines of code" (SLOC) for a module/submodule.
     */
    void registerSubmoduleSLOC(qlonglong componentCheckId,
                               qlonglong moduleSubmoduleId,
                               QString const &language,
                               int count);

    /**
     * Registers the plugin to the database and returns the id of the plugin. If
     * the plugin is already registered (i.e. tool, name, version combination)
     * this only returns the id.of the plugin. When the plugin could not be
     * registered and did not exist already, -1 is returned.
     */
    qlonglong registerPlugin(qlonglong toolId, Plugin const &plugin);

    /**
     * Registers the tool to the database and returns the id of the tool. If
     * the tool is already registered (i.e. name, version combination) this only
     * returns the id.of the plugin. When the tool could not be registered and
     * did not exist already, -1 is returned.
     */
    qlonglong registerTool(Tool const &tool);

  private:
    DatabasePrivate * const d_ptr;
    Q_DECLARE_PRIVATE( Database );
    Q_DISABLE_COPY( Database );
};

#endif // DATABASE_H
