/**
 * Application to store krazy xml output in a (sqlite) database.
 *
 * Copyright 2009-2010 by Bertjan Broeksema <b.broeksema@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#include "tool.h"

#include <QtCore/QMap>

#include "plugin.h"

class Tool::ToolPrivate
{
  public:
    QString                    mDescription;
    qlonglong                  mId;
    QString                    mName;
    QString                    mVersion;
    QMultiMap<QString, Plugin> mPlugins;

    ToolPrivate(QString const &name)
      : mName(name)
    {}

};

Tool::Tool(QString const &name)
    : d(new ToolPrivate(name))
{
  Q_ASSERT( !d->mName.isNull() );
  d->mVersion = QString( "Unknown" ); // null value not allowed for version
}

Tool::Tool(Tool const &other)
    : d(new ToolPrivate(other.name()))
{
  d->mDescription = other.d->mDescription;
  d->mId = other.d->mId;
  d->mVersion = other.d->mVersion;
  d->mPlugins = other.d->mPlugins;
}

Tool::~Tool()
{
  delete d;
}

void Tool::addPlugin(Plugin const &plugin)
{
  d->mPlugins.insert(plugin.fileType(), plugin);
}

QString Tool::description() const
{
  return d->mDescription;
}

qlonglong Tool::id() const
{
  return d->mId;
}

QString Tool::name() const
{
  return d->mName;
}

Plugin Tool::plugin(QString const &name) const
{
  foreach (Plugin const&plugin, d->mPlugins.values()) {
    if (plugin.name() == name) {
      return plugin;
    }
  }

  return Plugin();
}

QString Tool::version() const
{
  return d->mVersion;
}

//PluginList Tool::plugins(QString const &fileType = QString()) const;

void Tool::setDescription(QString const &description)
{
  d->mDescription = description;
}

void Tool::setId(qlonglong id)
{
  d->mId = id;
}

void Tool::setName(QString const &name)
{
  d->mName = name;
}

void Tool::setVersion(QString const &version)
{
  Q_ASSERT(!version.isNull());
  d->mVersion = version;
}

