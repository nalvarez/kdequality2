/**
 * Application to store krazy xml output in a (sqlite) database.
 *
 * Copyright 2009-2010 by Bertjan Broeksema <b.broeksema@kdemail.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
#ifndef PLUGIN_H
#define PLUGIN_H

#include <QtCore/QString>

class Plugin
{
  public:
    Plugin();
    Plugin(QString const &name, QString const &version);
    Plugin(Plugin const &other);

    ~Plugin();

    Plugin &operator=(Plugin const &other)
    {
      Plugin copy( other );
      swap( copy );
      return *this;
    }

    void swap(Plugin &other) { qSwap( d, other.d ); }

    QString name() const;
    QString version() const;

    QString fileType() const;
    void setFileType(QString const &fileType);

    qlonglong id() const;
    void setId(qlonglong id);

    bool isExtra() const;
    void setIsExtra(bool isExtra);

    QString shortDescription() const;
    void setShortDescription(QString const &description);

  private:
    class PluginPrivate;
    PluginPrivate *d;
};

#endif // PLUGIN_H
