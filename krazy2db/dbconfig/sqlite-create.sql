CREATE TABLE tools(id INTEGER PRIMARY KEY ASC AUTOINCREMENT,
                   name TEXT,
                   version TEXT,
                   description TEXT
);

CREATE TABLE plugins(id INTEGER PRIMARY KEY ASC AUTOINCREMENT,
                     tool_id INTEGER,
                     name TEXT,
                     version TEXT,
                     description TEXT,
                     filetype TEXT,
                     is_extra BOOLEAN
);

CREATE TABLE component_check(id INTEGER PRIMARY KEY ASC AUTOINCREMENT,
                             component TEXT,
                             revision TEXT,
                             date TEXT
);

CREATE TABLE module_submodule(id INTEGER PRIMARY KEY ASC AUTOINCREMENT,
                              module TEXT,
                              submodule TEXT
);

CREATE TABLE checked_files(id INTEGER PRIMARY KEY ASC AUTOINCREMENT,
                           module_submodule_id INTEGER,
                           file TEXT,
                           type TEXT
);

-- Results per file/plugin
CREATE TABLE checked_file_results(checked_files_id INTEGER,
                                  component_check_id INTEGER,
                                  plugin_id INTEGER,
                                  issue_count INTEGER
);

-- SLOC per submodule
CREATE TABLE module_submodule_sloc(component_check_id INTEGER,
                                   module_submodule_id INTEGER,
                                   language TEXT,
                                   sloc INTEGER
);

