
QT = # nothing
CONFIG = release
TARGET = rpp0
DESTDIR = ../bin
include(rpp.pri)

mac:CONFIG -= app_bundle

DEFINES += \
    PP_NO_SMART_HEADER_PROTECTION \
    PP_WITH_MACRO_POSITION

unix:DEFINES += HAVE_MMAP

SOURCES += pp-main.cpp

CONF_SOURCES = builtin-macros.cpp

*-g++ {
    DUMPEDVERSION = $$system(g++ -dumpversion)
    DEFINES += GCC_VERSION="\\\"$${DUMPEDVERSION}\\\""

    DUMPEDMACHINE = $$system(g++ -dumpmachine)
    DEFINES += GCC_MACHINE="\\\"$${DUMPEDMACHINE}\\\""

    conf_compiler.commands = $$QMAKE_CXX -E -xc++ -dM ${QMAKE_FILE_IN} -o ${QMAKE_FILE_OUT}
    conf_compiler.output = ../data/${QMAKE_FILE_BASE}.conf
    conf_compiler.input = CONF_SOURCES
    conf_compiler.variable_out = GENERATED_CONF_FILES
    conf_compiler.name = compiling[conf] ${QMAKE_FILE_IN}
    silent:conf_compiler.commands = @echo compiling[conf] ${QMAKE_FILE_IN} && $$conf_compiler.commands
    QMAKE_EXTRA_COMPILERS += conf_compiler

    makeconf.depends = compiler_conf_compiler_make_all
    QMAKE_EXTRA_TARGETS += makeconf
    ALL_DEPS += makeconf
}
