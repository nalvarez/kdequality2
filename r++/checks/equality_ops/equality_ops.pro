TEMPLATE = lib
INCLUDEPATH += $$PWD $$PWD/../../lib $$PWD/../../shared
DESTDIR = $$PWD/../../bin
TARGET = r++check_equality_ops
HEADERS += rpp-check-operators.h
SOURCES += rpp-check-operators.cpp
CONFIG += plugin
LIBS += -L$$PWD/../../bin -lr++
