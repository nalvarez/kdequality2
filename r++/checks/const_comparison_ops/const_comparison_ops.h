#ifndef CONST_COMPARISON_OPS_H
#define CONST_COMPARISON_OPS_H

#include "rpp-check-visitor.h"

namespace rpp
{

class CheckConstComparisonOps: public CheckVisitor
{
protected:
    virtual bool visit( InitDeclaratorAST *n, SemanticEnvironment *env );
    virtual bool visit( FunctionDefinitionAST *n, SemanticEnvironment *env );
    virtual bool visit( OperatorAST *n, SemanticEnvironment *env );

    virtual void endVisit( InitDeclaratorAST *n, SemanticEnvironment *env );

private:
    bool m_hadComparisonOp;
};

}

#endif // CONST_COMPARISON_OPS_H
