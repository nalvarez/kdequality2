TEMPLATE = lib
INCLUDEPATH += $$PWD $$PWD/../../lib $$PWD/../../shared $$PWD/../../model
DESTDIR = $$PWD/../../bin
TARGET = r++check_const_comparison_ops
HEADERS += const_comparison_ops.h
SOURCES += const_comparison_ops.cpp
CONFIG += plugin
LIBS += -L$$PWD/../../bin -lr++
