TEMPLATE = lib
INCLUDEPATH += $$PWD $$PWD/../../lib $$PWD/../../shared
DESTDIR = $$PWD/../../bin
TARGET = r++check_null
HEADERS += null.h
SOURCES += null.cpp
CONFIG += plugin
LIBS += -L$$PWD/../../bin -lr++
