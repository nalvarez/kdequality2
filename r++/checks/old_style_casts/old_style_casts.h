#ifndef NULL_H
#define NULL_H

#include "rpp-check-visitor.h"

namespace rpp
{

class CheckOldStyleCasts: public CheckVisitor
{
public:
    virtual bool visit( CastExpressionAST *ast, SemanticEnvironment *env );
};

}

#endif // NULL_H
