#include "old_style_casts.h"

#include "rpp-ast.h"

using namespace rpp;

bool CheckOldStyleCasts::visit( CastExpressionAST *ast, SemanticEnvironment * )
{
    // XXX Look at what gets casted into what, and suggest
    // static_cast, const_cast or reinterpret_cast correspondingly.

    // XXX Don't complain when casting a pointer-to-object to a
    // pointer-to-function (or the other way round), since that
    // cannot be done with C++ style casts.
    addResult( ast->start_token, "Consider using a C++ cast here." );

    return false;
}

extern "C" rpp::CheckVisitor *create_instance()
{
  return new rpp::CheckOldStyleCasts;
}

