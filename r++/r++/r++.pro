TEMPLATE = app
QT = core
DESTDIR = ../bin
TARGET = r++0
SOURCES += $$PWD/rpp-main.cpp

LIBS += -L$$PWD/../bin -lr++
INCLUDEPATH += $$PWD/../lib $$PWD/../shared

CONFIG += console
mac:CONFIG -= app_bundle
