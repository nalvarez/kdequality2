/*
    Copyright (C) 2002-2007 Roberto Raggi <roberto@kdevelop.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef RPP_AST_FWD_H
#define RPP_AST_FWD_H

namespace rpp {

class AST;
class AccessSpecifierAST;
class AsmDefinitionAST;
class BaseClauseAST;
class BaseSpecifierAST;
class BinaryExpressionAST;
class CastExpressionAST;
class ClassMemberAccessAST;
class ClassSpecifierAST;
class CompoundStatementAST;
class ConditionAST;
class ConditionalExpressionAST;
class CppCastExpressionAST;
class CtorInitializerAST;
class DeclarationAST;
class DeclarationStatementAST;
class DeclaratorAST;
class DeleteExpressionAST;
class DoStatementAST;
class ElaboratedTypeSpecifierAST;
class EnumSpecifierAST;
class EnumeratorAST;
class ExceptionSpecificationAST;
class ExpressionAST;
class ExpressionOrDeclarationStatementAST;
class ExpressionStatementAST;
class ForStatementAST;
class FunctionCallAST;
class FunctionDefinitionAST;
class IfStatementAST;
class IncrDecrExpressionAST;
class InitDeclaratorAST;
class InitializerAST;
class InitializerClauseAST;
class LabeledStatementAST;
class LinkageBodyAST;
class LinkageSpecificationAST;
class MemInitializerAST;
class NameAST;
class NamespaceAST;
class NamespaceAliasDefinitionAST;
class NewDeclaratorAST;
class NewExpressionAST;
class NewInitializerAST;
class NewTypeIdAST;
class OperatorAST;
class OperatorFunctionIdAST;
class ParameterDeclarationAST;
class ParameterDeclarationClauseAST;
class PostfixExpressionAST;
class PrimaryExpressionAST;
class PtrOperatorAST;
class PtrToMemberAST;
class ReturnStatementAST;
class SimpleDeclarationAST;
class SimpleTypeSpecifierAST;
class SizeofExpressionAST;
class StatementAST;
class StringLiteralAST;
class SubscriptExpressionAST;
class SwitchStatementAST;
class TemplateArgumentAST;
class TemplateDeclarationAST;
class TemplateParameterAST;
class ThrowExpressionAST;
class TranslationUnitAST;
class TryBlockStatementAST;
class TypeIdAST;
class TypeIdentificationAST;
class TypeParameterAST;
class TypeSpecifierAST;
class TypedefAST;
class UnaryExpressionAST;
class UnqualifiedNameAST;
class UsingAST;
class UsingDirectiveAST;
class WhileStatementAST;
class WinDeclSpecAST;

} // namespace rpp

#endif // RPP_AST_FWD_H

// kate: space-indent on; indent-width 2; replace-tabs on;
