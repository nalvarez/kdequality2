/*
    Copyright (C) 2007 Frerich Raabe <raabe@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include "rpp-check-visitor.h"
#include "rpp-parser.h"

using namespace rpp;

CheckVisitor::CheckVisitor()
    : _M_parser (0)
{
}

void CheckVisitor::setParser(const Parser *parser)
{
    _M_parser = const_cast<Parser *> (parser);
}

void CheckVisitor::addResult(std::size_t token_index, const QString &message)
{
    QString filename;
    int line;
    positionAt (token_index, &line, &filename);

    Result r = {filename, line, message};
    _M_results.append (r);
}

void CheckVisitor::positionAt(std::size_t token_index, int *line, QString *filename)
{
    const TokenStream *token_stream = _M_parser->tokenStream();
    std::size_t offset = token_stream->position(token_index);
    int column;
    _M_parser->locationManager()->positionAt(offset, line, &column, filename);
}

