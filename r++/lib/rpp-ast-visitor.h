/*
    Copyright (C) 2002-2007 Roberto Raggi <roberto@kdevelop.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef RPP_AST_VISITOR_H
#define RPP_AST_VISITOR_H

#include "rpp-ast-fwd.h"

namespace rpp {

class SemanticEnvironment;

class ASTVisitor
{
public:
  virtual ~ASTVisitor () {}

  virtual bool preVisit (AST *, SemanticEnvironment *) { return true; }
  virtual void postVisit (AST *, SemanticEnvironment *) {}

  virtual bool visit (AccessSpecifierAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (AsmDefinitionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (BaseClauseAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (BaseSpecifierAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (BinaryExpressionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (CastExpressionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (ClassMemberAccessAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (ClassSpecifierAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (CompoundStatementAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (ConditionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (ConditionalExpressionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (CppCastExpressionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (CtorInitializerAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (DeclarationStatementAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (DeclaratorAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (DeleteExpressionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (DoStatementAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (ElaboratedTypeSpecifierAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (EnumSpecifierAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (EnumeratorAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (ExceptionSpecificationAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (ExpressionOrDeclarationStatementAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (ExpressionStatementAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (FunctionCallAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (FunctionDefinitionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (ForStatementAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (IfStatementAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (IncrDecrExpressionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (InitDeclaratorAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (InitializerAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (InitializerClauseAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (LabeledStatementAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (LinkageBodyAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (LinkageSpecificationAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (MemInitializerAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (NameAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (NamespaceAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (NamespaceAliasDefinitionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (NewDeclaratorAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (NewExpressionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (NewInitializerAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (NewTypeIdAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (OperatorAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (OperatorFunctionIdAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (ParameterDeclarationAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (ParameterDeclarationClauseAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (PostfixExpressionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (PrimaryExpressionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (PtrOperatorAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (PtrToMemberAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (ReturnStatementAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (SimpleDeclarationAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (SimpleTypeSpecifierAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (SizeofExpressionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (StringLiteralAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (SubscriptExpressionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (SwitchStatementAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (TemplateArgumentAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (TemplateDeclarationAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (TemplateParameterAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (ThrowExpressionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (TranslationUnitAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (TryBlockStatementAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (TypeIdAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (TypeIdentificationAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (TypeParameterAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (TypedefAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (UnaryExpressionAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (UnqualifiedNameAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (UsingAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (UsingDirectiveAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (WhileStatementAST *, SemanticEnvironment *) { return true; }
  virtual bool visit (WinDeclSpecAST *, SemanticEnvironment *) { return true; }

  virtual void endVisit (AccessSpecifierAST *, SemanticEnvironment *) {}
  virtual void endVisit (AsmDefinitionAST *, SemanticEnvironment *) {}
  virtual void endVisit (BaseClauseAST *, SemanticEnvironment *) {}
  virtual void endVisit (BaseSpecifierAST *, SemanticEnvironment *) {}
  virtual void endVisit (BinaryExpressionAST *, SemanticEnvironment *) {}
  virtual void endVisit (CastExpressionAST *, SemanticEnvironment *) {}
  virtual void endVisit (ClassMemberAccessAST *, SemanticEnvironment *) {}
  virtual void endVisit (ClassSpecifierAST *, SemanticEnvironment *) {}
  virtual void endVisit (CompoundStatementAST *, SemanticEnvironment *) {}
  virtual void endVisit (ConditionAST *, SemanticEnvironment *) {}
  virtual void endVisit (ConditionalExpressionAST *, SemanticEnvironment *) {}
  virtual void endVisit (CppCastExpressionAST *, SemanticEnvironment *) {}
  virtual void endVisit (CtorInitializerAST *, SemanticEnvironment *) {}
  virtual void endVisit (DeclarationStatementAST *, SemanticEnvironment *) {}
  virtual void endVisit (DeclaratorAST *, SemanticEnvironment *) {}
  virtual void endVisit (DeleteExpressionAST *, SemanticEnvironment *) {}
  virtual void endVisit (DoStatementAST *, SemanticEnvironment *) {}
  virtual void endVisit (ElaboratedTypeSpecifierAST *, SemanticEnvironment *) {}
  virtual void endVisit (EnumSpecifierAST *, SemanticEnvironment *) {}
  virtual void endVisit (EnumeratorAST *, SemanticEnvironment *) {}
  virtual void endVisit (ExceptionSpecificationAST *, SemanticEnvironment *) {}
  virtual void endVisit (ExpressionOrDeclarationStatementAST *, SemanticEnvironment *) {}
  virtual void endVisit (ExpressionStatementAST *, SemanticEnvironment *) {}
  virtual void endVisit (FunctionCallAST *, SemanticEnvironment *) {}
  virtual void endVisit (FunctionDefinitionAST *, SemanticEnvironment *) {}
  virtual void endVisit (ForStatementAST *, SemanticEnvironment *) {}
  virtual void endVisit (IfStatementAST *, SemanticEnvironment *) {}
  virtual void endVisit (IncrDecrExpressionAST *, SemanticEnvironment *) {}
  virtual void endVisit (InitDeclaratorAST *, SemanticEnvironment *) {}
  virtual void endVisit (InitializerAST *, SemanticEnvironment *) {}
  virtual void endVisit (InitializerClauseAST *, SemanticEnvironment *) {}
  virtual void endVisit (LabeledStatementAST *, SemanticEnvironment *) {}
  virtual void endVisit (LinkageBodyAST *, SemanticEnvironment *) {}
  virtual void endVisit (LinkageSpecificationAST *, SemanticEnvironment *) {}
  virtual void endVisit (MemInitializerAST *, SemanticEnvironment *) {}
  virtual void endVisit (NameAST *, SemanticEnvironment *) {}
  virtual void endVisit (NamespaceAST *, SemanticEnvironment *) {}
  virtual void endVisit (NamespaceAliasDefinitionAST *, SemanticEnvironment *) {}
  virtual void endVisit (NewDeclaratorAST *, SemanticEnvironment *) {}
  virtual void endVisit (NewExpressionAST *, SemanticEnvironment *) {}
  virtual void endVisit (NewInitializerAST *, SemanticEnvironment *) {}
  virtual void endVisit (NewTypeIdAST *, SemanticEnvironment *) {}
  virtual void endVisit (OperatorAST *, SemanticEnvironment *) {}
  virtual void endVisit (OperatorFunctionIdAST *, SemanticEnvironment *) {}
  virtual void endVisit (ParameterDeclarationAST *, SemanticEnvironment *) {}
  virtual void endVisit (ParameterDeclarationClauseAST *, SemanticEnvironment *) {}
  virtual void endVisit (PostfixExpressionAST *, SemanticEnvironment *) {}
  virtual void endVisit (PrimaryExpressionAST *, SemanticEnvironment *) {}
  virtual void endVisit (PtrOperatorAST *, SemanticEnvironment *) {}
  virtual void endVisit (PtrToMemberAST *, SemanticEnvironment *) {}
  virtual void endVisit (ReturnStatementAST *, SemanticEnvironment *) {}
  virtual void endVisit (SimpleDeclarationAST *, SemanticEnvironment *) {}
  virtual void endVisit (SimpleTypeSpecifierAST *, SemanticEnvironment *) {}
  virtual void endVisit (SizeofExpressionAST *, SemanticEnvironment *) {}
  virtual void endVisit (StringLiteralAST *, SemanticEnvironment *) {}
  virtual void endVisit (SubscriptExpressionAST *, SemanticEnvironment *) {}
  virtual void endVisit (SwitchStatementAST *, SemanticEnvironment *) {}
  virtual void endVisit (TemplateArgumentAST *, SemanticEnvironment *) {}
  virtual void endVisit (TemplateDeclarationAST *, SemanticEnvironment *) {}
  virtual void endVisit (TemplateParameterAST *, SemanticEnvironment *) {}
  virtual void endVisit (ThrowExpressionAST *, SemanticEnvironment *) {}
  virtual void endVisit (TranslationUnitAST *, SemanticEnvironment *) {}
  virtual void endVisit (TryBlockStatementAST *, SemanticEnvironment *) {}
  virtual void endVisit (TypeIdAST *, SemanticEnvironment *) {}
  virtual void endVisit (TypeIdentificationAST *, SemanticEnvironment *) {}
  virtual void endVisit (TypeParameterAST *, SemanticEnvironment *) {}
  virtual void endVisit (TypedefAST *, SemanticEnvironment *) {}
  virtual void endVisit (UnaryExpressionAST *, SemanticEnvironment *) {}
  virtual void endVisit (UnqualifiedNameAST *, SemanticEnvironment *) {}
  virtual void endVisit (UsingAST *, SemanticEnvironment *) {}
  virtual void endVisit (UsingDirectiveAST *, SemanticEnvironment *) {}
  virtual void endVisit (WhileStatementAST *, SemanticEnvironment *) {}
  virtual void endVisit (WinDeclSpecAST *, SemanticEnvironment *) {}
};

} // namespace rpp

#endif // RPP_AST_VISITOR_H

// kate: space-indent on; indent-width 2; replace-tabs on;
