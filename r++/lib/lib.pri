
INCLUDEPATH += $$PWD $$PWD/../shared

HEADERS += \
    $$PWD/rpp-ast.h \
    $$PWD/rpp-ast-fwd.h \
    $$PWD/rpp-ast-visitor.h \
    $$PWD/rpp-check-visitor.h \
    $$PWD/rpp-control.h \
    $$PWD/rpp-lexer.h \
    $$PWD/rpp-list.h \
    $$PWD/rpp-memory-pool.h \
    $$PWD/rpp-parser.h \
    $$PWD/rpp-symbol.h \
    $$PWD/rpp-tokens.h

SOURCES += \
    $$PWD/rpp-ast.cpp \
    $$PWD/rpp-ast-visitor.cpp \
    $$PWD/rpp-check-visitor.cpp \
    $$PWD/rpp-control.cpp \
    $$PWD/rpp-lexer.cpp \
    $$PWD/rpp-list.cpp \
    $$PWD/rpp-memory-pool.cpp \
    $$PWD/rpp-parser.cpp \
    $$PWD/rpp-tokens.cpp

