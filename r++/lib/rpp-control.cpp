/*
    Copyright (C) 2002-2007 Roberto Raggi <roberto@kdevelop.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#include "rpp-control.h"
#include "rpp-lexer.h"

namespace rpp {

Control::Control()
  : _M_skipFunctionBody(false),
    _M_lexer(0),
    _M_parser(0)
{
}

Control::~Control()
{
}

Lexer *Control::changeLexer(Lexer *lexer)
{
  Lexer *old = _M_lexer;
  _M_lexer = lexer;
  return old;
}

Parser *Control::changeParser(Parser *parser)
{
  Parser *old = _M_parser;
  _M_parser = parser;
  return old;
}

QList<Control::ErrorMessage> Control::errorMessages () const
{
  return _M_error_messages;
}

void Control::clearErrorMessages ()
{
  _M_error_messages.clear ();
}

void Control::reportError (const ErrorMessage &errmsg)
{
    _M_error_messages.append(errmsg);
}

} // namespace rpp

// kate: space-indent on; indent-width 2; replace-tabs on;
