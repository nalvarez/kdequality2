/*
    Copyright (C) 2002-2007 Roberto Raggi <roberto@kdevelop.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/

#ifndef CONTROL_H
#define CONTROL_H

#include "rpp-symbol.h"
#include "rpp-memory-pool.h"

#include <QtCore/QHash>

namespace rpp {

class Declarator;
class Type;
class Lexer;
class Parser;

class Control
{
public:
  class ErrorMessage
  {
  public:
    ErrorMessage ():
      _M_line (0),
      _M_column (0) {}

    inline int line () const { return _M_line; }
    inline void setLine (int line) { _M_line = line; }

    inline int column () const { return _M_column; }
    inline void setColumn (int column) { _M_column = column; }

    inline QString fileName () const { return _M_fileName; }
    inline void setFileName (const QString &fileName) { _M_fileName = fileName; }

    inline QString message () const { return _M_message; }
    inline void setMessage (const QString &message) { _M_message = message; }

  private:
    int _M_line;
    int _M_column;
    QString _M_fileName;
    QString _M_message;
  };

  Control();
  ~Control();

  inline bool skipFunctionBody() const { return _M_skipFunctionBody; }
  inline void setSkipFunctionBody(bool skip) { _M_skipFunctionBody = skip; }

  Lexer *changeLexer(Lexer *lexer);
  Parser *changeParser(Parser *parser);

  Lexer *currentLexer() const { return _M_lexer; }
  Parser *currentParser() const { return _M_parser; }

  inline const NameSymbol *findOrInsertName(const char *data, size_t count)
  { return name_table.findOrInsert(data, count); }

  void reportError (const ErrorMessage &errmsg);
  QList<ErrorMessage> errorMessages () const;
  void clearErrorMessages ();

private:
  NameTable name_table;
  bool _M_skipFunctionBody;
  Lexer *_M_lexer;
  Parser *_M_parser;

  QList<ErrorMessage> _M_error_messages;
};

} // namespace rpp

#endif // CONTROL_H

// kate: space-indent on; indent-width 2; replace-tabs on;
