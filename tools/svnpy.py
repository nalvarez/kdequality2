### svnpy.py
###
### A module that implements some common functionality for the
### visualization tools in SVNTools.
###
### Copyright (C) 2006 by Adriaan de Groot, University of Nijmegen
###
### This file is licensed under the terms of the GNU General Public
### License (GPL) version 2.
###
###

import sys
from svn import fs, core, delta
from datetime import date
import Image, ImageDraw, ImageFont

# Get the date stamp of a given SVN revision from the FS as a date object
def revdate( FS, rev ):
  timestamp = fs.revision_prop( FS, rev, core.SVN_PROP_REVISION_DATE )

# Get the hour of a given SVN revision from the FS.
def revhour( FS, rev ):
  timestamp = fs.revision_prop( FS, rev, core.SVN_PROP_REVISION_DATE )
  return int(timestamp[11:13])


# Get the weekday of a given SVN revision from the FS.
def revweekday( FS, rev ):
  timestamp = fs.revision_prop( FS, rev, core.SVN_PROP_REVISION_DATE )
  return date( int(timestamp[0:4]), int(timestamp[5:7]), int(timestamp[8:10]) ).weekday()


# Get the contents of a given SVN filename from the root.
def filecontents( root, filename ):
  file = fs.file_contents(root, filename)
  filedata = ''
  while 1:
    data = core.svn_stream_read(file, 8192)
    if not data:
      break
    filedata = filedata + data

  return filedata

# Count the number of lines changed between the prev and current revisions.
def count_delta( prevroot, prevfile, root, file ):
  lines = 0
  d = fs.get_file_delta_stream(prevroot,prevfile,root,file)
  w = delta.svn_txdelta_next_window(d)
  while not w is None:
    lines += w.new_data.count('\n')
    w = delta.svn_txdelta_next_window(d)
  return lines

# Count number of lines in file in the revision.
def count_filelines( root, filename ):
  lines = 0
  file = fs.file_contents(root, filename)
  while 1:
    data = core.svn_stream_read(file, 8192)
    if not data:
      break
    lines += data.count('\n')
  return lines

class Progress:
  """
  This is a progress-reporting class. It just encapsulates a common
  method of showing progress while iterating over an SVN repository.
  For each revision processed, call next() which will dump to stderr
  some information every 100 revisions.

  You can initialize the revision count it prints by setting @p r.
  """
  def __init__(self,r=0):
    self.c = 0
    self.d = 0
    self.r = r

  def reset(self,r):
    """
    Reset the progress reporter to some specific revision.
    """
    __init__(self,r)

  def next(self):
    """
    Report one work unit / revision processed. Every 100 units,
    some information is sent to stderr to keep the user awake.
    """
    self.r = self.r+1
    self.c = self.c+1
    if ( self.c == 10 ):
      sys.stderr.write('.')
      self.c = 0
      self.d = self.d + 1
      if ( self.d == 12 ) :
        sys.stderr.write("%8d" % self.r)
        self.d = 0


class HorizontalBarGraph:
  """
  Class for creating horizontal bar graphs .. those are graphs
  where the bars are vertical and the important labeled axis is
  horizontal.

  Create the HorizontalBarGraph object with two parameters:
  the list of data to use and a list of labels. The two lists
  must be equally long, or an assert() is triggered.

  Draw the graph by calling graph().
  """
  def __init__(self,data,labels):
    assert( len(data) == len(labels) )
    self.h = data
    self.l = labels

  def graph(self,barwidth,filename,with_labels=True):
    """
    To draw the actual graph, call graph() with a bar width --
    this is multiplied by the font character width, so 1 produces
    a one-character-wide bar, 2 a two-character, etc. The
    graph is made rougly square and written to the given filename.
    """
    # Calculate the "grid" size
    font = ImageFont.load("neep.pil")
    BlockX,BlockY = font.getsize("t")

    # Create the image and fill it with something soothing
    ImageWidth = len(self.h)*barwidth*BlockX
    ImageHeight = ImageWidth + 3*BlockY
    im = Image.new( "RGB", ( ImageWidth, ImageHeight ) )
    draw = ImageDraw.Draw(im)
    draw.rectangle( [ (0,0),(ImageWidth,ImageHeight) ], fill="#FFFFFF", outline="#FFFFFF")

    for i in range(0,len(self.l)):
      draw.text( (i*barwidth*BlockX,ImageWidth+2*BlockY), self.l[i], font=font, fill="black" )

    darker="#C8AF8C"
    pale="#FFF0D2"
    MaxValue = max(self.h)
    for i in range(0,len(self.h)):
      BarTop = ImageWidth*(MaxValue-self.h[i])/MaxValue
      draw.rectangle( (i*barwidth*BlockX+1,ImageWidth+BlockY) + ((i+1)*barwidth*BlockX-1,BarTop+BlockY), fill=darker )
      if with_labels:
        label = "%d" % self.h[i]
        textWidth,textHeight = font.getsize(label)
        draw.text( (i*barwidth*BlockX+(barwidth*BlockX-textWidth)/2,BarTop), label, font=font, fill="black" )

    del draw
    im.save( filename, "PNG" )
    del im


