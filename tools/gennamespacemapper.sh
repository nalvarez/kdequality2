#!/bin/sh
# map a search maps for namespaces

# $1 is the component and $2 is the module
if ( test ! -d $1-api/$2-apidocs )
then
  exit 1
fi

echo "<?php"
echo "\$map = array("
for htmlfile in `find $1-api/$2-apidocs/ -type f -name "namespace[A-Z]*.html"`; 
do
  nsname=`echo $htmlfile | sed -e "s,.*/namespace\\(.*\\).html,\1," -e "s,_1_1,::,g" | tr "[A-Z]" "[a-z]"`
  echo "  \"$htmlfile\" => \"$nsname\","
done
echo "  \"index\" => \"$1-api/\""
echo ");"
echo "?>"

