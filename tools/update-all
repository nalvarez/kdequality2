#! /bin/sh
# Where the update programs live
BINTOP=/home/api/bin
# Where we store the logs
LOGPATH=/srv/logs/all

# Functions
Date() {
  DATE=`date`
}

# Prepare to run..
umask 0002
if [ ! -d $LOGPATH ]; then
    mkdir -p $LOGPATH
fi

LOGBASE=`date '+%Y%m%d%H%M%S'`
LOGFILE=$LOGPATH/$LOGBASE
cat /dev/null > $LOGFILE
(cd $LOGPATH; ln -sf $LOGBASE current)

### Start update process - update scripts and checkouts first....
echo "update-all run by $USER" | tee -a $LOGFILE
Date; echo "$DATE: $$ ********" | tee -a $LOGFILE

Date; echo "$DATE: *** Update script checkouts" | tee -a $LOGFILE
sh $BINTOP/update-scripts >> $LOGFILE 2>&1

Date; echo "$DATE: *** Update checkouts    " | tee -a $LOGFILE
sh $BINTOP/update-checkouts >> $LOGFILE 2>&1

# Run the quality checks
Date; echo "$DATE: *** Performing quality checks" | tee -a $LOGFILE
sh $BINTOP/update-apidox >> $LOGFILE &
sh $BINTOP/update-krazy >> $LOGFILE &
sh $BINTOP/update-i18n >> $LOGFILE &
sh $BINTOP/update-sanitizer >> $LOGFILE &
wait

# Update api.kde.org symlinks
Date; echo "$DATE: *** Setting up symlinks" | tee -a $LOGFILE
sh $BINTOP/update-symlinks

# Update the qch and man files
Date; echo "$DATE: *** Start QCH and ManPage generation" | tee -a $LOGFILE
sh $BINTOP/update-qch-man >> $LOGFILE 2>&1

# Update the classmapper files
Date; echo "$DATE: *** Update Classmapper files" | tee -a $LOGFILE
sh $BINTOP/update-mapper >> $LOGFILE 2>&1

# Update our custom CMake modules doc
Date; echo "$DATE: *** Update CMake modules doc" | tee -a $LOGFILE
sh $BINTOP/update-cmakedoc >> $LOGFILE 2>&1

# Run the quality tools used on ebn.kde.org
Date; echo "$DATE: *** Update details on EBN for API Generation" | tee -a $LOGFILE
sh $BINTOP/parse-api-logs >> $LOGFILE

# Cleanup database
Date; echo "$DATE: *** Clean the db        " | tee -a $LOGFILE
sh $BINTOP/cleanup-db >> $LOGFILE 2>&1

# we are done
Date; echo "$DATE: ********* DONE *********" | tee -a $LOGFILE
(cd $LOGPATH; rm -f current last; ln -s $LOGBASE last)

# Disabled items :(
#Date; echo "$DATE: *** Update graphs       "  | tee -a $LOGFILE
#(cd $BINTOP && sh $BINTOP/update-graphs >> $LOGFILE 2>&1)