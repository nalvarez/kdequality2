#! /usr/bin/env python

# Weekdaily - a program to tell you how many commits happened on
#   which days of the week, averaged.

# License is GPL v 2.

import sys
import os
import getopt
from svn import fs, core, repos
import datetime
import svnpy



# Produce an array of commits (per hour) from  the repo from rev. First to Last
def whowhat( repospath, First, Last=None, Exclude=[''] ):
  # Strip trailing / since it breaks stuff
  if repospath[-1] == "/":
    repospath = repospath[:-1]

  # Get the repo and the Last revision in it.
  Repo = repos.open( repospath )
  FS = repos.fs( Repo )
  if Last is None:
    Last = fs.youngest_rev( FS )

  if Last < First:
    raise ArgumentError,"First must be before Last (%d)" % Last

  Hours = [0] * 24
  WeekDays = [0] * 7
  p = svnpy.Progress(First)

  # Now walk all the SVN commits.
  c = 0
  d = 0
  for i in xrange(First, Last+1):
    Author = fs.revision_prop( FS, i, core.SVN_PROP_REVISION_AUTHOR) or ''
    if not Author in Exclude:
      day = svnpy.revweekday(FS,i)
      hour = svnpy.revhour(FS,i)
      WeekDays[day] += 1
      Hours[hour] += 1
    p.next()

  return ( WeekDays, Hours )




if __name__ == '__main__':
  Usage = """
Usage: %s [OPTIONS] REPOS-PATH
  --first=N        First revision to read (default 1)
  --last=N         Last revision to read (default HEAD)
  --outdir=dir     Directory for output graphs (default none)
  --limit=N        Limit output to top N committers (default all)
  --exclude=S[,S]  List of accounts to exclude (default '')

""" % sys.argv[0]

  if len(sys.argv) < 2:
    sys.stderr.write(Usage)
    sys.exit(1)

  l = []
  try:
    l = getopt.getopt(sys.argv[1:],'',['first=','last=','outdir=','exclude='])
  except getopt.GetoptError:
    sys.stderr.write(Usage)
    sys.exit(1)

  if len(l[1]) != 1:
    sys.stderr.write(Usage)
    sys.exit(1)

  First = 1
  Last = None;
  Except = ['']
  for i in l[0]:
    if i[0] == '--first':
      First = int(i[1])
    if i[0] == '--last':
      Last = int(i[1])
    if i[0] == '--exclude':
      Except = i[1].split(',')
  ( Days, Hours ) = whowhat( l[1][0], First, Last, Except )

  # Now set up info for the graph and rotate.
  DayNames = [ 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun' ]
  todayday = datetime.date.today().weekday()
  Days = Days[todayday:] + Days[:todayday]
  DayNames = DayNames[todayday:] + DayNames[:todayday]

  Dirname = None
  for i in l[0]:
    if i[0] == '--outdir':
      Dirname = i[1]
  if not Dirname is None:
    g = svnpy.HorizontalBarGraph( Days, DayNames )
    g.graph(6,Dirname + "/commits-weekdaily.png")

    HourNames = [ 'midnight' ] + [ '' ] * 5 + [ '6am' ] + [ '' ] * 5 + [ 'noon' ] + [ '' ] * 5 + [ '6pm' ] + [ '' ] * 5
    g = svnpy.HorizontalBarGraph( Hours, HourNames )
    g.graph( 2, Dirname + "/commits-hourly.png", False )
