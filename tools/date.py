import sys
import Image, ImageDraw, ImageFont

filename = sys.argv[1]
line = int(sys.argv[2])
text = sys.argv[3]
print "# Date-stamping `%s'" % filename

im = Image.open( filename )
draw = ImageDraw.Draw(im)

font = ImageFont.load("neep.pil")
BlockX,BlockY = font.getsize("t")

w,h = im.size
draw.text( ( w - 8 - BlockX * len(text), BlockY * line ), text, font=font, fill="black" )

im.save( filename, "PNG" )
