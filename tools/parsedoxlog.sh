#! /bin/sh
VERBOSE=false
VERSION=""

# Read arguments...
test "x--verbose" = "x$1" && { shift ; VERBOSE=true ; }
test "x--version" = "x$1" && { shift ; VERSION="$1" ; shift ; }
test -z "$VERSION" && { echo "No version given"; exit 1 ; }

# Configuration
# APIPATH: top-level for API stuff
APIPATH=/srv/www/api.kde.org
# OUTPATH: where the final output goes
OUTPATH=$APIPATH/apidocs/apidox-$VERSION
# TEMPPATH: where the temporary output goes
TEMPPATH=/srv/tmp/tempdocs/apidox-$VERSION
# TAGPATH: where tag files (i.e, qtX.Y.tag) reside
TAGPATH=/srv/sources/quality/apidox/data
# BINPATH: where the helper programs (i.e. doxylog2html.pl) reside
BINPATH=/home/api/bin
# SRCPREFIX: where the top-level KDE source code resides
SRCPREFIX=/srv/sources
# LOGPATH: where logs get moved
LOGPATH=/srv/logs/api/$VERSION
# SRCPATH: where the source code for this component resides
SRCPATH=$SRCPREFIX/$VERSION
# LIBSPATH: where the source code for kdelibs resides (so we can find doxygen.sh)
LIBSPATH=$HOME/
# DOXDATAPATH: where the doxygen.sh data lives
DOXDATAPATH=$LIBSPATH/kdelibs/doc/common
# Database access variables
PSQL="psql" #replace with echo when/if the database is in a bad way
PSQL_FLAGS="-t -h localhost -U kde ebn"

# Determine the modules we need to build the documentation of...
case "$VERSION" in
  kde-3.5|kde-4.0|kde-4.1|kde-4.2|kde-4.3|kde-4.4|kde-4.5|kde-4.6|kde-4.7|kde-4.8|kde-4.x )
    SRCPATH=$SRCPREFIX/$VERSION
    MODULES=`ls $SRCPATH`
    DOXDATAPATH=$SRCPREFIX/$VERSION/kdelibs/doc/common
    ;;
  * )
    SRCPATH=$SRCPREFIX/$VERSION
    MODULES=`ls $SRCPATH`
    ;;
esac

# If argument given, use it as modules list to run
test -n "$1" && MODULES="$1"

# Prepare to run....
test -d "$OUTPATH" || { echo "No path $OUTPATH -- creating it" ; mkdir -p $OUTPATH || exit 1 ; }
test -d "$TEMPPATH" || { echo "No path $TEMPPATH -- creating it" ; mkdir -p $TEMPPATH || exit 1 ; }
test -d "$LOGPATH" || { echo "No path $LOGPATH -- creating it" ; mkdir -p $LOGPATH || exit 1 ; }
test -d "$SRCPATH" || { echo "No source path $SRCPATH" ; exit 1 ; }
cd "$TEMPPATH" || { echo "Cannot cd into $TEMPPATH" ; exit 1 ; }

$VERBOSE && echo "* Parsing logs for $MODULES"

timestamp=""

# Find my flavor (dox for various versions have different flavors)
if [ "$PSQL" != "echo" ]; then
  component=`echo "SELECT id FROM components WHERE name = '$VERSION';" | $PSQL $PSQL_FLAGS | sed 's+^\s*++'`
expr 0 + "$component" > /dev/null 2>&1 || { echo "Could not get component."; exit 1 ; }
  flavor=`echo "SELECT id FROM tools WHERE name='dox' AND component = $component ;" | $PSQL $PSQL_FLAGS | sed 's+^\s*++'`
expr 0 + "$flavor" > /dev/null 2>&1 || { echo "Could not get flavor." ; exit 1 ; }
else
  component="kde"
  flavor="kde"
fi

# If argument (module list) given, don't overwrite log data
if test -z "$1" ; then
	timestamp=`date "+%B %d %Y %T"`
	# Bump generation now
	generation=`echo "SELECT * FROM nextval('generation') ;" | $PSQL $PSQL_FLAGS | sed 's+^\s*++'` ; 
else
	generation=`echo "SELECT generation FROM tools WHERE id = $flavor" | $PSQL $PSQL_FLAGS | sed 's+^\s*++'` ;
fi

if [ "$PSQL" != "echo" ]; then
  expr 0 + "$generation" > /dev/null 2>&1 || { echo "Bad generation." ; exit 1 ; }
else
  generation="1"
fi
generation=`expr 0 + "$generation"`

$VERBOSE && { echo "* Component $component"; echo "* Tool      $flavor" ; echo "* Generation $generation" ; }

export QTDOCDIR=http://doc.qt.nokia.com/4.7
export QTDOCTAG=$TAGPATH/qt4.7.tag
test "kde-3.5" = "$VERSION" && QTDOCDIR=http://doc.qt.nokia.com/3.3 && QTDOCTAG=$TAGPATH/qt3.3.tag

REV=`svn info svn://anonsvn.kde.org/home/kde/ | grep Revision | awk '{print \$2}'`

for i in $MODULES; do 
	LOGFILE=$LOGPATH/$i.log
	test -d "$SRCPATH/$i" || { echo "Skipping $SRCPATH/$i" ; continue ; }

	# Just count the total number of errors
	AMT=`grep ^$SRCPATH $LOGFILE | wc -l`
	expr 0 + "$AMT" > /dev/null 2>&1 || AMT="0"
	$VERBOSE && { echo "* Number of errors: $AMT" ; \
		echo "* Number of applications: " `grep 'Creating apidox' "$i.log" | wc -l` ; }

	# An impenetrable forest of awkisms. Scan the logfile,
	# breaking it into sections using the *** Creating
	# header; count total errors under a top-level application
	# heading (eg. dcop or kio) and print the results as
	# lines with 
	#     appname count
	#
	# ... then pipe that to a while loop that reads the results
	# and adds records to the results table for each appname.
	: > /tmp/doxygen.sh.$$
	awk 'BEGIN {t=-1;app=""; ot="'$i'"; } 
	     /\*\*\* Creating apidox/ { 
		napp=$5; slash=index(napp,"/"); 
		if (slash>0) napp=substr($5,1,slash-1); 
		if (napp!=app) { 
			if (t>=0) print app,t; 
			close( ot "-" app ".log" );
			app=napp; 
			t=0; 
			print $0 > ( ot "-" app ".log" ) ;
		} 
	     } 
	     /^\/.*[Ww]arning/ { if (t>=0) t=t+1; }
	     t>=0 { print $0 >> ( ot "-" app ".log" ); } 
	     END { if (t>=0) print app,t;}' $LOGFILE | \
	while true ; do
        read APP COUNT ;
		test -z "$APP" && break ;
		echo "$APP $COUNT" >> /tmp/doxygen.sh.$$
		$BINPATH/doxylog2html.pl --explain --export=ebn --cms="apidox-$VERSION/$i/$APP" --rev=$REV $TEMPPATH/$i-$APP.log | sed s+/tempdocs/+/apidocs/+g > $TEMPPATH/$i-$APP.html;
		echo "INSERT INTO results_apidox VALUES ( $generation, '$i', '$APP', $COUNT, $component, '', 'apidox-$VERSION/$i-$APP.html' ); ";
	done | \
	$PSQL $PSQL_FLAGS > /dev/null
done

if test -z "$1" ; then
	( 
	echo "INSERT INTO generations VALUES ( $generation, '$timestamp', (SELECT SUM(issues) FROM results_apidox WHERE generation = $generation), $flavor, $REV ) ;"
	echo "UPDATE tools SET generation = $generation WHERE id = $flavor ;"
	) | $PSQL $PSQL_FLAGS | sed 's+^\s*++' > /dev/null
fi

rm -f /tmp/doxygen.sh.$$
