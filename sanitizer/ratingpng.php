<?php
 // taken from one of the comments on
 // http://us3.php.net/manual/en/function.imagefilledrectangle.php

 //this needs to reside in its own php page
 //you can include that php page in your html as you would an image:
 //<IMG SRC="ratingpng.php?rating=25.2" border="0">
function drawRating($rating) {
    if (isset($_GET['width'])) {
        $width = $_GET['width'];
    } else {
        $width = 102;
    }

    if (isset($_GET['height'])) {
        $height = $_GET['height'];
    } else {
        $height = 10;
    }
 
    $rating = $_GET['rating'];
    $ratingbar = $rating/100.0*$width;
 
    $image = imagecreate($width,$height);
    $back = ImageColorAllocate($image,255,255,255);
    $fill = ImageColorAllocate($image,208,184,140);
 
    ImageFilledRectangle($image,0,0,$ratingbar,$height-1,$fill);
    imagePNG($image);
    imagedestroy($image);
 }
 
 Header("Content-type: image/png");
 drawRating($_GET['rating']);
?>

