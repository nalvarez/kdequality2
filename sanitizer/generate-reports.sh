#!/bin/sh
kdeSources=/srv/sources
sanitizerBin=/home/api/sanitizer-scripts
destdir=/srv/www/englishbreakfastnetwork.org/sanitizer/reports
components="kde-4.x bundled-apps playground extragear" # kdesupport and kdereview do not have modules, so won't work in here.

# You shouldn't need to customize anything below here.

if [ -z "$PSQL" ]; then
	PSQL=/usr/bin/psql
fi

execSql()
{
	echo "$1" | $PSQL -t -h localhost -U kde ebn -A -q
}

myname=`basename $0`

enforceUpdate=0
if [ -e force-update.tag ]; then
	echo "$myname: Update enforced."
	enforceUpdate=1
	rm force-update.tag
fi

# Caching is broken somehow. Disabled it for now.
enforceUpdate=1

svn_revision=`svn info svn://anonsvn.kde.org/home/kde/ | grep Revision | awk '{print \$2}'`

for component in $components; do
	echo "# $kdeSources/$component"
	( cd "$kdeSources/$component" > /dev/null )
done

for component in $components; do
	componentId=`execSql "SELECT id FROM components WHERE name='$component';"`
	test -z "$componentId" && echo "Warning: No componentId for $component" && continue
	expr 0 + "$componentId" > /dev/null 2>&1 || continue

	toolId=`execSql "SELECT id FROM tools WHERE name='sanitizer' AND component=$componentId;"`
	test -z "$toolId" && echo "Warning: No toolId for sanitizer and $component" && continue
	expr 0 + "$toolId" > /dev/null 2>&1 || continue

	generation=`execSql "SELECT * FROM nextval('generation');"`

	for docbookFile in `ls $kdeSources/$component/*/doc/*/index.docbook \
                               $kdeSources/$component/*/*/doc/index.docbook \
                               $kdeSources/$component/*/doc/kcontrol/*/index.docbook \
                               $kdeSources/$component/*/*/doc/*/index.docbook`; do
                #the kdevelop/index.docbook hangs the sanitizer, so skip it
                foo=`echo $docbookFile | grep -c kdevelop/doc/`
                if [ $foo -gt 0 ]; then
                  continue
                fi
                #the simon/index.docbook hangs the sanitizer, so skip it
                foo=`echo $docbookFile | grep -c simon/doc/`
                if [ $foo -gt 0 ]; then
                  continue
                fi
		fields=`echo $docbookFile | sed -e "s,^$kdeSources/,,;s,/, ,g"`
		module=`echo $fields | awk '{print $2;}'`
		app=`echo $fields | awk '{print $4;}'`
                if [ "$app" = "doc" ]; then
		  app=`echo $fields | awk '{print $3;}'`
                fi                  
		checksum=`md5sum $docbookFile | awk '{print $1}'`

		needToGenerate=1
		if [ $enforceUpdate -eq 0 ]; then
			cachedValues=`execSql "SELECT issues, report FROM results_sanitizer WHERE checksum='$checksum'"`
			if [ -n "$cachedValues" ]; then
				cachedReport=`echo $cachedValues | sed -e 's,.*|,,'`
				issues=`echo $cachedValues | sed -e 's,|.*,,'`
				test -f $destdir/$cachedReport && needToGenerate=0
			fi
		fi

		outputFile=$component/$module/$app/index.html
		mkdir -p $destdir/`dirname $outputFile`

		if [ $needToGenerate -eq 1 ]; then
			echo -n "$myname: Generating $outputFile..."
			$sanitizerBin/check.py $docbookFile "Results for $app" "$component" "$module" "$app" > $destdir/$outputFile $svn_revision
			issues=$?
			echo " done!"
		else
			echo "$myname: $outputFile is up to date."
		fi

		chmod 0664 $destdir/$outputFile > /dev/null 2>&1

#		execSql "UPDATE results_sanitizer SET checksum='$checksum', issues=$issues, report='$outputFile' WHERE component='$componentId' AND module='$module' AND application='$app';"
		execSql "INSERT INTO results_sanitizer (checksum, issues, report, component, module, application, generation) VALUES('$checksum', $issues, '$outputFile', $componentId, '$module', '$app', $generation);"
	done

	# Update our generation field in the tools table.
	execSql "UPDATE tools SET generation=$generation WHERE id=$toolId;"

	# Put a summary of this run into the generations table.
	now=`date "+%B %d %Y %T"`
	execSql "INSERT INTO generations VALUES ($generation, '$now', (SELECT SUM(issues) FROM results_sanitizer WHERE component='$componentId' AND generation=$generation), $toolId, $svn_revision );"
done

