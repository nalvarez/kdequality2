#!/usr/bin/python

import sys

if len(sys.argv) != 2:
    print "usage: check-doctype.py <docbook file>"
    sys.exit(1)

catalogFile = "/home/api/bin/sanitizer/docbook-catalog-nonxml"
catalogDecl = ""
for line in open(catalogFile).readlines():
    if line[:6] == "PUBLIC":
	firstQuote = line.find('"', 6) + 1
	if firstQuote == 0:
	    break

	secondQuote = line.find('"', firstQuote)
	if secondQuote == -1:
	    break

	catalogDecl = line[firstQuote:secondQuote]
	break

if not catalogDecl:
	print "Failed to determine catalog declaration! Please drop a "
	print "mail to <a href=\"mailto:raabe@kde.org\">Frerich "
	print "Raabe</a> about this, it's most likely a server "
	print "misconfiguration."
	sys.exit(1)

docbookDecl = ""
for line in open(sys.argv[1]).readlines():
    if line[:9] == "<!DOCTYPE":
	firstQuote = line.find('"', 9) + 1
	if firstQuote == 0:
	    break

	secondQuote = line.find('"', firstQuote)
	if secondQuote == -1:
	    break

	docbookDecl = line[firstQuote:secondQuote]
	break

if not docbookDecl:
	print "Failed to determine DOCTYPE declaration! If you do have "
	print "a <span style=\"markup\">&lt!DOCTYPE ..</span> declaration "
	print "in your markup, please drop a note about this false alert "
	print "to <a href=\"mailto:raabe@kde.org\">Frerich "
	print "Raabe</a> about this, together with the markup you gave to "
	print "the sanitizer."
	sys.exit(1)

if catalogDecl != docbookDecl:
	print "The DOCTYPE declaration doesn't seem to be the same version "
	print "as the catalog's declaration.<br/>"
	print "Your markup: <span class=\"markup\">%s</span><br/>" % docbookDecl
	print "Catalog: <span class=\"markup\">%s</span><br/>" % catalogDecl
	sys.exit(1)

sys.exit(0)

