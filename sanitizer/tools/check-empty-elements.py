#!/usr/bin/python
import re
import string
import sys

emptyElementRegExp = re.compile("<([^>]+)>(?:\\s|<!--.*-->)*</\\1>", re.DOTALL)

hits = 0
lineno = 0

for fileName in sys.argv[1:]:
	try:
		file = open(fileName, 'r')
		for m in emptyElementRegExp.finditer(file.read()):
			if hits == 0:
				print "<ul>"
			hits += 1

			lineno = m.string.count('\n', lineno, m.start()) + 1
			print "<li>Found empty <span class=\"markup\">&lt;%s&gt;</span> element in line <span class=\"lineno\">%i</span>.</li>" % (m.group(1), lineno)

		if hits != 0:
			print "</ul>"
	except IOError, e:
		print e
sys.exit(hits)

