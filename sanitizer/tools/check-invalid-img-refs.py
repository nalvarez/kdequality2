#!/usr/bin/python
import re
import string
import sys

invalidImgRefRx = re.compile("fileref=\"[^\"/]*/[^\"/]*")

hits = 0
lineno = 0

try:
	file = open(sys.argv[1], 'r')
	for m in invalidImgRefRx.finditer(file.read()):
		if hits == 0:
			print "<ul>"
			hits += 1

		lineno = m.string.count('\n', lineno, m.start()) + 1
		print "<li>Found invalid image reference in line <span class=\"lineno\">%i</span>.</li>" % (lineno)

	if hits != 0:
		print "</ul>"
except IOError, e:
	print e
sys.exit(hits)

