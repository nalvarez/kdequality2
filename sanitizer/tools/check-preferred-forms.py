#!/usr/bin/python
import re
import sys

dict = []

for line in open(sys.argv[1], 'r').readlines():
	line = line.strip()
	if line and line[0] != '#':
		tokens = line.split('/')
		if len(tokens) >= 2:
			tokens.append("")
		for i in range(len(tokens)):
			tokens[i] = tokens[i].replace("&", "&amp;")
			tokens[i] = tokens[i].replace("<", "&lt;")
			tokens[i] = tokens[i].replace(">", "&gt;")
		deprecatedFormRx = re.compile("\s%s\s" % re.escape(tokens[0]))
		dict.append((deprecatedFormRx, tokens[0], tokens[1], tokens[2]))

hits = 0
lineno = 0
incomment = False

for line in open(sys.argv[2], 'r').readlines():
	lineno += 1
	if incomment:
		end = line.find("-->")
		if end > -1:
			line = line[end + 3:]
			incomment = False
		else:
			line = ""
	start = line.find("<!--")
	while start > -1:
		end = line.find("-->", start)
		if end > -1:
			line = line[:start] + line[end+3:]
			start = line.find("<!--")
		else:
			line = line[:start]
			incomment = True
			break
	line = line.strip()
	if len(line) == 0:
		continue
	line = line.replace("&", "&amp;")
	line = line.replace("<", "&lt;")
	line = line.replace(">", "&gt;")
	for entry in dict:
		if entry[0].search(line):
			if hits == 0:
				print "<ul>"
			print "<li>"
			print "In line <span class=\"lineno\">%i</span>:" % lineno
			print "Prefer '%s' over '%s'." % (entry[2], entry[1])
			if entry[3]:
				print " (%s)" % entry[3]

			line = line.replace(entry[1], "<u>%s</u>" % entry[1])
			print "<p class=\"dbexcerpt\">..%s..</p>" % line
			print "</li>"
			hits += 1

if hits > 0:
	print "</ul>"

sys.exit(hits)

