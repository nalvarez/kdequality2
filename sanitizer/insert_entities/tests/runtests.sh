#!/bin/sh
for testfile in 28.test; do
	output=`INSERT_ENTITIES_OPTS="-k -e /usr/local/src/kde-3.5/kdelibs/kdoctools/customization/entities" ../insert_entities $testfile`
	expectedfile=`echo $testfile | sed -e "s,\.test$,\.expected,"`
	if [ "$output" != "`cat $expectedfile`" ]; then
		echo "ERROR! Failed test \"$testfile\"!"
		echo "-- Expected --------"
		cat $expectedfile
		echo "-- Got output ------"
		echo "$output"
		exit
	fi
done
echo "All OK!"
