/**
 * chunkstring.cc - (C) 2004, 2005 Frerich Raabe <raabe@kde.org>
 *
 * For licensing and distribution terms, refer to the accompanying
 * file ``COPYING''.
 */
#include "chunkstring.h"

#include <numeric>

using namespace std;

ChunkString::ChunkString( const string &s, string::value_type sep, string::value_type newSep )
	: m_string( s ),
	m_origSep( sep ),
	m_newSep( newSep )
{
	string::size_type p = m_string.find( m_origSep );
	while ( p != string::npos ) {
		m_separators.push_back( p );
		m_string[ p ] = newSep;
		p = m_string.find( m_origSep, p + 1 );
	}

	adjacent_difference( m_separators.begin(), m_separators.end(), m_separators.begin() );
}

string ChunkString::chunkString() const
{
	if ( m_separators.empty() ) {
		return m_string;
	}

	string s( m_string );

	Separators absSeparators( m_separators.size() );
	partial_sum( m_separators.begin(), m_separators.end(), absSeparators.begin() );

	Separators::const_iterator it = absSeparators.begin();
	Separators::const_iterator end = absSeparators.end();
	for ( ; it != end; ++it ) {
		s[ *it ] = m_origSep;
	}

	return s;
}

string::size_type ChunkString::find( const string &needle, string::size_type pos )
{
	return m_string.find( needle, pos );
}

void ChunkString::replace( string::size_type pos, const string &needle, const string &replacement )
{
	m_string.replace( pos, needle.size(), replacement );
	if ( m_separators.empty() || needle.size() == replacement.size() ) {
		return;
	}

	Separators::iterator affectedSepIt = m_separators.begin();
	string::size_type absPos = *affectedSepIt;
	while ( affectedSepIt != m_separators.end() && absPos < pos ) {
		++affectedSepIt;
		if ( affectedSepIt != m_separators.end() ) {
			absPos += *affectedSepIt;
		}
	}

	if ( affectedSepIt == m_separators.end() && absPos < pos ) {
		return;
	}

	unsigned int overlappedSeparators = 0;
	{
		string::size_type tmpAbsPos = absPos;
		Separators::iterator affectedSepEnd = affectedSepIt;
		while ( affectedSepEnd != m_separators.end() && tmpAbsPos < pos + needle.size() ) {
			++affectedSepEnd;
			++overlappedSeparators;
			if ( affectedSepEnd != m_separators.end() ) {
				tmpAbsPos += *affectedSepEnd;
			}
		}
	}

	if ( pos + needle.size() <= absPos ) {
		const int shift = replacement.size() - needle.size();
		*affectedSepIt += shift;
	} else {
		m_string.insert( pos + replacement.size(), 1, m_newSep );
		int shift = pos + replacement.size() - absPos;
		*affectedSepIt++ += shift;
		for ( unsigned int i = 1; i < overlappedSeparators; ++i ) {
			m_string.insert( pos + replacement.size() + 1, 1, m_newSep );
			shift -= *affectedSepIt;
			*affectedSepIt++ = 1;

		}
		if ( affectedSepIt != m_separators.end() ) {
			*affectedSepIt -= shift - ( replacement.size() - needle.size() ) - 1;
		}
	}
}

void ChunkString::replaceAll( const string &needle, const string &replacement )
{
	string::size_type p = find( needle );
	while ( p != string::npos ) {
		replace( p, needle, replacement );
		p = find( needle, p + replacement.size() );
	}
}
