/**
 * main.cc - (C) 2003, 2004, 2005 Frerich Raabe <raabe@kde.org>
 *
 * For licensing and distribution terms, refer to the accompanying
 * file ``COPYING''.
 */
#include "entity.h"
#include "entitydatabase.h"
#include "chunkstring.h"
#include "util.h"
#include "options.h"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <cstring>
#include <vector>

#include <unistd.h> // gcc 2.95.4 needs this for getopt et al

using namespace std;

vector<char *> assembleArguments( int argc, char **argv )
{
	vector<char *> arguments;
	arguments.push_back( argv[ 0 ] );

	if ( const char *s = ::getenv( "INSERT_ENTITIES_OPTS" ) ) {
		const vector<string> opts = split( s, ' ' );
		vector<string>::const_iterator it = opts.begin();
		vector<string>::const_iterator end = opts.end();
		for ( ; it != end; ++it ) {
			arguments.push_back( const_cast<char *>( ( *it ).c_str() ) );
		}
	}

	for ( int i = 1; i < argc; ++i ) {
		arguments.push_back( argv[ i ] );
	}

	return arguments;
}

int main( int argc, char **argv )
{
	int ch;
	string outputFile;
	vector<string> extraEntityFiles;

	vector<char *> arguments = assembleArguments( argc, argv );
	int argCount = arguments.size();
	while ( ( ch = getopt( argCount, &arguments[0], "?e:vko:" ) ) != -1 ) {
		switch ( ch ) {
			case '?':
				cout << "usage: insert_entities [-?] [-v] [-k] [-o <file>] [-e <file>] [file]" << endl;
				return 0;
			case 'v':
				++Options::VerbosityLevel;
				break;
			case 'k':
				Options::KDEMode = true;
				break;
			case 'o':
				outputFile = optarg;
				break;
			case 'e':
				extraEntityFiles.push_back( optarg );
				break;
		}
	}

	if ( Options::VerbosityLevel >= 2 ) {
		cout << "Given program arguments: " << endl;
		vector<char *>::const_iterator it = arguments.begin();
		++it; // Skip the program's invocation itself
		vector<char *>::const_iterator end = arguments.end();
		for ( ; it != end; ++it ) {
			cout << "  " << *it << endl;
		}
	}

	EntityDatabase database;
	{
		vector<string>::const_iterator it = extraEntityFiles.begin();
		vector<string>::const_iterator end = extraEntityFiles.end();
		for ( ; it != end; ++it ) {
			if ( !fileIsDirectory( *it ) ) {
				database.merge( *it );
				continue;
			}
			const vector<string> filesInDir = filesInDirectory( *it );
			if ( filesInDir.empty() ) {
				continue;
			}
			vector<string>::const_iterator fileIt = filesInDir.begin();
			vector<string>::const_iterator fileEnd = filesInDir.end();
			for ( ; fileIt != fileEnd; ++fileIt ) {
				database.merge( *fileIt );
			}
		}
	}

	ostringstream os;
	if ( optind == argCount ||
	     ::strcmp( arguments[ optind ], "-" ) == 0 ) {
		string line;
		while ( getline( cin, line ) ) {
			os << line;
		}
		database.merge( os.str(), "standard input" );
	} else {
		ifstream f( arguments[ optind ] );
		if ( !f ) {
			cerr << "Failed to open file " << arguments[ optind ] << endl;
			return 1;
		}
		os << f.rdbuf();
		database.merge( os.str(), arguments[ optind ] );
	}

	unsigned int hits = 0;

	ChunkString input( os.str(), '\n' );
	{
		const vector<Entity> entities = database.entities();
		vector<Entity>::const_iterator it = entities.begin();
		vector<Entity>::const_iterator end = entities.end();
		for ( ; it != end; ++it ) {
			hits += ( *it ).applyTo( input );
		}
	}

	if ( outputFile.empty() || outputFile == "-" ) {
		cout << input.chunkString();
	} else {
		ofstream f( outputFile.c_str() );
		if ( !f ) {
			cerr << "Failed to write to file " << outputFile << endl;
			return 1;
		}
		f << input.chunkString();
	}

	return hits;
}

