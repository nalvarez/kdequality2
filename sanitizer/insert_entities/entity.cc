/**
 * entity.cc - (C) 2003, 2004, 2005 Frerich Raabe <raabe@kde.org>
 *
 * For licensing and distribution terms, refer to the accompanying
 * file ``COPYING''.
 */
#include "entity.h"
#include "chunkstring.h"
#include "util.h"
#include "options.h"

#include <iostream>
#include <cstring>
#include <ctype.h>

using namespace std;

static bool isIgnoredElement( const char *e )
{
	static const char *ignoredElements[] = {
		"keyword",
		"guilabel",
		"guimenu",
		"guisubmenu",
		"guimenuitem",
		"guibutton",
		"userinput",
		"command",
		"option",
		"programlisting",
		"screen",
		"quote",
		"replaceable",
		"methodname",
		"parameter",
		"literal",
		0
	};

	for ( const char **it = ignoredElements; *it != 0; ++it )
		if ( strcmp( *it, e ) == 0 )
			return true;

	return false;
}

static string stripTags( const string &s_ )
{
	string s = s_;
	string::size_type p = s.find( '<' );
	while ( p != string::npos ) {
		string::size_type q = s.find( '>', p + 1 );
		if ( q != string::npos ) {
			s.erase( p, q - p + 1 );
		}
		p = s.find( '<', p );
	}
	return s;
}

static bool isInTag( string::size_type idx, const string &s )
{
	int depth = 0;
	string::size_type i = s.find_first_of( "<>" );
	while ( i != string::npos && i < idx ) {
		if ( s[ i ] == '<' ) {
			++depth;
		} else if ( s[ i ] == '>' ) {
			--depth;
		}
		i = s.find_first_of( "<>", i + 1 );
	}
	return depth > 0;
}

static string element( string::size_type idx, const string &s )
{
	string::size_type p = s.rfind( '<', idx );
	if ( p == string::npos )
		return string();
	++p; // Skip the '<'
	string::size_type q = s.find( '>', p );
	if ( q == string::npos )
		return string();
	return s.substr( p, q - p );
}

static bool isWordCharacter( char ch )
{
	// gcc 2.95.4 does not know std::isalnum
	return isalnum( ch ) || ch == '_';
}

static bool isLeftWordBoundary( string::size_type idx, const string &s )
{
	return 
	       !isWordCharacter( s[ idx ] ) && s[ idx ] != '&' && s[ idx ] != '@';
}

static bool isRightWordBoundary( string::size_type idx, const string &s )
{
	return idx >= s.length() || !isWordCharacter( s[ idx ] );
}

string::size_type Entity::findDeclarationStart( const string &s, string::size_type start )
{
	string::size_type p = s.find( "<", start );
	while ( p != string::npos ) {
		if ( s.substr( p, 4 ) == "<!--" ) {
			p = s.find( "-->", p );
		}
		if ( p == string::npos ) {
			return string::npos;
		}
		if ( s.substr( p, 8 ) == "<!ENTITY" ) {
			return p;
		}
		p = s.find( "<", p + 1 );
	}
	return p;
}

string::size_type Entity::findDeclarationEnd( const string &s, string::size_type start )
{
	unsigned int nestedBrackets = 1;
	for ( const char *data = s.c_str() + start; *data; ++data ) {
		if ( *data == '<' ) {
			++nestedBrackets;
		} else if ( *data == '>' && --nestedBrackets == 0 ) {
			return data - s.c_str();
		}
	}
	return string::npos;
}

bool Entity::isValidDeclaration( const string &declaration )
{
	vector<string> components = split( simplifyWhitespace( declaration ), ' ' );
	if ( components.size() < 3 ) {
		return false;
	}
	if ( components[ 0 ] != "<!ENTITY" ) {
		return false;
	}
	// Parameter entities are not interesting to us.
	if ( components[ 1 ] == "%" ) {
		return false;
	}
	// Neither are external general entities
	if ( components[ 2 ] == "SYSTEM" ) {
		return false;
	}
	return true;
}

Entity Entity::fromDeclaration( const string &declaration )
{
	// Skip leading whitespace
	string::size_type p = declaration.find( "<!ENTITY" );

	p = declaration.find_first_not_of( " \t\n\r", p + 8 );
	string::size_type q = declaration.find_first_of( " \t\n\r", p + 1 );
	const string name = declaration.substr( p, q - p );

	p = declaration.find_first_of( "'\"", q + 1 );
	q = declaration.find( declaration[ p ], p + 1 );
	const string value = declaration.substr( p + 1, q - p - 1 );

	return Entity( name, value );
}

Entity::Entity( const string &name, const string &value )
	: m_name( "&" + name + ";" ),
	m_value( value )
{
	if ( name == value ) {
		m_name = m_value;
	}

	if ( !Options::KDEMode ) {
		m_strippedValue = stripTags( m_value );
		return;
	}

	if ( m_value.substr( 0, 12 ) != "<personname>" ) {
		m_strippedValue = stripTags( m_value );
	} else {
		/* <personname> entities need special treatment since they are
		 * declared like
		 * <personname><firstname>Frerich</firstname><surname>Raabe</surname></personname>
		 * i.e. there is no whitespace between the name components. Since the
		 * naked entity is supposed to be "Frerich Raabe" though, we have to
		 * insert spaces manually.
		 */
		m_strippedValue = m_value;

		string::size_type p = m_strippedValue.find( "</firstname>" );
		p = m_strippedValue.find( '<', p + 12 );
		while ( p != string::npos ) {
			if ( m_strippedValue[ p + 1 ] != '/' ) {
				m_strippedValue.insert( p, " " );
			}
			p = m_strippedValue.find( '<', p + 2 );
		}

		m_strippedValue = stripTags( m_strippedValue );
	}
}

unsigned int Entity::applyTo( ChunkString &s_ ) const
{
	if ( m_name == m_value ) {
		return 0;
	}

	unsigned int hits = 0;
	string &s = s_.streamlineString();

	// First the simple values
	char *p = ::strstr( (char *)s.c_str(), m_value.c_str() );
	while ( p != NULL ) {
		const string::size_type idx = p - s.c_str();
		if ( !isInTag( idx, s ) ) {
			s_.replace( idx, m_value, m_name );
			++hits;
		}
		p = ::strstr( (char *)(s.c_str() + idx + m_value.size() - 1), m_value.c_str() );
	}

	// Now the naked ones.
	p = ::strstr( (char *)s.c_str(), m_strippedValue.c_str() );
	while ( p != NULL ) {
		const string::size_type idx = p - s.c_str();
		if ( isLeftWordBoundary( idx - 1, s ) &&
		     isRightWordBoundary( idx + m_strippedValue.size(), s ) &&
		     !isInTag( idx, s ) ) {
			const std::string e( element( idx, s ) );
			if ( !isIgnoredElement( e.c_str() ) ) {
				s_.replace( idx, m_strippedValue, m_name );
				++hits;
			}
		}
		p = ::strstr( (char *)(s.c_str() + idx + m_strippedValue.length() - 1), m_strippedValue.c_str() );
	}
	return hits;
}


