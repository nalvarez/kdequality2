/**
 * entity.h - (C) 2003, 2004, 2005 Frerich Raabe <raabe@kde.org>
 *
 * For licensing and distribution terms, refer to the accompanying
 * file ``COPYING''.
 */
#ifndef ENTITY_H
#define ENTITY_H

#include <string>

class ChunkString;

class Entity
{
	public:
		static std::string::size_type findDeclarationStart( const std::string &s, std::string::size_type start = 0 );
		static std::string::size_type findDeclarationEnd( const std::string &s, std::string::size_type start = 0 );
		static bool isValidDeclaration( const std::string &declaration );
		static Entity fromDeclaration( const std::string &declaration );

		Entity( const std::string &name, const std::string &value );

		const std::string &name() const { return m_name; }
		const std::string &value() const { return m_value; }
		const std::string &strippedValue() const { return m_strippedValue; }

		unsigned int applyTo( ChunkString &s ) const;

	private:
		std::string m_name;
		std::string m_value;
		std::string m_strippedValue;
};

#endif // ENTITY_H
