<h1>KDE DocBook Sanitizer</h1>
<p>The KDE DocBook Sanitizer is a validation tool useful for anybody who's
authoring, editing or commiting manuals for applications which make use of
the <a href="http://www.kde.org">KDE</a> framework. When given some DocBook
markup, it performs a number of checks on it, trying to catch common errors
and suggesting how to improve the quality of the markup.</p>
<p>You can inspect <a href="index.php">regularly regenerated statistics</a>
about <a href="http://www.kde.org">KDE</a>'s codebase, or you can have the
KDE DocBook Sanitizer check your own document <a href="just-in-time.php">just
in time</a>.</p>
