#!/usr/bin/perl -w

use File::Path 'mkpath';

my $solidFxResultDir = $ARGV[0];
my $xmlOutputDir = $ARGV[1];
my $solidFxExec = "/home/developer/cppchecks/build/cppanalyzer";

opendir DIR, $solidFxResultDir or die "Could not open $solidFxResultDir : $!\n";

system "$solidFxExec --export=xml --list > $xmlOutputDir/tool-list.xml 2> /dev/null";

for(readdir DIR)
{
  if ($_ =~ /.*\.fxc$/) {
    my $file = "$solidFxResultDir/$_";
    my $xmlFile = $_;
    $xmlFile =~ s/^([^\:]+\:)+//g;
    $xmlFile =~ s/fxc$/xml/;

    my $path = $_;
    $path =~ s/\:/\//g;
    $path =~ s/\/[^\/]+$//;

    mkpath( "$xmlOutputDir/$path", {mode => 0750} );

    $xmlFile = "$xmlOutputDir/$path/$xmlFile";

    print "=== Checking: $file\n";
    my $pid;

    eval {
      local $SIG{ALRM} = sub { die "!!! $_ timed out\n" }; # NB: \n required

      $pid = fork;
      if ($pid == 0) {
        exec "$solidFxExec --export=xml --explain $file > $xmlFile 2> /dev/null";
        die "Exec failed: $!";
      }

      alarm 2;
      waitpid $pid => 0;
    };

    if ($@) {
      # The command timed out.
      print "$@";
      kill 15, $pid + 1; # For some, to me unknown, reason + 1 is needed.
    }

    my $size = `du -b $xmlFile`;
    if ( $size =~ /^0\s+/ ) {
      `rm $xmlFile`;
    }
  }
}

closedir DIR or die "close failed : $!\n";

