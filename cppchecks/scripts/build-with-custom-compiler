#!/bin/bash

# This script is used to plugin our own compiler replacement SolidFX. SolidFX is
# called with (almost) the same arguments as the normal compiler would have been
# called. For each sourcefile it creates a fxc file. This file, containing all
# information extracted by SolidFX is than read by our own tool
#
# Short HOWTO:
# 1) Create a dir that will contain the components (e.g. ~/components)
# 2) Create in the components dir a dir for the component you want to check
#    (e.g. ~/components/kde-4.x)
# 3) Check out the module that you want to check in the component directory.
#    cd ~/components/kde-4.x && svn co svn://anonsvn.kde.org/home/kde/trunk/KDE/kdesupport
# 4) Create a temporary directory where the results will be stored
#    (e.g. mkdir ~/tmp && cd ~/tmp)
# 5) Modify the SCRIPTS_DIR && COMPONENTS_DIR variable below to match you system.
# 6) Start the check process: build-with-custom-compiler kde-4.x kdesupport

SCRIPTS_DIR=/home/developer/cppchecks/scripts
COMPONENTS_DIR=/home/developer/work/components
MAKE_OPTS="-j3"

# Read the component
if [[ -n $1 ]]; then
  COMPONENT=$1;
else
  echo "BWCC: No component specified"
  exit 1;
fi

if [[ ! -d $COMPONENTS_DIR/$COMPONENT ]]; then
  echo "BWCC: Component: $COMPONENTS_DIR/$COMPONENT does not exist.";
  exit 1;
fi

# Read the module
if [[ -n $2 ]]; then
  MODULE=$2;
else
  echo "BWCC: No submodule specified"
  exit 1;
fi

if [[ ! -d $COMPONENTS_DIR/$COMPONENT/$MODULE ]]; then
  echo "BWCC: Module $COMPONENTS_DIR/$COMPONENT/$MODULE does not exist.";
  exit 1;
fi

if [[ ! -d $COMPONENT-$MODULE-check ]]; then
  mkdir $COMPONENT-$MODULE-check;
fi

if [[ ! -d $COMPONENT-$MODULE-check/solidfx ]]; then
  mkdir $COMPONENT-$MODULE-check/solidfx;
else
  rm $COMPONENT-$MODULE-check/solidfx/* 2&> /dev/null; # Clean up old results.
fi

export BWCC_OUTPUT_DIR=`readlink -f $COMPONENT-$MODULE-check`/solidfx;

cd $COMPONENT-$MODULE-check;

# Check for in source build, we disallow this to prevent creating a mess.
if [[ `readlink -f $COMPONENTS_DIR/$COMPONENT/$MODULE` == `pwd` ]]; then
  echo "BWCC: In source build not allowed."
  exit 1;
fi

echo "BWCC: Checking source dir: $COMPONENTS_DIR/$COMPONENT/$MODULE"
export BWCC_SOURCE_DIR=$COMPONENTS_DIR/$COMPONENT/$MODULE

# plugin our custom compilers
export CC="$SCRIPTS_DIR/cmake-gcc"
export CXX="$SCRIPTS_DIR/cmake-g++"
unset BWCC_CHECK_EXECUTABLE;

if [[ ! -f `pwd`/Makefile ]]; then
  cmake $COMPONENTS_DIR/$COMPONENT/$MODULE
else
  echo "BWCC: Makefile exists, skipping cmake step, cleaning the build first";
  make $MAKE_OPTS clean;
fi;

export BWCC_CHECK_EXECUTABLE=$SCRIPTS_DIR/run-solidfx;
make $MAKE_OPTS

# move out of the $COMPONENT-$MODULE-check dir.
cd ..

# Clean up the environment variables afterwards.
unset CC
unset CXX

unset BWCC_OUTPUT_DIR
unset BWCC_SOURCE_DIR
unset BWCC_CHECK_EXECUTABLE

