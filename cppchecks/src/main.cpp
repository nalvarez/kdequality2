#include <QtCore/QCoreApplication>
#include <QtCore/QDir>
#include <QtCore/QPluginLoader>
#include <QtCore/QSettings>
#include <QtCore/QString>

#include <added/Path.h>
#include <added/FactDB.h>
#include <added/TypesEx.h>
#include <EfesAPI/FactDBEngine.h>

#include <iostream>

#include <pluginbase/analyzerplugin.h>

#include "outputformatter.h"
#include "textformatter.h"
#include "xmlformatter.h"

using namespace std;
using namespace EFES;

bool checkPluginDir(QString const &dir)
{
  QDir pluginDir(dir);
  if (!pluginDir.exists()) {
    cerr << "error: Dir" << dir.toUtf8().data() << "does not exist" << endl;
    return false;
  }

  return true;
}

QStringList plugins(QSettings const &settings)
{
  QString pluginDir = settings.value("plugin-dir").toString();

  if (!checkPluginDir(pluginDir))
    return QStringList();

  return settings.value("plugins").toStringList();
}

void printHelp(char *appName)
{
  cout << "CppAnalyzer is a plugin based tool to analyze C++ code. It" << endl;
  cout << "makes use of the SolidFx C++ factextraction framework." << endl << endl;
  cout << "Usage: " << appName << " [OPTIONS] factdb.fxc" << endl;
  cout << "   or: " << appName << " [OPTIONS] [MODE]" << endl;
  cout << endl;
  cout << "Modes:" << endl;
  cout << "--help -h: prints this help" << endl;
  cout << "--list -l: lists the available plugins." << endl << endl;
  //cout << "Options:" << endl;
  //cout << "--explain -e: lists the available plugins." << endl;
}

void printList(QSettings const &settings, OutputFormatter const &formatter)
{
  QStringList pluginNames = plugins(settings);

  if (!pluginNames.size())
    cout << "No plugins available" << endl;

  formatter.printListHeader();
  QString pluginDir = settings.value("plugin-dir").toString();
  foreach (QString const &pluginName, pluginNames) {
    QPluginLoader loader(pluginDir + QDir::separator() + pluginName);
    if (!loader.load()) {
      cerr << "warning: Unable to load " << pluginName.toUtf8().data() << ": "
           << loader.errorString().toUtf8().data() << " (skipping)" << endl;
      continue;
    } else {
      QObject *plugin = loader.instance();
      if (AnalyzerInterface *analyzer = qobject_cast<AnalyzerInterface*>(plugin)) {
        formatter.printPlugin(analyzer->name(), analyzer->version(), analyzer->shortDescription());
      }

      loader.unload(); // Unload the plugin after we ran it.
    }
  }
  formatter.printListFooter();
}


void printUsage(char *appName)
{
  cout << "Usage: " << appName << " [OPTIONS] factdb.fxc" << endl;
}

int main(int argc, char* argv[])
{
  QCoreApplication app(argc, argv);
  if (argc < 2) {
    printUsage(argv[0]);
    return 1;
  }

  if (app.arguments().contains("--help") || app.arguments().contains("-h")) {
    printHelp(argv[0]);
    return 0;
  }

  // On linux machines the config file can be found at:
  // ~/.config/EnglishBreakfastNetwork/CppAnalyzer.conf
  QSettings settings("EnglishBreakfastNetwork", "CppAnalyzer");

  QStringList pluginNames = plugins(settings);
  if (pluginNames.size() < 1) { // We require at least one plugin.
    cerr << "error: No plugins configured. Please check"
         << settings.fileName().toUtf8().data() << endl;
    return 1;
  }

  OutputFormatter *formatter;
  QString pluginDir = settings.value("plugin-dir").toString();

  QStringList outputformats = app.arguments().filter(QRegExp("--export=\\S+"));
  if (outputformats.size() > 0) {
    QString format = outputformats.first();
    format = format.right(format.size() - 9);
    if (format == "xml") {
      formatter = new XmlFormatter();
    }else {
      formatter = new TextFormatter();
      if (format != "text") {
        cerr << "Unsupported output format: " << qPrintable(format) << endl;
        cerr << "Falling back to text formatting." << endl << endl;
      }
    }
  } else {
    formatter = new TextFormatter();
  }

  if (app.arguments().contains("--list") || app.arguments().contains("-l")) {
    printList(settings, *formatter);
    delete formatter;
    return 0;
  }

  QFileInfo info(app.arguments().last());
  if (!info.exists()) {
    cerr << "error: Could not find file: " << qPrintable(app.arguments().last()) << endl;
    return 1;
  }

  // Okay, we seem to have at least one plugin. Now load the fcx file
  FactDBEngine dbe;
  Path p(argv[0],false);
  dbe.init(Path(argv[0],false));

  // Add the unit-name to the in-memory factdb. Now a unit, with index 0,
  // is present in the fact-db. We can retrieve it (by index) to use it further.
  FactDB::instance().addUnit(app.arguments().last().toUtf8());
  ExtractionUnitPtr extractionUnit = FactDB::instance().getExtractionUnit(0);
  extractionUnit->read(false,true,true,true); // (prepro, ast, types, locations)

  if (!extractionUnit->hasAST() || !extractionUnit->hasTypes()) {
    cerr << "error: Could not read AST and/or types from fact database." << endl;
    return 1;    //Nothing to do w/o the right info
  }

  bool headerPrinted = false;
  foreach(QString const &pluginName, pluginNames) {
    QPluginLoader loader(pluginDir + QDir::separator() + pluginName);
    if (!loader.load()) {
      cerr << "warning: Unable to load " << pluginName.toUtf8().data() << ": "
           << loader.errorString().toUtf8().data() << " (skipping)" << endl;
      continue;
    } else {
      QObject *plugin = loader.instance();
      if (AnalyzerInterface *analyzer = qobject_cast<AnalyzerInterface*>(plugin)) {
        // Run the analyzer
        analyzer->run(extractionUnit);
        formatter->setAnalyzerName(analyzer->name());
        formatter->setAnalyzerDescription(analyzer->description());
        formatter->setAnalyzerShortDescription(analyzer->shortDescription());

        if (analyzer->issues().size() > 0) {
          if (!headerPrinted) {
            formatter->printHeader();
            headerPrinted = true;
          }
          formatter->printResults(analyzer->issues());
        }
      } else {
        cerr << "warning: " << pluginName.toUtf8().data()
             << "does not implement the AnalyzerInterface (skipping)" << endl;
      }

      loader.unload(); // Unload the plugin after we ran it.
    }
  }

  if (headerPrinted)
    formatter->printFooter();
  else
    cerr <<"Okay!" << endl;

  delete formatter;
  return 0;
}
