#include "xmlformatter.h"

#include <iostream>
#include <QtCore/QDateTime>
#include <QtXml/QXmlStreamWriter>

using namespace std;

XmlFormatter::XmlFormatter()
  : mResult(new QByteArray), mWriter(new QXmlStreamWriter(mResult))
{ }

XmlFormatter::~XmlFormatter()
{
  delete mResult;
  delete mWriter;
}

void XmlFormatter::printFooter() const
{
  mWriter->writeEndElement(); // file-type
  mWriter->writeEndElement(); // file-types
  mWriter->writeEndElement(); // tool-result
  mWriter->writeEndDocument();
  cout << mResult->constData();
}

void XmlFormatter::printHeader() const
{
  mWriter->setAutoFormatting(true);
  mWriter->setAutoFormattingIndent(2);
  mWriter->writeStartDocument();
  mWriter->writeStartElement("tool-result");
  mWriter->writeAttribute("tool", "CppAnalyzer");
  mWriter->writeStartElement("global");
  mWriter->writeStartElement("date");
  mWriter->writeAttribute("value", QDateTime::currentDateTime().toString("MMMM dd yyyy hh:mm:ss"));
  mWriter->writeEndElement(); // date
  //mWriter->writeComment("Not exactly one as a some of the included files may be part of the check.");
  mWriter->writeStartElement("processed-files");
  mWriter->writeAttribute("value", "1");
  mWriter->writeEndElement(); // processed-files
  mWriter->writeStartElement("svn-rev");
  mWriter->writeAttribute("value", "unspecified");
  mWriter->writeEndElement(); // svn-rev
  mWriter->writeEndElement(); // global
  mWriter->writeStartElement("file-types");
  mWriter->writeStartElement("file-type");
  mWriter->writeAttribute("value", "c++");
}

void XmlFormatter::printListFooter() const
{
  mWriter->writeEndElement(); // file-type
  mWriter->writeEndElement(); // file-types
  mWriter->writeEndElement(); // tool-result
  mWriter->writeEndDocument();
  cout << mResult->constData();
}

void XmlFormatter::printListHeader() const
{
  mWriter->setAutoFormatting(true);
  mWriter->setAutoFormattingIndent(2);
  mWriter->writeStartDocument();
  mWriter->writeStartElement("tool");
  mWriter->writeAttribute("name", "CppAnalyzer");
  mWriter->writeStartElement("file-types");
  mWriter->writeStartElement("file-type");
  mWriter->writeAttribute("name", "c++");
}

void XmlFormatter::printPlugin(QString const &name, QString const &version, QString const &desc) const
{
  mWriter->writeStartElement("plugin");
  mWriter->writeAttribute("name", name);
  mWriter->writeAttribute("version", version);
  mWriter->writeAttribute("short-desc", desc);
  mWriter->writeEndElement();
}

void XmlFormatter::printResults(IssueList const &issues) const
{
  if (issues.size() == 0)
    return;

  mWriter->writeStartElement("check");
  mWriter->writeAttribute("name", mAnalyzerName);
  mWriter->writeAttribute("desc", mAnalyzerShortDescription);

  IssueMap issuesPerFile = orderIssues(issues, FileName);
  IssueMapIterator i(issuesPerFile);
  while (i.hasNext()) {
    i.next();
    mWriter->writeStartElement("file");
    mWriter->writeAttribute("name", i.key());

    IssueMap issuesPerDescription = orderIssues(i.value(), Description);
    IssueMapIterator j(issuesPerDescription);
    while (j.hasNext()) {
      j.next();
      mWriter->writeTextElement("message", j.key());
      mWriter->writeStartElement("issues");

      foreach (Issue const &issue, j.value()) {
        mWriter->writeTextElement("line",QString::number(issue.location().line()));
        //mWriter->writeStartElement("issue");
        //mWriter->writeTextElement("scope", issue.scope());

        //foreach (QString const &line, issue.lines()) {
        //  mWriter->writeTextElement("message", line);
        //}

        //mWriter->writeEndElement(); // issue;
      }
      mWriter->writeEndElement(); // issues
    }

    mWriter->writeEndElement(); // file
  }

  mWriter->writeTextElement("explanation", mAnalyzerDescription);
  mWriter->writeEndElement(); // plugin
}
