set(virtualscheck_SRCS
  checkerdatafactory.cpp
  classhierarchychecker.cpp
  virtualscheckvisitor.cpp
  virtualscheck.cpp
)

set(virtualscheck_MOC_HDRS
  virtualscheck.h
)

add_definitions(${QT_DEFINITIONS})
add_definitions(-DQT_PLUGIN)
add_definitions(-DQT_SHARED)

qt4_wrap_cpp(virtualscheck_MOC_SRCS ${virtualscheck_MOC_HDRS})

add_library(virtualscheck SHARED ${virtualscheck_SRCS} ${virtualscheck_MOC_SRCS})
target_link_libraries(virtualscheck
  pluginbase
  ${QT_QTCORE_LIBRARY}
  ${SolidFX_FXCXXAPI_LIBRARY}
)
