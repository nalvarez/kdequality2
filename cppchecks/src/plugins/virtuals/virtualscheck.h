#ifndef VIRTUALSCHECK_H
#define VIRTUALSCHECK_H

#include <pluginbase/analyzerplugin.h>

class VirtualsCheck : public QObject, public AnalyzerInterface
{
  Q_OBJECT
  Q_INTERFACES(AnalyzerInterface)

  public:
    VirtualsCheck();

    /**
     * Returns a description of what the analyzer does. Paragraphs are marked by
     * '\n' and if a line starts with '-' it is part of a list.
     */
    virtual QString description() const;

    virtual QString name() const;

    virtual void run(EFES::ExtractionUnitPtr const &eup);

    virtual QString shortDescription() const;

    virtual QString version() const;
};

#endif // VIRTUALSCHECK_H
