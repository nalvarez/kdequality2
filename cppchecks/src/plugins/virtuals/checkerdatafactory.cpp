#include "checkerdatafactory.h"

#include <added/TypeNodes.h>
#include <QtCore/QDebug>

using namespace EFES;
using namespace OverloadUtils;

bool OVFunctionData::virtualKeywordMissing() const
{
  return ((var->flags & DF_VIRTUAL) != DF_NONE)
         && ((dflags & DF_VIRTUAL) == DF_NONE);
}

bool OVFunctionsSigData::virtualDeclarationInconsistent() const
{
  int virtualFunctionCount = 0;
  bool topFunctionNotVirtual = true; // Is the function in the top of the hierarchy virtual?

  for(const_iterator it = begin(); it != end(); ++it) {
    if ((*it)->var->flags & DF_VIRTUAL) {
      if ((it - begin()) == 0)
        topFunctionNotVirtual = false;

      ++virtualFunctionCount;
    }
  }

  if (virtualFunctionCount == size()) return false;  // full virtuality in this set? no conflict
  if (size() < 2) return false;                      // just 1 func in this set, no conflict, continue
  // Only return true when the function at the top of the hierarchy was not
  // virtual, but functions with the same signature in base classes are virtual.
  return topFunctionNotVirtual && (virtualFunctionCount > 0);
}

bool OVFunctionsSigData::virtualKeywordMissing() const
{
  bool virtualKeywordMissing = false;
  for (const_iterator it = begin();it != end(); ++it) {
     OVFunctionData *f = static_cast<OVFunctionData*>(*it);
     virtualKeywordMissing = f->virtualKeywordMissing();
  }

  return virtualKeywordMissing;
}

OVFunction *CheckerDataFactory::newOVFunction()
{
  return new OVFunctionData;
}

OVFunctionsSig *CheckerDataFactory::newOVFunctionsSig()
{
  return new OVFunctionsSigData;
}
