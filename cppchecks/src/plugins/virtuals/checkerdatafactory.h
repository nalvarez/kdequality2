#ifndef CHECKERDATAFACTORY_H
#define CHECKERDATAFACTORY_H

#include <added/OverloadUtils.h>

//Captures data for the signature part of a method decl or def
class OVFunctionData : public EFES::OverloadUtils::OVFunction
{
  public:
    bool virtualKeywordMissing() const;
};

// Set of funcs, all having the same signature and name (e.g. from a class
// hierarchy)
class OVFunctionsSigData : public EFES::OverloadUtils::OVFunctionsSig
{
  public:
   /**
    * Functions with the same signature which are overriden via inheritance
    * should be declared virtual first time they appear in the hierarchy. Works
    * correctly in the presence of typedefs in function signatures. Considers
    * destructors too.
    */
    bool virtualDeclarationInconsistent() const;

    /**
     * Virtual function from a baseclass is overriden without re-specifying the
     * 'virtual' keyword (bad style)
     */
    bool virtualKeywordMissing() const;
};

class CheckerDataFactory : public EFES::OverloadUtils::OVDataFactory
{
  public:
    virtual EFES::OverloadUtils::OVFunction     *newOVFunction();
    virtual EFES::OverloadUtils::OVFunctionsSig *newOVFunctionsSig();
};

#endif // CHECKERDATAFACTORY_H
