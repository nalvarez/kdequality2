#include "virtualscheck.h"

#include <EfesAPI/EFES.h>

#include "virtualscheckvisitor.h"

using namespace EFES;
using namespace std;

VirtualsCheck::VirtualsCheck() : QObject(0)
{ }

QString VirtualsCheck::description() const
{
  return QString("The VirtualsCheck performs various checks with respect to "
                 "class hierarchies. Currently it checks for:\n"
                 "- Consistency of virtual declarations. Functions with the same "
                 "signature which are overriden via inheritance should be "
                 "declared virtual first time they appear in the hierarchy.\n"
                 "- Missing virtual keywords. Virtual function from a baseclass "
                 "is overriden without re-specifying the 'virtual' keyword"
                 "(bad style)");
}

QString VirtualsCheck::name() const
{
  return "VirtualsCheck";
}

void VirtualsCheck::run(ExtractionUnitPtr const &eup)
{
  VirtualsCheckVisitor visitor(eup);
  visitor.traverse(*eup->ast());
  mIssues = visitor.issues();
}

QString VirtualsCheck::shortDescription() const
{
  return QString("Checks for consistency of virtual methods declarations");
}

QString VirtualsCheck::version() const
{
  return "0.1";
}

Q_EXPORT_PLUGIN2(virtualscheck, VirtualsCheck)
