#include "virtualmismatchcheck.h"

#include <EfesAPI/EFES.h>

#include "virtualmismatchcheckvisitor.h"

using namespace EFES;

VirtualMismatchCheck::VirtualMismatchCheck()
{
}


QString VirtualMismatchCheck::description() const
{
  return QString("The virtual mismatch function checks virtual functions which "
                 "have almost the same signature. With almost the same is meant"
                 "that all types of the signature (return types, argument types)"
                 "are the same but there are some slight difference which are"
                 "hard to differentiate from a caller perspective. I.g. missing "
                 "const key words or value vs.reference.");
}

QString VirtualMismatchCheck::name() const
{
  return QString("VirtualSignatureMismatch");
}

void VirtualMismatchCheck::run(ExtractionUnitPtr const &eup)
{
  VirtualMismatchCheckVisitor visitor(eup);
  visitor.traverse(*eup->ast());
  mIssues = visitor.issues();
}

QString VirtualMismatchCheck::shortDescription() const
{
  return QString("Checks for slight mismatches in virtual overloaded methods.");
}

QString VirtualMismatchCheck::version() const
{
  return "0.1";
}

Q_EXPORT_PLUGIN2(virtualmismatchcheck, VirtualMismatchCheck)
