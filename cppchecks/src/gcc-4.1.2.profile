<Name>
  <![CDATA[gcc 4.1.2]]>
</Name>  
<System>
  <Directory>
    <![CDATA[/usr/lib/gcc/i686-pc-linux-gnu/4.1.2/include/g++-v4]]>
  </Directory>
  <Directory>
    <![CDATA[/usr/lib/gcc/i686-pc-linux-gnu/4.1.2/include/g++-v4/i686-pc-linux-gnu]]>
  </Directory>
  <Directory>
    <![CDATA[/usr/lib/gcc/i686-pc-linux-gnu/4.1.2/include/g++-v4/backward]]>
  </Directory>
  <Directory>
    <![CDATA[/usr/lib/gcc/i686-pc-linux-gnu/4.1.2/include]]>
  </Directory>
  <Directory>
    <![CDATA[/usr/include]]>
  </Directory>
</System>
<Includes>
</Includes>
<Force>
</Force>
<Defines>
  <Define>
    <![CDATA[__STDC__]]>
  </Define>
</Defines>
