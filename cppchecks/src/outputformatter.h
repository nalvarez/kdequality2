#ifndef OUTPUTFORMATTER_H
#define OUTPUTFORMATTER_H

#include <pluginbase/issue.h>

class OutputFormatter
{
  public:
    /**
     * Should be called after the last printResults() call.
     */
    virtual void printFooter() const = 0;

    /**
     * Prints a header. Is called from the constructor. The default
     * implementation does nothing.
     */
    virtual void printHeader() const = 0;

    /**
     * Prints the footer for listing available plugins.
     */
    virtual void printListFooter() const = 0;

    /**
     * Prints the header for listing available plugins.
     */
    virtual void printListHeader() const = 0;

    /**
     * Prints a plugin.
     */
    virtual void printPlugin(QString const &name, QString const &version, QString const &desc) const = 0;

    /**
     * Prints the results in a specific format for
     */
    virtual void printResults(IssueList const &issues) const = 0;

    /**
     * Sets the description of what is checked for by the analyzer.
     */
    void setAnalyzerDescription(QString const &description);

    /**
     * Sets the description of what is checked for by the analyzer.
     */
    void setAnalyzerShortDescription(QString const &shortDescription);

    /**
     * Sets the name of the analyzer for which the results will be printed.
     */
    void setAnalyzerName(QString const &name);

  protected: // Members
    QString mAnalyzerName;
    QString mAnalyzerDescription;
    QString mAnalyzerShortDescription;
};

#endif // OUTPUTFORMATTER_H
