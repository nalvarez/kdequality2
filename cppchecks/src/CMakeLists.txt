project(cppchecks)
cmake_minimum_required(VERSION 2.6)
set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/modules/")
include(CheckCXXCompilerFlag)

if(NOT "${CMAKE_SYSTEM_PROCESSOR}" MATCHES "i.86")
  message(FATAL_ERROR "CppChecks can only be build on 32-bit architecture")
else(NOT "${CMAKE_SYSTEM_PROCESSOR}" MATCHES "i.86")
  message(STATUS "Building on 32-bit architecture")
endif(NOT "${CMAKE_SYSTEM_PROCESSOR}" MATCHES "i.86")

set(CMAKE_CXX_FLAGS)
set(CMAKE_SHARED_LINKER_FLAGS)

set(Sqlite_MIN_VERSION "3.6")
find_package(Sqlite REQUIRED)

set(Boost_MIN_VERSION "1.35.0")
find_package(Boost COMPONENTS filesystem system regex REQUIRED)

set(QT_MIN_VERSION "4.5.0")
find_package(Qt4 REQUIRED)

find_package(SolidFX REQUIRED)

set(CMAKE_INSTALL_RPATH ${CMAKE_PREFIX_PATH}/lib)
check_cxx_compiler_flag(-fvisibility=hidden __KRAZY_HAVE_GCC_VISIBILITY)
set( __KRAZY_HAVE_GCC_VISIBILITY ${__KDE_HAVE_GCC_VISIBILITY} CACHE BOOL "GCC support for hidden visibility")

configure_file(krazymacros.h.cmake ${CMAKE_CURRENT_BINARY_DIR}/krazymacros.h)

set(INSTALL_TARGETS_DEFAULT_ARGS RUNTIME DESTINATION bin
                                 LIBRARY DESTINATION lib${LIB_SUFFIX}
                                 ARCHIVE DESTINATION lib${LIB_SUFFIX})

include_directories(
  ${QT_INCLUDE_DIR}
  ${SolidFX_INCLUDE_DIR}
  ${CMAKE_BINARY_DIR} # For krazymacros.h
  ${CMAKE_SOURCE_DIR}
)

add_subdirectory(pluginbase)
add_subdirectory(plugins)

# SolidFX needs those two files in the same dir as the executable.
configure_file(${CMAKE_SOURCE_DIR}/FXCXX.ini ${CMAKE_BINARY_DIR}/FXCXX.ini COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/default.project ${CMAKE_BINARY_DIR}/default.project COPYONLY)

set(ANALYZER_SRCS
  outputformatter.cpp
  textformatter.cpp
  xmlformatter.cpp
  main.cpp
)

add_executable(cppanalyzer ${ANALYZER_SRCS})
target_link_libraries(cppanalyzer
  pluginbase
  ${QT_QTCORE_LIBRARY}
  ${QT_QTXML_LIBRARY}
  ${SolidFX_LIBRARIES}
  ${Boost_LIBRARIES}
  ${SQLITE_LIBRARIES}
)
