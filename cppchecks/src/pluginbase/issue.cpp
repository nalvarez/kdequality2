#include "issue.h"
#include <QtCore/QMap>

void Issue::addIssueLine(QString const &line)
{
  mLines << line;
}

QString Issue::description() const
{
  return mDescription;
}

QStringList Issue::lines() const
{
  return mLines;
}

Location Issue::location() const
{
  return mLocation;
}

QString Issue::scope() const
{
  return mScope;
}

void Issue::setDescription(QString const &description)
{
  mDescription = description;
}

void Issue::setLocation(Location const &location)
{
  mLocation = location;
}

void Issue::setScope(QString const &scope)
{
  mScope = scope;
}

IssueMap orderIssues(IssueList const &issues, Field field)
{
  IssueMap orderedIssues;
  foreach (Issue const &issue, issues) {
    QString key;
    switch (field) {
    case Description:
      key = issue.description();
      break;
    case FileName:
      key = issue.location().fileName();
      break;
    case Scope:
      key = issue.scope();
      break;
    }

    IssueList issues = orderedIssues.value(key);
    issues << issue;
    orderedIssues.insert(key, issues);
  }

  return orderedIssues;
}
