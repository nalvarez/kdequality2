#ifndef ANALYZER_INTERFACE_H
#define ANALYZER_INTERFACE_H

#include <added/TypesEx.h>
#include <QtCore/QtPlugin>

#include <krazymacros.h>

#include "issue.h"

class KRAZY_EXPORT AnalyzerInterface
{
  public:
    virtual ~AnalyzerInterface() {}

    virtual QString description() const = 0;

    IssueList issues() const { return mIssues; }

    virtual QString name() const = 0;

    virtual void run(EFES::ExtractionUnitPtr const &eup) = 0;

    virtual QString shortDescription() const = 0;

    virtual QString version() const = 0;

  protected:
    IssueList mIssues;
};

Q_DECLARE_INTERFACE(AnalyzerInterface, "org.englishbreakfastnetwork.c++.AnalyzerInterface");

#endif // ANALYZER_INTERFACE_H
