#include "checkvisitor.h"

#include <added/FileTable.h>
#include <EfesAPI/FactDBEngine.h>

using namespace EFES;

CheckVisitor::CheckVisitor(ExtractionUnitPtr const &p)
  : mExtractionUnitPtr(p)
{}

IssueList CheckVisitor::issues() const
{
  return mIssues;
}

ASTVisitor::Visit CheckVisitor::postVisitASTNode(ASTNode &obj)
{
  return postVisitASTNode(static_cast<ASTNode const&>(obj));
}

ASTVisitor::Visit CheckVisitor::postVisitASTNode(ASTNode const &obj)
{
  return VISIT_SIBBLING;
}

ASTVisitor::Visit CheckVisitor::visitASTNode(ASTNode const &obj)
{
  return check(obj);
}

ASTVisitor::Visit CheckVisitor::visitASTNode(ASTNode &obj)
{
  return check(obj);
}

Location CheckVisitor::location(EFES::NodeWithLocation const &node) const
{
  FileMarker start, end;
  node.location(mExtractionUnitPtr.get(), start, end);

  FileTable const *fileTable = mExtractionUnitPtr->fileTable();

  Location location;
  location.setFileName(fileTable->filename(start.fileId()));
  location.setLine(start.offset().row);
  return location;
}

bool CheckVisitor::inUserSpace(ASTNode const &node)
{
  FileMarker start, end;
  node.location(mExtractionUnitPtr.get(), start, end);

  FileTable const *fs = mExtractionUnitPtr->fileTable();

  //Check if this is the source file or if it is a user header.
  if (start.fileId() == 0 || fs->isUserFile(start.fileId()))
    return true;

  return false; //If we arrive here, we have a sys header
}
