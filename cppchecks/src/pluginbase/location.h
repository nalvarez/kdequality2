#ifndef LOCATION_H
#define LOCATION_H

#include <QtCore/QString>

class Location
{
  public:
    Location();

    Location(Location const &other);

    ~Location();

    QString fileName() const;

    int line() const;

    void setFileName(QString const &fileName);

    void setLine(int line);

    Location &operator=(Location const &other);

  private:
    class LocationPrivate;
    LocationPrivate *d;
};

#endif // LOCATION_H
