#include "location.h"

class Location::LocationPrivate
{
  public:
    LocationPrivate()
      : mFileName("<unknown>")
      , mLine(-1)
    { }

    QString mFileName;
    int     mLine;
};

Location::Location()
    : d(new LocationPrivate)
{ }

Location::Location(Location const &other)
    : d(new LocationPrivate(*other.d))
{ }

Location::~Location()
{
  delete d;
}

QString Location::fileName() const
{
  return d->mFileName;
}

int Location::line() const
{
  return d->mLine;
}

void Location::setFileName(QString const &fileName)
{
  d->mFileName = fileName;
}

void Location::setLine(int line)
{
  d->mLine = line;
}

Location &Location::operator=(Location const &other)
{
  if (this == &other)
    return *this;

  delete d;
  d = new LocationPrivate(*other.d);
}
