#include "outputformatter.h"

void OutputFormatter::setAnalyzerDescription(QString const &description)
{
  mAnalyzerDescription = description;
}

void OutputFormatter::setAnalyzerName(QString const &name)
{
  mAnalyzerName = name;
}

void OutputFormatter::setAnalyzerShortDescription(QString const &shortDescription)
{
  mAnalyzerShortDescription = shortDescription;
}
