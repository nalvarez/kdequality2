--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- Name: component; Type: SEQUENCE; Schema: public; Owner: testr
--

CREATE SEQUENCE component
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.component OWNER TO testr;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: components; Type: TABLE; Schema: public; Owner: testr; Tablespace: 
--

CREATE TABLE components (
    id integer,
    name character varying(32),
    verbose_name character varying(30),
    description character varying(30)
);


ALTER TABLE public.components OWNER TO testr;

--
-- Name: generation; Type: SEQUENCE; Schema: public; Owner: testr
--

CREATE SEQUENCE generation
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.generation OWNER TO testr;

--
-- Name: generations; Type: TABLE; Schema: public; Owner: testr; Tablespace: 
--

CREATE TABLE generations (
    id integer,
    gdate timestamp without time zone,
    gissues integer,
    gtool integer,
    revision character varying(32),
    count integer,
    generation integer
);


ALTER TABLE public.generations OWNER TO testr;

--
-- Name: results_apidox; Type: TABLE; Schema: public; Owner: testr; Tablespace: 
--

CREATE TABLE results_apidox (
    id integer,
    checksum character varying(32),
    issues integer,
    report character varying(32),
    component character varying(32),
    module character varying(32),
    application character varying(32),
    generation integer
);


ALTER TABLE public.results_apidox OWNER TO testr;

--
-- Name: results_krazy; Type: TABLE; Schema: public; Owner: testr; Tablespace: 
--

CREATE TABLE results_krazy (
    id integer,
    checksum character varying(32),
    issues integer,
    report character varying(32),
    component character varying(32),
    module character varying(32),
    application character varying(32),
    generation integer
);


ALTER TABLE public.results_krazy OWNER TO testr;

--
-- Name: results_sanitizer; Type: TABLE; Schema: public; Owner: testr; Tablespace: 
--

CREATE TABLE results_sanitizer (
    id integer,
    checksum character varying(32),
    issues integer,
    report character varying(32),
    component character varying(32),
    module character varying(32),
    application character varying(32),
    generation integer
);


ALTER TABLE public.results_sanitizer OWNER TO testr;

--
-- Name: results_unittests; Type: TABLE; Schema: public; Owner: testr; Tablespace: 
--

CREATE TABLE results_unittests (
    id integer,
    checksum character varying(32),
    issues integer,
    report character varying(32),
    component character varying(32),
    module character varying(32),
    application character varying(32),
    generation integer
);


ALTER TABLE public.results_unittests OWNER TO testr;

--
-- Name: results_usability; Type: TABLE; Schema: public; Owner: testr; Tablespace: 
--

CREATE TABLE results_usability (
    id integer,
    checksum character varying(32),
    issues integer,
    report character varying(32),
    component character varying(32),
    module character varying(32),
    application character varying(32),
    generation integer
);


ALTER TABLE public.results_usability OWNER TO testr;

--
-- Name: tools; Type: TABLE; Schema: public; Owner: testr; Tablespace: 
--

CREATE TABLE tools (
    id integer,
    name character varying(32),
    component integer,
    description character varying(32),
    path character varying(32),
    generation integer
);


ALTER TABLE public.tools OWNER TO testr;

--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

